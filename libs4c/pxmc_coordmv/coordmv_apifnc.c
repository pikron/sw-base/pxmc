/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  coordmv_apifnc.c - coordinated multiple axes movements
                     the application interface related functions

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include "pxmc_coordmv_internal.h"

#include <string.h>
#include <malloc.h>

int pxmc_coordmv_connect(pxmc_coordmv_state_t *mcs_state,int options)
{
  pxmc_state_t *mcs;
  int flg;
  int inpr=mcs_state->mcs_flg&MCS_INPR_m;
  {
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	flg=mcs->pxms_flg;
	if(((flg&PXMS_BSY_m)&&!(flg&PXMS_CMV_m))
           ||(flg&PXMS_ENG_m)||(flg&PXMS_ERR_m))
          return -1;
	if(!inpr) continue;
	if(!(flg&PXMS_CMV_m)||!(flg&PXMS_ENR_m))
          return -1;
      }
    }
  }
  if(inpr) return 0;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      flg=mcs->pxms_flg;
      if(!(flg&PXMS_ENR_m))
      { /* smooth connection of controler to axis */
	if(pxmc_connect_controller(mcs)<0)
	  return -1;
      }
    }
  }

  inpr=0;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      flg=mcs->pxms_flg;
      if(!(flg&PXMS_CMV_m)){
        mcs->pxms_ep=mcs->pxms_rp;
	mcs->pxms_gen_mcsl_sp=mcs->pxms_ep;
	mcs->pxms_gen_mcsl_ep=mcs->pxms_ep;
	mcs->pxms_gen_mcsl_abort=0;
	mcs->pxms_gen_mcsl_ptr=(pxmc_info_t)mcs_state;
        if(options){
	 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
	  pxmc_set_flags(mcs,PXMS_CMV_m|PXMS_BSY_m);
	 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	  pxmc_set_flag(mcs,PXMS_BSY_b);
	  pxmc_set_flag(mcs,PXMS_CMV_b);
	 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	  flg=((volatile pxmc_state_t *)mcs)->pxms_flg;
	  if(flg&PXMS_ERR_m){
	   #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
	    pxmc_clear_flags(mcs,PXMS_CMV_m|PXMS_BSY_m);
	   #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	    pxmc_clear_flag(mcs,PXMS_CMV_b);
	    pxmc_clear_flag(mcs,PXMS_BSY_b);
	   #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	    inpr=-1;
	  }
	}
      }
    }
  }
  return inpr;
}


int pxmc_coordmv_bottom(pxmc_coordmv_state_t *mcs_state)
{
  pxmc_state_t *mcs;
  pxmc_cmvs_clear_flag_bit(mcs_state,MCS_BOTT_b);
  pxmc_coordmv_seg_clean(mcs_state,0);
  if(mcs_state->mcs_flg&MCS_STOP_m){
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	pxmc_stop(mcs,0);
      }
    }
   #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
    pxmc_cmvs_clear_flags(mcs_state,MCS_STOP_m|MCS_ERR_m|MCS_INPR_m);
   #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    pxmc_cmvs_clear_flag_bit(mcs_state,MCS_STOP_b);
    pxmc_cmvs_clear_flag_bit(mcs_state,MCS_ERR_b);
    pxmc_cmvs_clear_flag_bit(mcs_state,MCS_INPR_b);
   #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    __memory_barrier();
    mcs_state->mcs_seg_inpr=NULL;
    pxmc_coordmv_seg_clean(mcs_state,1);

  }
  if(!(mcs_state->mcs_flg&MCS_INPR_m)){
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	if(mcs->pxms_flg&PXMS_CMV_m){
	 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
	  pxmc_clear_flags(mcs,PXMS_CMV_m|PXMS_BSY_m);
	 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	  pxmc_clear_flag(mcs,PXMS_CMV_b);
	  pxmc_clear_flag(mcs,PXMS_BSY_b);
	 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
	  mcs->pxms_rs=0;
	}
      }
    }
  }
  return 0;
}

int pxmc_coordmv_foreach_get_pos(pxmc_coordmv_state_t *mcs_state,
           long *val_array, int val_array_size, unsigned int field_offset,
           int apply_subdiv)
{
  pxmc_state_t *mcs;

  pxmc_coordmv_for_mcs(mcs_state,mcs) {
    typeof(mcs->pxms_ap) pos;
    if(!val_array_size--)
      return 0;
    pos=*(typeof(pos)*)((char*)mcs+field_offset);
    if(apply_subdiv)
      *(val_array++)=pos>>PXMC_SUBDIV(mcs);
    else
      *(val_array++)=pos;
  }
  return 0;
}

int pxmc_coordmv_foreach_get_long(pxmc_coordmv_state_t *mcs_state,
           long *val_array, int val_array_size, unsigned int field_offset)
{
  pxmc_state_t *mcs;

  pxmc_coordmv_for_mcs(mcs_state,mcs) {
    if(!val_array_size--)
      return 0;
    *(val_array++)=*(long*)((char*)mcs+field_offset);
  }
  return 0;
}

int pxmc_coordmv_checkst(pxmc_coordmv_state_t *mcs_state)
{
  int i;
  int ret;
  pxmc_coordmv_seg_clean(mcs_state,0);
  ret=0;
  if(mcs_state->mcs_flg&MCS_INPR_m)
    ret=1;
  i=mcs_state->mcs_seg_max-mcs_state->mcs_seg_warn;
  if(mcs_state->mcs_seg_cnt>i)
    ret=2;
  if(mcs_state->mcs_flg&MCS_ERR_m)
    ret=-1;
  return ret;
}


int pxmc_coordmv_absmv(pxmc_coordmv_state_t *mcs_state, unsigned short final_cnt,
		     long *final, unsigned long mintim)
{
  int ret;
  pxmc_coordmv_seg_clean(mcs_state,0);

  if(mcs_state->mcs_seg_cnt>=mcs_state->mcs_seg_max)
    return -1;

  ret=pxmc_coordmv_connect(mcs_state,1);
  if(ret<0) return ret;

  ret=pxmc_coordmv_seg_add_line(mcs_state,final_cnt,final,0,mintim);
  if(ret<0) return ret;
  pxmc_coordmv_optimize(mcs_state);

  if(!(mcs_state->mcs_flg&MCS_INPR_m)){
    /* start real run */
    pxmc_coordmv_seg_clean(mcs_state,0);
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_INPR_b);
  }
  return 1;
}


int pxmc_coordmv_relmv(pxmc_coordmv_state_t *mcs_state, unsigned short final_cnt,
		     long *final, unsigned long mintim)
{
  int ret;
  pxmc_coordmv_seg_clean(mcs_state,0);

  if(mcs_state->mcs_seg_cnt>=mcs_state->mcs_seg_max)
    return -1;

  ret=pxmc_coordmv_connect(mcs_state,1);
  if(ret<0) return ret;

  ret=pxmc_coordmv_seg_add_line(mcs_state,final_cnt,final,1,mintim);
  if(ret<0) return ret;
  pxmc_coordmv_optimize(mcs_state);

  if(!(mcs_state->mcs_flg&MCS_INPR_m)){
    /* start real run */
    pxmc_coordmv_seg_clean(mcs_state,0);
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_INPR_b);
  }
  return 1;
}


int pxmc_coordmv_spline(pxmc_coordmv_state_t *mcs_state,
                     unsigned short param_cnt, long *param, int order,
		     unsigned long mintim)
{
  int ret;
  pxmc_coordmv_seg_clean(mcs_state,0);

  if(mcs_state->mcs_seg_cnt>=mcs_state->mcs_seg_max)
    return -1;

  ret=pxmc_coordmv_connect(mcs_state,1);
  if(ret<0) return ret;

  ret=pxmc_coordmv_seg_add_spline(mcs_state,param_cnt,param,order,mintim);
  if(ret<0) return ret;
  pxmc_coordmv_optimize(mcs_state);

  if(!(mcs_state->mcs_flg&MCS_INPR_m)){
    /* start real run */
    pxmc_coordmv_seg_clean(mcs_state,0);
    pxmc_cmvs_set_flag_bit(mcs_state,MCS_INPR_b);
  }
  return 1;
}

int pxmc_coordmv_grp(pxmc_coordmv_state_t *mcs_state,
		     unsigned short con_cnt, int *con_indx)
{
  pxmc_state_list_t *reg_list=mcs_state->mcs_con_list;
  unsigned long reg_msk=0, msk;
  int indx;
  int i, ret;

  pxmc_coordmv_seg_clean(mcs_state,0);

  if(mcs_state->mcs_flg&MCS_INPR_m) return -1;
  if(con_cnt>PXMC_SPLINE_ORDER_MAX) return -1;

  pxmc_cmvs_set_flag_bit(mcs_state,MCS_STOP_b);
  pxmc_coordmv_bottom(mcs_state);

  for(i=0;i<con_cnt;i++){
    indx=con_indx[i];
    if(indx>=reg_list->pxml_cnt) return -1;
    if(!reg_list->pxml_arr[indx]) return -1;
    msk=1<<indx;
    if(reg_msk&msk) return -1;
    if(reg_msk>msk) return -1;
    reg_msk|=msk;
  }

  mcs_state->mcs_con_cnt=con_cnt;
  mcs_state->mcs_con_msk=reg_msk;

  ret=pxmc_coordmv_connect(mcs_state,0);
  if(ret<0){

    return ret;
  }

  return 1;
}

