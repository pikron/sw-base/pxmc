/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  pxmc_coordmv.h - coordinated multiple axes movements
 
  Copyright (C) 2001-2017 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#ifndef _COORDMV_H_
#define _COORDMV_H_

#include <cpu_def.h>
#include "pxmc.h"

/* 64-bit square root is necessary for coordinated movement */
unsigned long sqrtll(unsigned long long x);
unsigned long sqrtll_approx(unsigned long long x);

#ifndef PXMC_COORD_CNT_MAX
#define PXMC_COORD_CNT_MAX 32
#endif /*PXMC_COORD_CNT_MAX*/

#ifndef PXMC_SPLINE_ORDER_MAX
#define PXMC_SPLINE_ORDER_MAX 6
#endif /*PXMC_SPLINE_ORDER_MAX*/

#if defined(__GNUC__) && (PXMC_COORD_CNT_MAX>3)
#define MCS_LOCARR_CNT(cnt) (cnt)
#else
#define MCS_LOCARR_CNT(cnt) PXMC_COORD_CNT_MAX
#endif

#if defined(__GNUC__) && (PXMC_SPLINE_ORDER_MAX>3)
#define MCS_LOCARR_ORDER(order) (order)
#else
#define MCS_LOCARR_ORDER(order) PXMC_SPLINE_ORDER_MAX
#endif

/* Coordinated segment flags */
#define MCSEG_DONE_b  0	/* segment interpretation done */
#define MCSEG_DONE_m  (1<<MCSEG_DONE_b)

#define MCSEG_LINE    0
#define MCSEG_SPLINE  1

/* Parameters of one straight movement segment */
typedef struct pxmc_coordmv_seg{
  unsigned short mcseg_flg;	/* flags */
  unsigned short mcseg_type;	/* the shape/type code of the segment */
  unsigned long  mcseg_sqn;	/* sequence number assigned to the segment*/
  struct pxmc_coordmv_seg *mcseg_next;
  struct pxmc_coordmv_seg *mcseg_prev;
  unsigned long  mcseg_ms;	/* segment maximal normalized speed */
  unsigned long  mcseg_ma;	/* segment maximal normalized acceleration */
  unsigned long  mcseg_sels;	/* normalized start edge limit speed of this segment */
  unsigned long  mcseg_eels;	/* normalized end edge limit speed of this segment */
  unsigned long  mcseg_es;	/* segment normalized end speed <= mcs_vv2 */
  signed long    mcseg_rmv[0];	/* relative movement for each axis */
}pxmc_coordmv_seg_t;

/* Coordinated state flags */
#define MCS_INPR_b   0	/* coordinated system is running */
#define MCS_BOTT_b   1	/* coordinator ending => needs bottom cleanup */
#define MCS_STOP_b   2	/* stop and flush all movements */
#define MCS_ERR_b    3	/* some axis in error state */
#define MCS_CHINPR_b 4	/* change parameters of inprogress seg on the fly */
#define MCS_INPR_m (1<<MCS_INPR_b)
#define MCS_BOTT_m (1<<MCS_BOTT_b)
#define MCS_STOP_m (1<<MCS_STOP_b)
#define MCS_ERR_m  (1<<MCS_ERR_b)
#define MCS_CHINPR_m (1<<MCS_CHINPR_b)

/* State structure for coordinated movements */
typedef struct pxmc_coordmv_state{
  pxmc_flags_t   mcs_flg;	/* flags */
 #ifndef PXMC_WITH_FLAGS_LONG_TYPE
  pxmc_flags_t   mcs_res1;
 #endif /*PXMC_WITH_FLAGS_LONG_TYPE*/
  unsigned long  mcs_par;	/* parameter <0,2^32> of generating function */
  unsigned long  mcs_rs;	/* actual requested speed in scale of parameter */
  unsigned long  mcs_es;	/* end speed of segment in scale of parameter */
  unsigned long  mcs_ms;	/* maximal speed for segment in scale of parameter */
  unsigned long  mcs_ma;	/* maximal acceleration for segment */
  unsigned long  mcs_disca;	/* maximal multiple of acceleration for edge discontinuity */
  int            (*mcs_gen)(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg);
  pxmc_state_list_t  *mcs_con_list;	/* connection to regulator list */
  /* pxmc_state_t **mcs_con_arr;*/ /* connection to regulators */
  unsigned long  mcs_con_msk;	/* coordinated regulators mask */
  unsigned short mcs_con_cnt;	/* number of coordinated regulators */
  unsigned short mcs_seg_max;	/* maximal allowable number of segments */
  unsigned short mcs_seg_warn;	/* warning when near to limit */
  unsigned short mcs_seg_cnt;	/* number of segnments in queue */
  unsigned long  mcs_next_sqn;	/* next number assigned to the segment */
  pxmc_coordmv_seg_t *mcs_seg_head;	/* segment list head */
  pxmc_coordmv_seg_t *mcs_seg_tail;	/* segment list tail */
  pxmc_coordmv_seg_t *mcs_seg_inpr;	/* actual segment in progress */
}pxmc_coordmv_state_t;

/* fields used in pxmc_state_t structure */
#define pxms_gen_mcsl_abort pxms_gen_info[0]	/* abort function call */
#define pxms_gen_mcsl_ptr   pxms_gen_info[1]	/* pointer to pxmc_coordmv_state */
#define pxms_gen_mcsl_sp    pxms_gen_info[2]	/* start position of segment */
#define pxms_gen_mcsl_ep    pxms_gen_info[3]	/* end position of segment */

#ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
#ifdef PXMC_WITH_FLAGS_LONG_TYPE

#define pxmc_cmvs_clear_flags(mcs,mask) \
  atomic_clear_mask(mask,&(mcs->mcs_flg))

#define pxmc_cmvs_set_flags(mcs,mask) \
  atomic_set_mask(mask,&(mcs->mcs_flg))

#define pxmc_cmvs_clear_flag_bit(mcs,nr) \
  atomic_clear_mask(1<<(nr),&(mcs->mcs_flg))

#define pxmc_cmvs_set_flag_bit(mcs,nr) \
  atomic_set_mask(1<<(nr),&(mcs->mcs_flg))

#else /*PXMC_WITH_FLAGS_LONG_TYPE*/

#define pxmc_cmvs_clear_flags(mcs,mask) \
  atomic_clear_mask_w(mask,&(mcs->mcs_flg))

#define pxmc_cmvs_set_flags(mcs,mask) \
  atomic_set_mask_w(mask,&(mcs->mcs_flg))

#define pxmc_cmvs_clear_flag_bit(mcs,nr) \
  atomic_clear_mask_w(1<<(nr),&(mcs->mcs_flg))

#define pxmc_cmvs_set_flag_bit(mcs,nr) \
  atomic_set_mask_w(1<<(nr),&(mcs->mcs_flg))

#endif /*PXMC_WITH_FLAGS_LONG_TYPE*/

#else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/

#define pxmc_cmvs_clear_flag_bit(mcs,nr) \
  clear_bit(nr,&((mcs)->mcs_flg))

#define pxmc_cmvs_set_flag_bit(mcs,nr) \
  set_bit(nr,&((mcs)->mcs_flg))

#endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/


/* structure of main coordinator */
extern pxmc_coordmv_state_t pxmc_coordmv_state;

/* add linear movement segment to previoudly configured coordinator */
int pxmc_coordmv_absmv(pxmc_coordmv_state_t *mcs_state, unsigned short final_cnt,
		     long *final, unsigned long mintim);

int pxmc_coordmv_relmv(pxmc_coordmv_state_t *mcs_state, unsigned short final_cnt,
		     long *final, unsigned long mintim);

int pxmc_coordmv_spline(pxmc_coordmv_state_t *mcs_state,
                     unsigned short param_cnt, long *param, int order,
		     unsigned long mintim);

/* group regulators into coordinated group  */
int pxmc_coordmv_grp(pxmc_coordmv_state_t *mcs_state,
		     unsigned short con_cnt, int *con_indx);

/* translates index in pxmc_coordmv_state_t to pointer to mcs struct */
pxmc_state_t *
  pxmc_coordmv_indx2mcs(pxmc_coordmv_state_t *mcs_state,int indx);

/* bottom coordinator handling */
int pxmc_coordmv_bottom(pxmc_coordmv_state_t *mcs_state);

/* checks coordinator state */
/*   -1 .. error, 0 .. idle, 1 .. running, 2 .. queue full */
int pxmc_coordmv_checkst(pxmc_coordmv_state_t *mcs_state);

int pxmc_coordmv_foreach_get_pos(pxmc_coordmv_state_t *mcs_state,
           long *val_array, int val_array_size, unsigned int field_offset,
           int apply_subdiv);

int pxmc_coordmv_foreach_get_long(pxmc_coordmv_state_t *mcs_state,
           long *val_array, int val_array_size, unsigned int field_offset);

#endif /* _COORDMV_H_ */
