/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  coordmv_base.c - coordinated multiple axes movements
                   basic fundamental functions
 
  Copyright (C) 2001-2017 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include "pxmc_coordmv_internal.h"

#include <string.h>
#include <malloc.h>

#undef DEBUG_EDGES

#if defined (__m68k__) || defined(__mc68332__) || defined(__mc68000__)
#define PXMC_CPU_ARCH_M68K
#elif defined(__i386__)
#warning i386
#define PXMC_CPU_ARCH_I386
#else
#warning Unknown CPU
#endif

pxmc_coordmv_seg_ops_t pxmc_coordmv_seg_ops_line;
pxmc_coordmv_seg_ops_t pxmc_coordmv_seg_ops_spline;

pxmc_coordmv_seg_ops_t *pxmc_coordmv_seg_types_default[]={
  [MCSEG_LINE]=&pxmc_coordmv_seg_ops_line,
  [MCSEG_SPLINE]=&pxmc_coordmv_seg_ops_spline
};

pxmc_coordmv_seg_ops_t **pxmc_coordmv_seg_types=pxmc_coordmv_seg_types_default;

/*------------------------------------------------------------------*/
/* Linear segment */

#if defined(PXMC_CPU_ARCH_M68K)

/* distribute value of control parameter to axes */
int pxmc_coordmv_line_gen(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  pxmc_state_t *mcs;
  long int new_rp, tmp;
  unsigned int flg=0;
  pxmc_coordmv_for_mcs(mcs_state,mcs){
    if(!(mcs->pxms_flg&PXMS_CMV_m)) continue;
    flg|=mcs->pxms_flg;
    __asm__ (
     "	movel	%3,%1\n"
     "	subl	%4,%1\n"
     "	blts	1f\n"
     "	mulul	%2,%0,%1\n"
     "	bras	2f\n"
     "1:mulul	%2,%0,%1\n"
     "	subl	%2,%0\n"
     "2:addl	%4,%0\n"
      :"=&d"(new_rp),"=&d"(tmp)
      :"d"(mcs_state->mcs_par),
       "g"(mcs->pxms_gen_mcsl_ep),"r"(mcs->pxms_gen_mcsl_sp)
      :"cc"
    );
    mcs->pxms_rs=new_rp-mcs->pxms_rp;
    mcs->pxms_rp=new_rp;
  }
  return (flg&PXMS_ERR_m)?-1:flg;
}

#else /*PXMC_CPU_ARCH_M68K*/

/* distribute value of control parameter to axes */
int pxmc_coordmv_line_gen(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  pxmc_state_t *mcs;
  long int new_rp;
  unsigned int flg=0;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      if(!(mcs->pxms_flg&PXMS_CMV_m)) continue;
      flg|=mcs->pxms_flg;
      new_rp=((
          (long long)mcs_state->mcs_par*
	    ((long long)mcs->pxms_gen_mcsl_ep-mcs->pxms_gen_mcsl_sp)
	)>>32)+mcs->pxms_gen_mcsl_sp;
      mcs->pxms_rs=new_rp-mcs->pxms_rp;
      mcs->pxms_rp=new_rp;
    }
  }
  return (flg&PXMS_ERR_m)?-1:flg;
}

#endif /*PXMC_CPU_ARCH_M68K*/

/* prepare segment processing and distribute values to axes */
int pxmc_coordmv_line_prep(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  int i=0;
  pxmc_state_t *mcs;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      if(mcs->pxms_flg&PXMS_CMV_m){
	mcs->pxms_gen_mcsl_sp=mcs->pxms_gen_mcsl_ep;
	mcs->pxms_gen_mcsl_ep+=mcseg->mcseg_rmv[i];
      }
      i++;
    }
  }
  return 0;
}

/* get segment start or end speed relative to parameter increment */
int pxmc_coordmv_line_get_ss_es(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg, long *spd)
{
  pxmc_state_t *mcs;
  int i=0;
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      spd[i]=mcseg->mcseg_rmv[i];
      i++;
    }
  }
  return 0;
}

pxmc_coordmv_seg_ops_t pxmc_coordmv_seg_ops_line={
  .segop_gen =  pxmc_coordmv_line_gen,
  .segop_prep = pxmc_coordmv_line_prep,
  .segop_get_ss=pxmc_coordmv_line_get_ss_es,
  .segop_get_es=pxmc_coordmv_line_get_ss_es
};

/*------------------------------------------------------------------*/
/* Spline segment */

/* distribute value of control parameter to axes */
int pxmc_coordmv_spline_gen(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  pxmc_state_t *mcs;
  unsigned int flg=0;
  long *param_ptr=mcseg->mcseg_rmv;
  unsigned int order;
  int i;

  order=*(param_ptr++);

  {
    unsigned long p[MCS_LOCARR_ORDER(order)];
    signed   long l;
    long long sum_rp;

    p[0]=mcs_state->mcs_par;

    i=0;
    do{
      p[i+1]=((long long)p[0]*p[i]+0x80000000ul)>>32;
      i++;
    }while(i<order-1);

    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	if(!(mcs->pxms_flg&PXMS_CMV_m)){
	  param_ptr+=order;
	  continue;
	}
	flg|=mcs->pxms_flg;
	sum_rp=(long long)mcs->pxms_gen_mcsl_sp<<32;
	for(i=0;i<order;i++){
	 #if 1
          /* this abundant teporary variable helps GCC to sort things right way */
          long long ll;
          l=*(param_ptr++);
	  if(l>=0){
	    ll=(unsigned long)l*(long long)p[i];
	    sum_rp+=ll;
	  }else{
	    ll=(unsigned long)(-l)*(long long)p[i];
	    sum_rp-=ll;
	  }
	 #else
          l=*(param_ptr++);
	  if(l<0)
	    sum_rp-=(long long)p[i]<<32;
	  sum_rp+=(unsigned long)l*(long long)p[i];
	 #endif
	}
	l=sum_rp>>32;
	mcs->pxms_rs=l-mcs->pxms_rp;
	mcs->pxms_rp=l;
      }
    }
  }

  return (flg&PXMS_ERR_m)?-1:flg;
}


/* prepare segment processing and distribute values to axes */
int pxmc_coordmv_spline_prep(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  pxmc_state_t *mcs;
  long *param_ptr=mcseg->mcseg_rmv;
  long ep;
  unsigned int order;

  int i;
  order=*(param_ptr++);
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      if(mcs->pxms_flg&PXMS_CMV_m){
	mcs->pxms_gen_mcsl_sp=mcs->pxms_gen_mcsl_ep;
	ep=0;;
	for(i=0;i<order;i++)
	  ep+=*(param_ptr++);
	mcs->pxms_gen_mcsl_ep+=ep;
      }else{
	param_ptr+=order;
      }
    }
  }
  return 0;
}

/* get segment start speed relative to parameter increment - this is equal to a1 for spline for par = 0 */
int pxmc_coordmv_spline_get_ss(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg, long *ss)
{
  pxmc_state_t *mcs;
  long *param_ptr=mcseg->mcseg_rmv;
  unsigned int order;

  order=*(param_ptr++);
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      *(ss++)=*(param_ptr);
      param_ptr+=order;
    }
  }

  return 0;
}

/* get segment end speed relative to parameter increment - this is equal to a1 + 2*a2 + .. for spline */
int pxmc_coordmv_spline_get_es(struct pxmc_coordmv_state *mcs_state, pxmc_coordmv_seg_t *mcseg, long *es)
{
  pxmc_state_t *mcs;
  long *param_ptr=mcseg->mcseg_rmv;
  long spd;
  unsigned int order;

  int i;
  order=*(param_ptr++);
  { /* Iterate over all grouped axes */
    pxmc_coordmv_for_mcs(mcs_state,mcs){
      spd=0;
      for(i=1;i<=order;i++)
	spd+=*(param_ptr++)*i;
      *(es++)=spd;
    }
  }

  return 0;
}

pxmc_coordmv_seg_ops_t pxmc_coordmv_seg_ops_spline={
  .segop_gen =  pxmc_coordmv_spline_gen,
  .segop_prep = pxmc_coordmv_spline_prep,
  .segop_get_ss=pxmc_coordmv_spline_get_ss,
  .segop_get_es=pxmc_coordmv_spline_get_es
};

/*------------------------------------------------------------------*/
/* Parameter updates */

#if defined(PXMC_CPU_ARCH_M68K)

/* update of control parameter at sample time */
int pxmc_coordmv_upda(pxmc_coordmv_state_t *mcs_state)
{
  unsigned long par;
  unsigned long ms, ma, rs, es, t2;
  int ret;
  rs=mcs_state->mcs_rs;
  ms=mcs_state->mcs_ms;
  ma=mcs_state->mcs_ma;
  if(rs<ms-ma){
     rs+=ma;
  } else {
     rs=ms;
  }
  if(mcs_state->mcs_flg&MCS_CHINPR_m){
    es=mcs_state->mcs_seg_inpr->mcseg_es;
    mcs_state->mcs_es=es;
  }else{
    es=mcs_state->mcs_es;
  }
  __asm__ (
    "	mulul	%1,%0,%3\n"	/* %0:%3=ma*mcs_par */
    "	negl	%3\n"
    "	negxl	%0\n"
    "	addl	%1,%0\n"
    "	lsll	#1,%3\n"
    "	roxll	#1,%0\n"
    "	bcss	8f\n"		/* %0:%3=2*ma*(0x100000000-mcs_state->mcs_par) */
    "	mulul	%2,%%d0,%2\n"
    "	addl	%2,%3\n"
    "	addxl	%%d0,%0\n"	/* %0:%3 += mcs_es*mcs_es */ 
    "	bcss	8f\n"
    "	movel	%3,%%sp@-\n"
    "	movel	%0,%%sp@-\n"
    "	bsrl	sqrtll\n"
    "	addql	#8,%%sp\n"
    "	movel	%%d0,%0\n"
    "	subl	%1,%0\n"	/* %0-=ma */
    "	bccs	7f\n"
    "	clrl	%0\n"
    "7:	lsrl	#1,%1\n"	/* %1=ma/2 */
    "	addl	%1,%0\n"	/* %0+=ma/2 */
    "	bras	9f\n"
    "8:	movql	#-1,%0\n"
    "9:		\n"
    :"=&d"(ms),"=&d"(ma),"=&d"(es),"=&d"(t2)
    :"0"(ms),"1"(ma),"2"(es),"3"(t2=mcs_state->mcs_par)
    :"d0","d1","a0","a1","cc"
  );
  if(rs>ms) rs=ms;
  if(!rs) rs=1;
  mcs_state->mcs_rs=rs;
  par=mcs_state->mcs_par+rs;
  ret=(par>=mcs_state->mcs_par);
  mcs_state->mcs_par=par;
  return ret;
}

#else /*PXMC_CPU_ARCH_M68K*/

/* update of control parameter at sample time */
int pxmc_coordmv_upda(pxmc_coordmv_state_t *mcs_state)
{
  unsigned long par;
  unsigned long ms, ma, rs, es;
  int ret;
  rs=mcs_state->mcs_rs;
  ms=mcs_state->mcs_ms;
  ma=mcs_state->mcs_ma;
  if(rs<ms-ma){
     rs+=ma;
  } else {
     rs=ms;
  }
  if(mcs_state->mcs_flg&MCS_CHINPR_m){
    es=mcs_state->mcs_seg_inpr->mcseg_es;
    mcs_state->mcs_es=es;
  }else{
    es=mcs_state->mcs_es;
  }
 #ifndef PXMC_WITH_SQRTLL_APPROX
  /* s=1/2*a*t^2+ve*t , v=t*a+ve			*/
  /* v=sqrt(ve^2+2*a*s) , t=(sqrt(ve^2+2*a*s)-ve)/a	*/
  /* s1=1/2*a*(t-1)^2+ve*t , v1=s-s1 , v1=v-1/2*a	*/
  ms=sqrtll((unsigned long long)es*es+
            2*ma*(0x100000000ll-mcs_state->mcs_par));
 #else /*PXMC_WITH_SQRTLL_APPROX*/
  ms=sqrtll_approx((unsigned long long)es*es+
            2*ma*(0x100000000ll-mcs_state->mcs_par));
 #endif /*PXMC_WITH_SQRTLL_APPROX*/
  if(ms>ma) ms-=ma/2; else ms=ma/2;

  if(rs>ms) rs=ms;
  if(!rs) rs=1;
  mcs_state->mcs_rs=rs;
  par=mcs_state->mcs_par+rs;
  /* The long type has to be changed to int32_t for 64-bit architecture */
  ret=(par>=mcs_state->mcs_par);
  mcs_state->mcs_par=par;
  return ret;
}

#endif /*PXMC_CPU_ARCH_M68K*/

int pxmc_coordmv_seg_add(pxmc_coordmv_state_t *mcs_state, pxmc_coordmv_seg_t *mcseg)
{
  /* add segment to BLL list */
  {
    pxmc_coordmv_seg_t *mcseg_tail;
    mcseg_tail=mcs_state->mcs_seg_tail;
    mcseg->mcseg_prev=mcseg_tail;
    mcseg->mcseg_sqn=mcs_state->mcs_next_sqn;
    __memory_barrier();
    mcs_state->mcs_seg_tail=mcseg;
    if(mcseg_tail)
      mcseg_tail->mcseg_next=mcseg;
    else
      mcs_state->mcs_seg_head=mcseg;
    mcs_state->mcs_seg_cnt++;
    mcs_state->mcs_next_sqn++;
  }

  /* compute segment edge */
  if(mcseg->mcseg_prev&&mcs_state->mcs_disca)
  {
    /* Next conditions must be fullfiled */
    /* Vv1=(Aa*Sb2+Ab*Sa2)/(Sa1*Sb2-Sb1*Sa2) */
    /* Vv2=(Aa*Sb1+Ab*Sa1)/(Sa1*Sb2-Sb1*Sa2) */
    pxmc_state_t *mcs;
    long long int  Sa1=0, Sa2=0, Aa=0;
    long long int  Sb1=0, Sb2=0, Ab=0;
    long long int  Si1,   Si2,   Ai;
    long long int  Vv1ai=0, Vv1bi=0, Sai, Sbi;
    long long int  Vv1=0x7fffffffffffffffLL;
    long long int  Vv2=0x7fffffffffffffffLL;
    long int	   Smax1=0, Smax2=0;
    int		   NZ=0,NZab=0,NZai,NZbi;
    pxmc_coordmv_seg_t *mcseg_prev=mcseg->mcseg_prev;
    int i=-1;

    long prev_es[MCS_LOCARR_CNT(mcs_state->mcs_con_cnt)]; /* relative end speeds of previous segment */
    long this_ss[MCS_LOCARR_CNT(mcs_state->mcs_con_cnt)]; /* relative start speeds of actual segment */

    /* compute segment star and stop axis speeds relative to the parameter  */
    pxmc_coordmv_seg_xop(mcseg->mcseg_prev,get_es)(mcs_state,mcseg->mcseg_prev,prev_es);
    pxmc_coordmv_seg_xop(mcseg,get_ss)(mcs_state,mcseg,this_ss);

    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	i++;
	Ai=mcs_state->mcs_disca*mcs->pxms_ma;
	Si1=prev_es[i];Si2=this_ss[i];

	if(Si1<0){
          Si1=-Si1;
	  Si2=-Si2;
	}

	if((Si1==0)&&(Si2==0)) continue;
	if((Si1>=Smax1)&&(Si2>=Smax2))
	{
          Smax1=Si1;
          Smax2=Si2;
	}

	if(NZ){

	  Sai=Sa1*Si2-Si1*Sa2;
	  if((NZai=(Sai!=0))) {
            Vv1ai=((Aa*Si2+Ai*Sa2)*(1LL<<32))/Sai;
	    Vv1ai=Vv1ai>=0?Vv1ai:-Vv1ai;
            #ifdef DEBUG_EDGES
              fprintf(stderr,"  Vv1ai=%lld\n",Vv1ai);
	    #endif
	  }

	  Sbi=Sb1*Si2-Si1*Sb2;
	  if((NZbi=(Sbi!=0))) {
            Vv1bi=((Ab*Si2+Ai*Sb2)*(1LL<<32))/Sbi;
	    Vv1bi=Vv1bi>=0?Vv1ai:-Vv1bi;
            #ifdef DEBUG_EDGES
              fprintf(stderr,"  Vv1bi=%lld\n",Vv1bi);
	    #endif
	  }

	  if(NZai&&NZbi){
            if(Vv1ai>Vv1bi) NZai=0; else NZbi=0;
	  }

	  if(NZai){
	    if(Vv1ai<Vv1){
              Vv2=((Aa*Si1+Ai*Sa1)*(1LL<<32))/Sai;
	      Vv2=Vv2>=0?Vv2:-Vv2;
              Vv1=Vv1ai; Sb1=Si1; Sb2=Si2; Ab=Ai;
	      NZab=1;
	    }
	    continue;
	  }

	  if(NZbi){
	    if(Vv1bi<Vv1){
              Vv2=((Ab*Si1+Ai*Sb1)*(1LL<<32))/Sbi;
	      Vv2=Vv2>=0?Vv2:-Vv2;
              Vv1=Vv1bi; Sa1=Si1; Sa2=Si2; Aa=Ai;
	      NZab=1;
	    }
	    continue;
	  }

	} /* NZ */

	if(!NZab) {
          if((!NZ)||(Sa1*Ai<Si1*Aa)) {
            Sa1=Sb1=Si1; Sa2=Sb2=Si2; Aa=Ab=Ai;
	    NZ=1;
	  }
	}

      } /* pxmc_coordmv_for_mcs */
    }

    if(!NZab) {
      Vv1=Smax2*0x100000000ll;
      Vv2=Smax1*0x100000000ll;
    }

    if((Vv1>mcseg_prev->mcseg_ms)||(Vv2>mcseg->mcseg_ms)){
      while((Vv1>0x80000000ll)||(Vv2>0x80000000ll)){
	Vv1>>=1; Vv2>>=1;
      }

      if(Vv1*mcseg->mcseg_ms<Vv2*mcseg_prev->mcseg_ms){
        Vv1=(mcseg->mcseg_ms*Vv1)/Vv2;
	Vv2=mcseg->mcseg_ms;
      }else{
        Vv2=(mcseg_prev->mcseg_ms*Vv2)/Vv1;
	Vv1=mcseg_prev->mcseg_ms;
      }
    }

    mcseg_prev->mcseg_eels=(unsigned long)Vv1;
    mcseg->mcseg_sels=(unsigned long)Vv2;

    #ifdef DEBUG_EDGES
      fprintf(stderr,"Vv1=%lld Vv2=%lld\n",Vv1,Vv2);
    #endif
  }

  return 1;
}

int pxmc_coordmv_seg_add_line(pxmc_coordmv_state_t *mcs_state,
			unsigned short final_cnt, long *final, int relative_fl,
			unsigned long mintim)
{
  pxmc_coordmv_seg_t *mcseg;
  if(mcs_state->mcs_con_cnt!=final_cnt)
    return -1;
  /* get memory for new segment */
  mcseg=(pxmc_coordmv_seg_t *)malloc(sizeof(pxmc_state_t)
			+sizeof(mcseg->mcseg_rmv[0])*final_cnt);
  if(!mcseg)
    return -1;
  mcseg->mcseg_flg=0;
  mcseg->mcseg_type=MCSEG_LINE;
  mcseg->mcseg_next=NULL;
  mcseg->mcseg_prev=NULL;
  mcseg->mcseg_sels=0;
  mcseg->mcseg_eels=0;
  mcseg->mcseg_es=0;
  /* Compute parameters of segment independent on next segment */
  {
    pxmc_state_t *mcs;
    unsigned long ms_min=~0;
    unsigned long ms;
    unsigned long ma_min=~0;
    unsigned long ma;
    unsigned long difp;
    signed long   difps;
    int i=0;
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	if(relative_fl){
	  difps=final[i];
	  mcs->pxms_ep+=final[i];
	  mcseg->mcseg_rmv[i]=difps;
	}else{
	  difps=final[i]-mcs->pxms_ep;
	  mcs->pxms_ep=final[i];
	  mcseg->mcseg_rmv[i]=difps;
	}
	if(difps){
          difp=difps>=0?difps:-difps;

	  if(mcs->pxms_ms<difp) {
            ms=mcs->pxms_ms*0x100000000ll/difp;
            if(ms<ms_min) ms_min=ms;
	  }
	  if(mcs->pxms_ma<difp) {
            ma=mcs->pxms_ma*0x100000000ll/difp;	/* 1/2*t^2*pxms_ma=difp    */
      	    if(ma<ma_min) ma_min=ma;		/* 1/2*t^2*ma=0x100000000 */
	  }
	}
	i++;
      }
    }
    if(mintim>1){ /* minimal time to spend for segment */
      ms=0x100000000ll/mintim;
      if(ms<ms_min) ms_min=ms;
    }
    if(!ms_min) ms_min=1;
    if(!ma_min) ma_min=1;
      else if(ma_min>ms_min) ma_min=ms_min;

    mcseg->mcseg_ms=ms_min;
    mcseg->mcseg_ma=ma_min;
  }

  return pxmc_coordmv_seg_add(mcs_state, mcseg);
}

int pxmc_coordmv_seg_add_spline(pxmc_coordmv_state_t *mcs_state,
			unsigned short param_cnt, long *param, int order,
			unsigned long mintim)
{
  pxmc_coordmv_seg_t *mcseg;
  if((order<=0)||(order>PXMC_SPLINE_ORDER_MAX))
    return -1;
  if(mcs_state->mcs_con_cnt*order!=param_cnt)
    return -1;
  /* get memory for new segment */
  mcseg=(pxmc_coordmv_seg_t *)malloc(sizeof(pxmc_state_t)
			+sizeof(mcseg->mcseg_rmv[0])*(param_cnt+1));
  if(!mcseg)
    return -1;
  mcseg->mcseg_flg=0;
  mcseg->mcseg_type=MCSEG_SPLINE;
  mcseg->mcseg_next=NULL;
  mcseg->mcseg_prev=NULL;
  mcseg->mcseg_sels=0;
  mcseg->mcseg_eels=0;
  mcseg->mcseg_es=0;
  /* Compute parameters of segment independent on next segment */
  {
    pxmc_state_t *mcs;
    unsigned long ms_min=~0;
    unsigned long ms;
    unsigned long ma_min=~0;
    unsigned long ma;
    unsigned long difp;
    signed long   difps;
    signed long   *param_ptr=param;
    signed long   *mcseg_ptr=mcseg->mcseg_rmv;
    int i;
    *(mcseg_ptr++)=order;
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	difps=0;
	for(i=0;i<order;i++){
          difps+=*(mcseg_ptr++)=*(param_ptr++);
	}
	mcs->pxms_ep+=difps;

	if(difps){
          difp=difps>=0?difps:-difps;

	  if(mcs->pxms_ms<difp) {
            ms=mcs->pxms_ms*0x100000000ll/difp;
            if(ms<ms_min) ms_min=ms;
	  }
	  if(mcs->pxms_ma<difp) {
            ma=mcs->pxms_ma*0x100000000ll/difp;	/* 1/2*t^2*pxms_ma=difp    */
      	    if(ma<ma_min) ma_min=ma;		/* 1/2*t^2*ma=0x100000000 */
	  }
	}
      }
    }
    if(mintim>1){ /* minimal time to spend for segment */
      ms=0x100000000ll/mintim;
      if(ms<ms_min) ms_min=ms;
    }
    if(!ms_min) ms_min=1;
    if(!ma_min) ma_min=1;
      else if(ma_min>ms_min) ma_min=ms_min;

    mcseg->mcseg_ms=ms_min;
    mcseg->mcseg_ma=ma_min;
  }

  return pxmc_coordmv_seg_add(mcs_state, mcseg);
}

int pxmc_coordmv_optimize(pxmc_coordmv_state_t *mcs_state)
{
  pxmc_coordmv_seg_t *mcseg, *mcseg_prev;
  unsigned long es=0;
  unsigned long ma;
  
  for(mcseg=mcs_state->mcs_seg_tail;mcseg;mcseg=mcseg_prev){
    if(!(mcseg_prev=mcseg->mcseg_prev)) break;
    if(!mcseg_prev->mcseg_eels) break;

    /* v=sqrt(ve^2+2*a*s) */
    ma=mcseg->mcseg_ma;
    es=sqrtll((unsigned long long)es*es+2*ma*(0x100000000ll))-ma/2;
    if(es>mcseg->mcseg_ms)
      es=mcseg->mcseg_ms;
    if(es>mcseg->mcseg_sels)
      es=mcseg->mcseg_sels;
    {
      unsigned long long ll1=0;
      ll1=(unsigned long long)es*mcseg_prev->mcseg_eels;
      if(ll1<mcseg->mcseg_sels*0x100000000ll)
	es=ll1/mcseg->mcseg_sels;
      else
	es=mcseg_prev->mcseg_eels;
    }
    if(es<=mcseg_prev->mcseg_es) break;
    mcseg_prev->mcseg_es=es;
    #ifdef DEBUG_EDGES  
      fprintf(stderr,"  updating es %ld\n",es);
    #endif
  }
  
  return 1;
}


int pxmc_coordmv_seg_next(pxmc_coordmv_state_t *mcs_state)
{
  pxmc_coordmv_seg_t *mcseg,*mcseg_last;
  unsigned long par=0;
  unsigned long rs=0;
  if(!(mcseg_last=mcs_state->mcs_seg_inpr)){
    mcseg=mcs_state->mcs_seg_inpr=mcs_state->mcs_seg_head;
  }else{
    mcseg=mcs_state->mcs_seg_inpr=mcseg_last->mcseg_next;
    if(mcseg_last->mcseg_eels){
      par=((unsigned long long)mcs_state->mcs_par*
         mcseg->mcseg_sels)/mcseg_last->mcseg_eels;
	 
      rs=mcs_state->mcs_rs;
    #if 0
      if(rs>mcseg_last->mcseg_eels){
        #ifdef DEBUG_EDGES  
	  fprintf(stderr,"  requested rs %ld > eels %ld\n",
		    rs,mcseg_last->mcseg_eels);
	#endif
	rs=mcseg_last->mcseg_eels;
      }
    #else
      if(rs>mcseg_last->mcseg_es){
	#ifdef DEBUG_EDGES  
	  fprintf(stderr,"  requested rs %ld > es %ld\n",
		    rs,mcseg_last->mcseg_es);
	#endif
	rs=mcseg_last->mcseg_es;
      }
    #endif
      rs=((unsigned long long)rs*mcseg->mcseg_sels)/mcseg_last->mcseg_eels;
      
      #ifdef DEBUG_EDGES  
	fprintf(stderr,"  old es %ld old rs %ld new rs %ld\n",
		mcseg_last->mcseg_es,mcs_state->mcs_rs,rs);
      #endif
    }
    mcseg_last->mcseg_flg|=MCSEG_DONE_m;
  }
  if(!mcseg){
    pxmc_state_t *mcs;
    { /* Iterate over all grouped axes */
      pxmc_coordmv_for_mcs(mcs_state,mcs){
	if(!(mcs->pxms_flg&PXMS_CMV_m)) continue;
	mcs->pxms_rp=mcs->pxms_gen_mcsl_sp=mcs->pxms_gen_mcsl_ep;
	mcs->pxms_rs=0;
      }
    }
    mcs_state->mcs_gen=pxmc_coordmv_line_gen;
    return 0;
  }
  mcs_state->mcs_ms=mcseg->mcseg_ms;
  mcs_state->mcs_ma=mcseg->mcseg_ma;
  mcs_state->mcs_es=mcseg->mcseg_es;
  mcs_state->mcs_par=par;
  mcs_state->mcs_rs=rs;
  if(rs>mcseg->mcseg_sels){
    #ifdef DEBUG_EDGES  
      fprintf(stderr,"  requested rs %ld > sels %ld\n",
		rs,mcseg->mcseg_sels);
    #endif
  }
  pxmc_cmvs_set_flag_bit(mcs_state,MCS_CHINPR_b);
  pxmc_coordmv_seg_xop(mcseg,prep)(mcs_state,mcseg);
  mcs_state->mcs_gen=pxmc_coordmv_seg_xop(mcseg,gen);
  return 1;
}

void pxmc_coordmv_seg_del(pxmc_coordmv_state_t *mcs_state,
			pxmc_coordmv_seg_t *mcseg)
{
  pxmc_coordmv_seg_t *mcseg_prev,*mcseg_next;

  mcseg_prev=mcseg->mcseg_prev;
  mcseg_next=mcseg->mcseg_next;
  if(mcseg_prev) {
    mcseg_prev->mcseg_next=mcseg_next;
  }else{
    mcs_state->mcs_seg_head=mcseg_next;
  }
  if(mcseg_next) {
    mcseg_next->mcseg_prev=mcseg_prev;
  }else{
    mcs_state->mcs_seg_tail=mcseg_prev;
  }
  mcs_state->mcs_seg_cnt--;
}

void pxmc_coordmv_seg_clean(pxmc_coordmv_state_t *mcs_state, int force)
{
  pxmc_coordmv_seg_t *mcseg,*mcseg_next;
  for(mcseg=mcs_state->mcs_seg_head;mcseg;mcseg=mcseg_next){
    mcseg_next=mcseg->mcseg_next;
    if(!(mcseg->mcseg_flg&MCSEG_DONE_m)&&!force) break;
    pxmc_coordmv_seg_del(mcs_state,mcseg);
    free(mcseg);
  }
}

/* translates index in pxmc_coordmv_state_t to pointer to mcs struct */
pxmc_state_t *
  pxmc_coordmv_indx2mcs(pxmc_coordmv_state_t *mcs_state,int indx)
{
  pxmc_state_t **reg_p;
  unsigned int reg_msk;
  if(!mcs_state->mcs_con_list) return NULL;
  reg_p=mcs_state->mcs_con_list->pxml_arr;
  for(reg_msk=mcs_state->mcs_con_msk;reg_msk;reg_msk>>=1,reg_p++){
    if(reg_msk&1){
      if(!indx--) return *reg_p;
    }
  }
  return NULL;
}
