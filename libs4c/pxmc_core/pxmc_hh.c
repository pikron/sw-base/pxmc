/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_hhfind.c - generic multi axis motion controller
                  hard home finding support

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"

/**
 * pxmc_hh - Starts reference search - Hard Home
 * @mcs:	Motion controller state information
 */
int pxmc_hh(pxmc_state_t *mcs)
{
  long ispeed;
  pxmc_call_t *do_gen_init=pxmc_get_hh_gi_4axis(mcs);

  if(!do_gen_init)
    return -1;

  if(pxmc_set_gen_prep(mcs)<0) return -1;

  mcs->pxms_gen_st=0;
  ispeed=mcs->pxms_ms>>(mcs->pxms_cfg&PXMS_CFG_HSPD_m);	/* initial speed */
  if(!ispeed)ispeed=1;
  if(mcs->pxms_cfg&PXMS_CFG_HDIR_m)ispeed=-ispeed;	/* initial direction */
  mcs->pxms_gen_hsp=ispeed;		/* initial speed */
  mcs->pxms_gen_hac=mcs->pxms_ma;	/* axis acceleration */
  mcs->pxms_gen_hcfg=mcs->pxms_cfg;	/* home config */

 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_BSY_b);
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
  pxmc_set_flag(mcs,PXMS_ENG_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  __memory_barrier();

  mcs->pxms_do_gen=do_gen_init;

  return 0;
}
