/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_gen_pthalalign.c - generic multi axis motion controller
                  motion generator to measure HAL sensor
                  to phasetable alignment

  (C) 2001-2011 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2011 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"

#define pxms_gen_ptha_start pxms_gen_info[0]	/* start position */
#define pxms_gen_ptha_r2acq pxms_gen_info[1]	/* position range to measure HAL */
#define pxms_gen_ptha_ptbt  pxms_gen_info[2]	/* phase-table base tracking */
#define pxms_gen_ptha_sp    pxms_gen_info[3]	/* copied pxms_ms */
#define pxms_gen_ptha_hal1  pxms_gen_info[4]	/* previous HAL state */
#define pxms_gen_ptha_ofsac pxms_gen_info[5]	/* HAL phase offset accumulator */
#define pxms_gen_ptha_ofscn pxms_gen_info[6]	/* count of accumulated infomation */
#define pxms_gen_ptha_fin   pxms_gen_info[7]	/* function to process final computation */

int pxmc_pthalalign_gu10(pxmc_state_t *mcs);
int pxmc_pthalalign_gu20(pxmc_state_t *mcs);
int pxmc_pthalalign_gd10(pxmc_state_t *mcs);
int pxmc_pthalalign_gd20(pxmc_state_t *mcs);
int pxmc_pthalalign_gd30(pxmc_state_t *mcs);

static inline int
pxmc_pthalalign_ptofs_diff(pxmc_state_t *mcs)
{
  short pt_ofs;
  short ptbt;

  pt_ofs=mcs->pxms_ptofs;
  __memory_barrier();

  ptbt=mcs->pxms_gen_ptha_ptbt;
  if((short)(pt_ofs-ptbt)>mcs->pxms_ptirc/2)
    mcs->pxms_gen_ptha_ptbt=ptbt+=mcs->pxms_ptirc;
  else if((short)(pt_ofs-ptbt)<-mcs->pxms_ptirc/2)
    mcs->pxms_gen_ptha_ptbt=ptbt-=mcs->pxms_ptirc;
  return (short)pt_ofs-ptbt;
}

int
pxmc_pthalalign_gi(pxmc_state_t *mcs)
{
  pxmc_set_flag(mcs,PXMS_BSY_b);
  mcs->pxms_rsfg=0;
  mcs->pxms_do_gen=pxmc_pthalalign_gu10;
  return pxmc_pthalalign_gu10(mcs);
}

int
pxmc_pthalalign_gu10(pxmc_state_t *mcs)
{
  int res;

  if(mcs->pxms_flg&PXMS_ERR_m) {
    pxmc_clear_flag(mcs,PXMS_PRA_b);
    return pxmc_spdnext_gend(mcs);
  }

  res=pxmc_spd_gacc(&(mcs->pxms_rs),mcs->pxms_gen_ptha_sp,mcs->pxms_ma);
  mcs->pxms_rp+=mcs->pxms_rs;

  pxmc_set_flag(mcs,PXMS_PRA_b);

  if(res || !(mcs->pxms_flg&PXMS_PTI_m))
    return 0;

  mcs->pxms_gen_ptha_hal1=mcs->pxms_hal;
  mcs->pxms_gen_ptha_start=mcs->pxms_rp;
  mcs->pxms_gen_ptha_ptbt=mcs->pxms_ptofs;
  mcs->pxms_do_gen=pxmc_pthalalign_gu20;
  return 0;
}

int
pxmc_pthalalign_gu20(pxmc_state_t *mcs)
{
  short hal_state;

  if(mcs->pxms_flg&PXMS_ERR_m) {
    pxmc_clear_flag(mcs,PXMS_PRA_b);
    return pxmc_spdnext_gend(mcs);
  }

  mcs->pxms_rp+=mcs->pxms_rs;

  hal_state=mcs->pxms_hal;
  __memory_barrier();

  if((hal_state!=mcs->pxms_gen_ptha_hal1) &&
     !((mcs->pxms_gen_ptha_hal1 | hal_state) & ~0x3f)) {
    mcs->pxms_gen_ptha_ofsac+=pxmc_pthalalign_ptofs_diff(mcs);
    mcs->pxms_gen_ptha_ofscn++;
  }

  mcs->pxms_gen_ptha_hal1=hal_state;
  pxmc_set_flag(mcs,PXMS_PRA_b);

  if((long)((unsigned long)mcs->pxms_rp-(unsigned long)mcs->pxms_gen_ptha_start)>mcs->pxms_gen_ptha_r2acq)
    mcs->pxms_do_gen=pxmc_pthalalign_gd10;

  return 0;
}

int
pxmc_pthalalign_gd10(pxmc_state_t *mcs)
{
  short hal_state;
  int res;

  if(mcs->pxms_flg&PXMS_ERR_m) {
    pxmc_clear_flag(mcs,PXMS_PRA_b);
    return pxmc_spdnext_gend(mcs);
  }

  res=pxmc_spd_gacc(&(mcs->pxms_rs),-mcs->pxms_gen_ptha_sp,mcs->pxms_ma);
  mcs->pxms_rp+=mcs->pxms_rs;

  hal_state=mcs->pxms_hal;
  __memory_barrier();

  if((hal_state!=mcs->pxms_gen_ptha_hal1) &&
     !((mcs->pxms_gen_ptha_hal1 | hal_state) & ~0x3f)) {
    pxmc_pthalalign_ptofs_diff(mcs);
  }
  mcs->pxms_gen_ptha_hal1=hal_state;
  pxmc_set_flag(mcs,PXMS_PRA_b);

  if(res)
    return 0;

  mcs->pxms_do_gen=pxmc_pthalalign_gd20;
  return 0;
}

int
pxmc_pthalalign_gd20(pxmc_state_t *mcs)
{
  short hal_state;

  if(mcs->pxms_flg&PXMS_ERR_m) {
    pxmc_clear_flag(mcs,PXMS_PRA_b);
    return pxmc_spdnext_gend(mcs);
  }

  mcs->pxms_rp+=mcs->pxms_rs;

  hal_state=mcs->pxms_hal;
  __memory_barrier();

  if((hal_state!=mcs->pxms_gen_ptha_hal1) &&
     !((mcs->pxms_gen_ptha_hal1 | hal_state) & ~0x3f)) {
    mcs->pxms_gen_ptha_ofsac+=pxmc_pthalalign_ptofs_diff(mcs);
    mcs->pxms_gen_ptha_ofscn++;
  }

  mcs->pxms_gen_ptha_hal1=hal_state;
  pxmc_set_flag(mcs,PXMS_PRA_b);

  if((long)(mcs->pxms_rp-mcs->pxms_gen_ptha_start)<0)
    mcs->pxms_do_gen=pxmc_pthalalign_gd30;

  return 0;
}

int
pxmc_pthalalign_gd30(pxmc_state_t *mcs)
{
  short hal_state;
  int res;

  if(mcs->pxms_flg&PXMS_ERR_m) {
    pxmc_clear_flag(mcs,PXMS_PRA_b);
    return pxmc_spdnext_gend(mcs);
  }

  res=pxmc_spd_gacc(&(mcs->pxms_rs),0,mcs->pxms_ma);
  mcs->pxms_rp+=mcs->pxms_rs;

  hal_state=mcs->pxms_hal;
  __memory_barrier();

  if((hal_state!=mcs->pxms_gen_ptha_hal1) &&
     !((mcs->pxms_gen_ptha_hal1 | hal_state) & ~0x3f)) {
    pxmc_pthalalign_ptofs_diff(mcs);
  }
  mcs->pxms_gen_ptha_hal1=hal_state;

  if(res) {
    pxmc_set_flag(mcs,PXMS_PRA_b);
    return 0;
  }

  pxmc_clear_flag(mcs,PXMS_PRA_b);

  if(mcs->pxms_gen_ptha_ofscn) {
    int ofscn=mcs->pxms_gen_ptha_ofscn;
    short ptofs;
    short ptofs_diff;
    ptofs_diff=(mcs->pxms_gen_ptha_ofsac+ofscn/2)/ofscn;

    ptofs=mcs->pxms_gen_ptha_ptbt+ptofs_diff;

    ptofs_diff=ptofs-mcs->pxms_ptofs;

    if(ptofs_diff>mcs->pxms_ptirc/2) {
      ptofs-=mcs->pxms_ptirc;
      ptofs_diff-=mcs->pxms_ptirc;
    } else if(ptofs_diff<-mcs->pxms_ptirc/2) {
      ptofs+=mcs->pxms_ptirc;
      ptofs_diff+=mcs->pxms_ptirc;
    }

    if((ptofs_diff>mcs->pxms_ptirc/2) ||
       (ptofs_diff<-mcs->pxms_ptirc/2))
      pxmc_set_errno(mcs,PXMS_E_COMM);
    else
      mcs->pxms_ptofs=ptofs;
  }

  if(mcs->pxms_gen_ptha_fin) {
    pxmc_call_t *fin_fnc;
    fin_fnc=(pxmc_call_t *)mcs->pxms_gen_ptha_fin;
    fin_fnc(mcs);
  }
  pxmc_spdnext_gend(mcs);
  return 0;
}

/**
 * pxmc_pthalalign - Starts HAL alignment measurement cycle
 * @mcs:	Motion controller state information
 * @r2acq:	Range to acquire HAL alignment information
 * @spd:	Speed to run acquire at
 * @fin_fnc:	Function to do final computation
 */
int pxmc_pthalalign(pxmc_state_t *mcs, long r2acq, long spd, pxmc_call_t *fin_fnc)
{
  pxmc_call_t *do_gen_init=pxmc_pthalalign_gi;

  if(pxmc_set_gen_prep(mcs)<0) return -1;

  mcs->pxms_gen_st=0;
  mcs->pxms_gen_ptha_sp=spd;		/* initial speed */
  mcs->pxms_gen_ptha_r2acq=r2acq;	/* range to acquire */
  mcs->pxms_gen_ptha_fin=(pxmc_info_t)fin_fnc;	/* function for final computation */
  mcs->pxms_gen_ptha_ofscn=0;
  mcs->pxms_gen_ptha_ofsac=0;

 #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
  pxmc_set_flags(mcs,PXMS_ENI_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
 #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  pxmc_set_flag(mcs,PXMS_BSY_b);
  pxmc_set_flag(mcs,PXMS_ENI_b);
  pxmc_set_flag(mcs,PXMS_ENR_b);
  pxmc_set_flag(mcs,PXMS_ENG_b);
 #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
  __memory_barrier();

  mcs->pxms_do_gen=do_gen_init;

  return 0;
}
