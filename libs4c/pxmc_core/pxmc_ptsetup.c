/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_ptsetup.c - generic multi axis motion controller
                  phase tables setup utilities

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <limits.h>
#include "pxmc.h"
#include "pxmc_internal.h"

#if !defined(clzl) && !defined(HAVE_CLZL)
#define clzl __builtin_clzl
#endif

/**
 * pxmc_ptvang_deg2irc - Converts phase shift from deg to IRC
 * @mcs:	Motion controller state information
 * @deg:	Phase shift angle in degree
 */
short pxmc_ptvang_deg2irc(pxmc_state_t *mcs, int deg)
{
  short ptvang;
  ptvang=(((long)mcs->pxms_ptirc*deg)/mcs->pxms_ptper+(2*90))/(4*90);
  return ptvang;
}

/**
 * pxmc_setup_ptable - Initializes phase tables
 * @mcs:	Motion controller state information
 * @ptprofile:	Pointer to precompiled phase table descriptor
 * @ptirc:	IRC count per phase table
 * @ptper:	Number of periods per table
 *
 * The function setups PXMC state fields related to the
 * phase table profiles according to provided precomputed
 * provile data @ptprofile. It sets @pxms_ptptr1, @pxms_ptptr2
 * and @pxms_ptptr3 and computes phase table scaling
 * quotient @pxms_ptscale_mult and shift/divisor @pxms_ptscale_shift
 * according to provided parameters and table length.
 */
int
pxmc_ptable_set_profile(pxmc_state_t *mcs, const pxmc_ptprofile_t *ptprofile,
                        unsigned int ptirc, unsigned int ptper)
{
  unsigned long scale_mult;
  unsigned long num;
  unsigned long den;
  unsigned int scale_shift;
  unsigned int adj_range;
  unsigned int adj_reserve=(sizeof(long)-sizeof(typeof(mcs->pxms_ptscale_mult)))*CHAR_BIT;

  if(adj_reserve<(sizeof(long)*CHAR_BIT)/2)
    adj_reserve=(sizeof(long)*CHAR_BIT)/2;

  if(!ptirc || !ptprofile)
    ptirc=mcs->pxms_ptirc;

  if(!ptirc)
    return -1;

  if(!ptper) {
    ptper=mcs->pxms_ptper;
    if(!ptirc)
      ptper=1;
  }

  if((ptirc!=mcs->pxms_ptirc)||(ptper!=mcs->pxms_ptper)) {
    pxmc_clear_flag(mcs,PXMS_PHA_b);
    pxmc_clear_flag(mcs,PXMS_PTI_b);
    mcs->pxms_ptindx=0;
  }

  num=ptprofile->ptirc*ptper;
  den=ptirc*ptprofile->ptper;

  scale_shift=clzl(num);

  num<<=scale_shift;
  scale_mult=(num+den/2)/den;
  adj_range=clzl(scale_mult);

  if(adj_range<adj_reserve){
    adj_range=adj_reserve-adj_range;
    scale_shift-=adj_range;
    scale_mult>>=adj_range-1;
    scale_mult++;
    scale_mult>>=1;
    if(clzl(scale_mult)<adj_reserve){
      scale_shift--;
      scale_mult>>=1;
    }
  } else if(adj_range>adj_reserve) {
    adj_range=adj_range-adj_reserve;
    scale_shift+=adj_range;
    scale_mult=num/den;
    scale_mult<<=adj_range;
    scale_mult+=(((num%den)<<adj_range)+den/2)/den;
  }

  mcs->pxms_ptptr1=(typeof(mcs->pxms_ptptr1))ptprofile->ptptr1;
  mcs->pxms_ptptr2=(typeof(mcs->pxms_ptptr2))ptprofile->ptptr2;
  mcs->pxms_ptptr3=(typeof(mcs->pxms_ptptr3))ptprofile->ptptr3;

  mcs->pxms_ptirc=ptirc;
  mcs->pxms_ptper=ptper;
  mcs->pxms_ptscale_mult=scale_mult;
  mcs->pxms_ptscale_shift=scale_shift;

  return 1;
}
