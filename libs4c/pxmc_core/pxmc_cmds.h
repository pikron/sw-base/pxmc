/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_cmds.h - generic multi axis motion controller
                descriptor lists for command processor

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_CMDS_H_
#define _PXMC_CMDS_H_

#include <cmd_proc.h>
#include "pxmc.h"

pxmc_state_t *cmd_opchar_getreg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_reg_rw_pos(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_reg_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_reg_long_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_regptmod_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

extern cmd_des_t const *const cmd_pxmc_base[];
extern cmd_des_t const *const cmd_pxmc_ptable[];
extern cmd_des_t const *const cmd_pxmc_deb[];
extern cmd_des_t const *const cmd_pxmc_misc[];


#endif /*_PXMC_CMDS_H_*/
