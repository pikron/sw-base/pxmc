/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  mo_psys2asm.c - helper source for automatic definitions
                  and C structures layout transformation
		  to assembly language format 
 
  Copyright (C) 2001-2003 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <string.h>
#include <system_def.h>
#include "pxmc.h"
#include "pxmc_gen_info.h"

/* Export of offset information to assembly source code */

/* %N (for N a digit) means print operand N in usual manner.
   %lN label name with no punctuation
   %cN constant expression with no punctuation
   %aN means expect operand N to be a memory address
       (not a memory reference!) and print a reference
       to that address.
   %nN constant expression for minus the value with no other punctuation. 
 */

#ifndef NUM2ASM

#ifdef __riscv
#define NUM2ASM_ASM_CONST ""
#else
#define NUM2ASM_ASM_CONST "c"
#endif

#define NUM2ASM(sym,val) \
    __asm__ ( \
	".set " #sym ",%" NUM2ASM_ASM_CONST "0\n" \
	".global " #sym "\n" \
	::"n"(val) \
    )
#endif /*NUM2ASM*/

#define OPXMS2ASM(fld) \
	NUM2ASM(_cmetric2def_o##fld,pxmc_state_offs(fld)); \
	NUM2ASM(_cmetric2def_s##fld,sizeof(((pxmc_state_t*)0L)->fld))

#define MASKBIT2ASM(def_base) \
	NUM2ASM(_cmetric2def_##def_base##_b,def_base##_b); \
	NUM2ASM(_cmetric2def_##def_base##_m,def_base##_m)

#define VALUE2ASM(value_name,value) \
	NUM2ASM(_cmetric2def_##value_name,value)

void pxmc_public2asm(void)
{
NUM2ASM(_size2asm_opxms_len,sizeof(pxmc_state_t));

OPXMS2ASM(pxms_flg);
OPXMS2ASM(pxms_do_inp);
OPXMS2ASM(pxms_do_con);
OPXMS2ASM(pxms_do_out);
OPXMS2ASM(pxms_do_deb);
OPXMS2ASM(pxms_do_gen);
OPXMS2ASM(pxms_do_ap2hw);
OPXMS2ASM(pxms_ap);
OPXMS2ASM(pxms_as);
#ifdef PXMC_WITH_FINE_GRAINED
OPXMS2ASM(pxms_rpfg);
#endif /*PXMC_WITH_FINE_GRAINED*/
OPXMS2ASM(pxms_rp);
OPXMS2ASM(pxms_rs);
#ifdef PXMC_WITH_FINE_GRAINED
OPXMS2ASM(pxms_rsfg);
#endif /*PXMC_WITH_FINE_GRAINED*/
#ifndef PXMC_WITH_FIXED_SUBDIV
OPXMS2ASM(pxms_subdiv);
#endif /*PXMC_WITH_FIXED_SUBDIV*/
OPXMS2ASM(pxms_md);
OPXMS2ASM(pxms_ms);
OPXMS2ASM(pxms_ma);
OPXMS2ASM(pxms_inp_info);
OPXMS2ASM(pxms_out_info);
OPXMS2ASM(pxms_ene);
OPXMS2ASM(pxms_erc);
OPXMS2ASM(pxms_p);
OPXMS2ASM(pxms_i);
OPXMS2ASM(pxms_d);
OPXMS2ASM(pxms_s1);
OPXMS2ASM(pxms_s2);
OPXMS2ASM(pxms_me);
OPXMS2ASM(pxms_foi);
OPXMS2ASM(pxms_fod);
OPXMS2ASM(pxms_tmp);
#ifdef PXMC_WITH_PHASE_TABLE
OPXMS2ASM(pxms_ptirc);
OPXMS2ASM(pxms_ptper);
OPXMS2ASM(pxms_ptofs);
OPXMS2ASM(pxms_ptshift);
OPXMS2ASM(pxms_ptvang);
OPXMS2ASM(pxms_ptindx);
OPXMS2ASM(pxms_ptptr1);
OPXMS2ASM(pxms_ptptr2);
OPXMS2ASM(pxms_ptptr3);
OPXMS2ASM(pxms_pwm1cor);
OPXMS2ASM(pxms_pwm2cor);
OPXMS2ASM(pxms_pwm3cor);
#endif /*PXMC_WITH_PHASE_TABLE*/
#ifdef PXMC_WITH_CURRENTFB
OPXMS2ASM(pxms_curfb_acum);
OPXMS2ASM(pxms_curfb_acur);
OPXMS2ASM(pxms_curfb_act);
OPXMS2ASM(pxms_curfb_acr);
OPXMS2ASM(pxms_curfb_p);
OPXMS2ASM(pxms_curfb_i);
OPXMS2ASM(pxms_curfb_ir);
OPXMS2ASM(pxms_curfb_foi);
OPXMS2ASM(pxms_curfb_out);
#endif /*PXMC_WITH_CURRENTFB*/
OPXMS2ASM(pxms_errno);
OPXMS2ASM(pxms_cfg);
OPXMS2ASM(pxms_ep);
OPXMS2ASM(pxms_gen_st);
OPXMS2ASM(pxms_gen_info);

OPXMS2ASM(pxms_gen_tep);
OPXMS2ASM(pxms_gen_tac);
OPXMS2ASM(pxms_gen_tsp);
#ifdef pxms_gen_tspfg
OPXMS2ASM(pxms_gen_tspfg);
#endif
OPXMS2ASM(pxms_gen_ttp);
OPXMS2ASM(pxms_gen_ttpfr);

OPXMS2ASM(pxms_gen_spd_next);
OPXMS2ASM(pxms_gen_spd_ac);
OPXMS2ASM(pxms_gen_spd_sp);
#ifdef pxms_gen_spd_spfg
OPXMS2ASM(pxms_gen_spd_spfg);
#endif
OPXMS2ASM(pxms_gen_spd_timeout);

OPXMS2ASM(pxms_gen_htim);
OPXMS2ASM(pxms_gen_hcfg);
OPXMS2ASM(pxms_gen_hac);
OPXMS2ASM(pxms_gen_hsp);

MASKBIT2ASM(PXMS_ENI);
MASKBIT2ASM(PXMS_ENR);
MASKBIT2ASM(PXMS_ENG);
MASKBIT2ASM(PXMS_ERR);
MASKBIT2ASM(PXMS_BSY);
MASKBIT2ASM(PXMS_DBG);
MASKBIT2ASM(PXMS_CMV);
MASKBIT2ASM(PXMS_CQF);
MASKBIT2ASM(PXMS_PHA);
MASKBIT2ASM(PXMS_PTI);

MASKBIT2ASM(PXMS_CFG_HDIR);
MASKBIT2ASM(PXMS_CFG_HRI);
MASKBIT2ASM(PXMS_CFG_HMC);
MASKBIT2ASM(PXMS_CFG_HLS);
MASKBIT2ASM(PXMS_CFG_HPS);
MASKBIT2ASM(PXMS_CFG_SMTH);
MASKBIT2ASM(PXMS_CFG_MD2E);
MASKBIT2ASM(PXMS_CFG_CYCL);
VALUE2ASM(PXMS_CFG_HSPD_m,PXMS_CFG_HSPD_m);

#ifdef PXMC_WITH_PHASE_TABLE
VALUE2ASM(PXMC_WITH_PHASE_TABLE,1);
VALUE2ASM(PXMS_E_COMM,PXMS_E_COMM);
#endif /*PXMC_WITH_PHASE_TABLE*/
};

