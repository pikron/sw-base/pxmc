/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_gen_info.h - generic multi axis motion controller
                    information fields allocation for generators

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_GEN_INFO_H_
#define _PXMC_GEN_INFO_H_

/* the fields allocation for :
 *  - pxmc_trp_gi
 */

#define pxms_gen_tep pxms_gen_info[0]	/* copied opxms_ep */
#define pxms_gen_tac pxms_gen_info[1]	/* copied opxms_ma */
#define pxms_gen_tsp pxms_gen_info[2]	/* copied opxms_ms */
#define pxms_gen_tspfg pxms_gen_info[3]	/* fine grained speed */
#define pxms_gen_ttp pxms_gen_info[4]	/* triggering position */
#define pxms_gen_ttpfr pxms_gen_info[5]	/* fractions */

/* the fields allocation for :
 *  - pxmc_stop_gi
 *  - pxmc_spd_gi
 *  - pxmc_spdnext_gi
 */

#define pxms_gen_spd_next pxms_gen_info[0]	/* next generator */
#define pxms_gen_spd_ac pxms_gen_info[1]	/* copied opxms_ma */
#define pxms_gen_spd_sp pxms_gen_info[2]	/* final speed	  */
#define pxms_gen_spd_spfg pxms_gen_info[3]	/* fine grained speed */
#define pxms_gen_spd_timeout pxms_gen_info[4]	/* timeout	  */

/* the fields allocation for :
 *  - pxmc_hh_gi
 *  - pxmc_fast_hh_gi
 */

#define pxms_gen_htim pxms_gen_info[0]		/* timer */
#define pxms_gen_hcfg pxms_gen_info[1]		/* copied opxms_cfg */
#define pxms_gen_hac pxms_gen_info[2]		/* copied opxms_ma */
#define pxms_gen_hsp pxms_gen_info[3]		/* copied opxms_ms */


#endif /*_PXMC_GEN_INFO_H_*/
