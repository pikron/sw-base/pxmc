/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_misccmds.c - generic multi axis motion controller
             miscellaneous commands for command processor

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>
#include "pxmc.h"
#include "pxmc_cmds.h"

#if 0
int cmd_do_clr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_axis_release(mcs);
  /* Set actual position to zero */
  pxmc_axis_set_pos(mcs,0);
  mcs->pxms_ep=0;
  return 0;
}
#endif

int cmd_do_status_bsybits(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int val;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    val=0;
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs)
        val|=(mcs->pxms_flg&PXMS_BSY_b)?1<<i:0;
    }
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0;
}

int cmd_do_axst_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  unsigned chan;
  if(!param[1])
  {
    for(chan=0;chan<pxmc_main_list.pxml_cnt;chan++){
      mcs=pxmc_main_list.pxml_arr[chan];
      if(mcs){
        printf("mcs%c flg=0x%04x ap=%8d rp=%8d as=%4d rs=%4d ene=%6d\n",
                chan+'A',(int)mcs->pxms_flg,
                (int)(mcs->pxms_ap>>PXMC_SUBDIV(mcs)),
                (int)(mcs->pxms_rp>>PXMC_SUBDIV(mcs)),
                (int)(mcs->pxms_as>>PXMC_SUBDIV(mcs)),
                (int)(mcs->pxms_rs>>PXMC_SUBDIV(mcs)),
                (int)(mcs->pxms_ene)
              );
      }
    }
  }else{
    chan=*param[1]-'A';
    if(chan>=pxmc_main_list.pxml_cnt) {
      chan=*param[1]-'a';
      if(chan>=pxmc_main_list.pxml_cnt)
        return -CMDERR_BADREG;
    }
    mcs=pxmc_main_list.pxml_arr[chan];
    if(!mcs) return -CMDERR_BADREG;
    printf("mcs%c flg=0x%04x ap=%8d rp=%8d as=%4d rs=%4d ene=%6d\n",
            chan+'A',(int)mcs->pxms_flg,
            (int)(mcs->pxms_ap>>PXMC_SUBDIV(mcs)),
            (int)(mcs->pxms_rp>>PXMC_SUBDIV(mcs)),
            (int)(mcs->pxms_as>>PXMC_SUBDIV(mcs)),
            (int)(mcs->pxms_rs>>PXMC_SUBDIV(mcs)),
            (int)(mcs->pxms_ene)
          );

    printf("     p=%4d i=%4d d=%4d me=%4d foi=%5d fod=%5d erc=%5d\n",
            (int)(mcs->pxms_p),(int)(mcs->pxms_i),(int)(mcs->pxms_d),
            (int)(mcs->pxms_me),(int)(mcs->pxms_foi),(int)(mcs->pxms_fod),
            (int)(mcs->pxms_erc)
          );
    printf("     md=%4d ms=%4d ma=%4d tmp=%6d\n",
            (int)(mcs->pxms_md>>PXMC_SUBDIV(mcs)),
            (int)(mcs->pxms_ms),
            (int)(mcs->pxms_ma),
            (int)(mcs->pxms_tmp)
          );
  }
  return 0;
}

int cmd_do_cer_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    if(mcs->pxms_flg&PXMS_ERR_m){
      pxmc_axis_release(mcs);
     #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
      pxmc_clear_flags(mcs,PXMS_ERR_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
     #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
      pxmc_clear_flag(mcs,PXMS_ENG_m);
      pxmc_clear_flag(mcs,PXMS_ENR_m);
      pxmc_clear_flag(mcs,PXMS_BSY_m);
      pxmc_clear_flag(mcs,PXMS_ERR_m);
     #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    }
  }
  /*statchk_power_stop=0*/
  pxmc_clear_power_stop();
  return 0;
}

int cmd_do_clr_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    pxmc_axis_release(mcs);
   #ifndef PXMC_WITH_FLAGS_BYBITS_ONLY
    pxmc_clear_flags(mcs,PXMS_ERR_m|PXMS_ENR_m|PXMS_BSY_m);
   #else /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    pxmc_clear_flag(mcs,PXMS_ENR_m);
    pxmc_clear_flag(mcs,PXMS_BSY_m);
    pxmc_clear_flag(mcs,PXMS_ERR_m);
   #endif /*PXMC_WITH_FLAGS_BYBITS_ONLY*/
    /* Set actual position to zero */
    pxmc_axis_set_pos(mcs,0);
    mcs->pxms_ep=0;
  }
  /*statchk_power_stop=0*/
  pxmc_clear_power_stop();
  return 0;
}

#if 0
int cmd_do_status_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int val;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    val=0;
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs)
        val|=mcs->pxms_flg;
    }
    if(pxmc_coordmv_checkst(&pxmc_coordmv_state)>=2)
      val|=PXMS_CQF_m;	/* coordinator coomand queue full */
    if(trap_pc_addr)
      val|=0x1000000;
    if(statchk_power_stop)
      val|=0x20000;
    if(statchk_power_off)
      val|=0x10000;
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0;
}
#endif

int cmd_do_setap(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  pxmc_state_t *mcs;
  long val;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val<<=PXMC_SUBDIV(mcs);

  /* Set new actual position */
  if(pxmc_axis_set_pos(mcs,val)<0) return -1;
  return 0;
}

#if 0
int cmd_do_reg_type(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  /* should set controller structure */
  return 0;
}
#endif

#ifdef WITH_SFI_SEL
/**
 * cmd_do_regsfrq - sets or returns smapling frequency
 */
int cmd_do_regsfrq(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    return pxmc_sfi_sel(val);
  }else{
    return cmd_opchar_replong(cmd_io, param, pxmc_get_sfi_hz(NULL), 0, 0);
  }
  return 0;
}
#endif /* WITH_SFI_SEL */

#if 0
cmd_des_t const cmd_des_align={0, CDESM_OPCHR,
			"ALIGN?","align commutator",cmd_do_align,{}};
#endif

#if 0
cmd_des_t const cmd_des_r_one={0, CDESM_OPCHR,
			"R?","send R?! or FAIL?! at axis finish",cmd_do_r_one,{}};
#endif

#if 0
cmd_des_t const cmd_des_axis_mode={0, CDESM_OPCHR|CDESM_WR,
			"REGMODE?","axis working mode",cmd_do_axis_mode,
			{}};
#endif
#ifdef WITH_SFI_SEL
cmd_des_t const cmd_des_regsfrq={0, CDESM_OPCHR|CDESM_RW,
			"REGSFRQ","set new sampling frequency",cmd_do_regsfrq,{}};
#endif /* WITH_SFI_SEL */
/* axes debugging and tuning */
#if 0
cmd_des_t const cmd_des_reg_type={0, CDESM_OPCHR|CDESM_RW,
			"REGTYPE?","unused",cmd_do_reg_type,{}};
#endif
cmd_des_t const cmd_des_axst_all={0, 0,"axst","controlers status",cmd_do_axst_all,{}};

cmd_des_t const *const cmd_pxmc_misc[]={
//  &cmd_des_align,
   /*&cmd_des_r_one,*/
//  &cmd_des_axis_mode,
//  &cmd_des_reg_type,
#ifdef WITH_SFI_SEL
  &cmd_des_regsfrq,
#endif /* WITH_SFI_SEL */
  &cmd_des_axst_all,
   NULL
};
