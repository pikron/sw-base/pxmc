/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_hh_basic.c - generic multi axis motion controller
                  hard home finding support

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <pxmcbsp.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"

int pxmc_hh_gd10(pxmc_state_t *mcs);
int pxmc_hh_gd20(pxmc_state_t *mcs);

int
pxmc_hh_gi(pxmc_state_t *mcs)
{
  long spd;
  pxmc_set_flag(mcs,PXMS_BSY_b);
  mcs->pxms_rsfg=0;
  spd=mcs->pxms_ms;
  spd>>=mcs->pxms_cfg&PXMS_CFG_HSPD_m;
  if(!spd) spd=1;
  if(mcs->pxms_cfg&PXMS_CFG_HDIR_m)
    spd=-spd;
  mcs->pxms_gen_hsp=spd;
  mcs->pxms_do_gen=pxmc_hh_gd10;
  return pxmc_hh_gd10(mcs);
}

int
pxmc_hh_gd10(pxmc_state_t *mcs)
{
  long spd;
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  pxmc_spd_gacc(&(mcs->pxms_rs),mcs->pxms_gen_hsp,mcs->pxms_ma);
  mcs->pxms_rp+=mcs->pxms_rs;

  if(!pxmcbsp_is_home(mcs)){
    spd=mcs->pxms_gen_hsp;
    if(spd>0){
      spd>>=2;
      spd=spd?-spd:-1;
    }else{
      spd>>=2;
      spd=spd?-spd:1;
    }
    mcs->pxms_gen_hsp=spd;
    mcs->pxms_do_gen=pxmc_hh_gd20;
  }

  return 0;
}

int
pxmc_hh_gd20(pxmc_state_t *mcs)
{
  if(mcs->pxms_flg&PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  pxmc_spd_gacc(&(mcs->pxms_rs),mcs->pxms_gen_hsp,mcs->pxms_ma);
  mcs->pxms_rp+=mcs->pxms_rs;

  if(pxmcbsp_is_home(mcs)){
    pxmc_axis_set_pos(mcs,0);
    mcs->pxms_do_gen=pxmc_stop_gi;
  }

  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_hh_gi;
}
