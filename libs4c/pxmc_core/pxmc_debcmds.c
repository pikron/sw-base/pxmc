/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_debcmds.c - generic multi axis motion controller
             debugging commands for command processor

  (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>
#include "pxmc.h"
#include "pxmc_cmds.h"

int cmd_do_reg_dbgset(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  pxmc_state_t *mcs;
  long val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  if(opchar==':'){
    p=param[3];
    if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;
    pxmc_dbgset(mcs,NULL,val);
  }else{
    cmd_io_write(cmd_io,param[0],param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,mcs->pxms_flg&PXMS_DBG_m?'1':'0');
  }
  return 0; 
}

int cmd_do_reg_dbgpre(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_dbg_hist_t *hist;
  long count, val;
  int i;
  int opchar;
  char *ps;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ps=param[3];
  if(si_long(&ps,&count,0)<0) return -CMDERR_BADPAR;
  if(!count||(count>0x10000)) return -CMDERR_BADPAR;
  pxmc_dbg_histfree(NULL);
  if((hist=pxmc_dbg_histalloc(count+2))==NULL) return -CMDERR_NOMEM;
  for(i=0;i<count;i++){
    ps=cmd_io_line_rdline(cmd_io, 1);
    if(ps==NULL) return -CMDERR_BADPAR;
    if(si_long(&ps,&val,0)<0) return -CMDERR_BADPAR;
    hist->buff[i]=val;
  }
  pxmc_dbg_hist=hist;
  return 0;
}

int cmd_do_reg_dbghis(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_dbg_hist_t *hist;
  long count, val;
  int i, opchar;
  char *ps;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ps=param[3];
  if(si_long(&ps,&count,0)<0) return -CMDERR_BADPAR;
  if(!count||(count>0x10000)) return -CMDERR_BADPAR;
  hist=pxmc_dbg_hist;
  for(i=0;i<count;i++){
    if(hist&&(&hist->buff[i]<hist->end))
      val=hist->buff[i];
    else
      val=0;
    printf("%ld\r\n",val);
  }
  return 0;
}

int cmd_do_reg_dbggnr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    if(mcs&&(mcs->pxms_flg&PXMS_DBG_m)){
      if(pxmc_dbg_gnr(mcs)<0){
	for(i=0;i<pxmc_main_list.pxml_cnt;i++){
	  mcs=pxmc_main_list.pxml_arr[i];
	  if(mcs&&(mcs->pxms_flg&PXMS_DBG_m))
              pxmc_stop(pxmc_main_list.pxml_arr[i],0);
        }
        return -CMDERR_BADREG;
      }
    }
  }
  pxmc_dbg_hist->ptr=pxmc_dbg_hist->buff;  
  return 0; 
}

cmd_des_t const cmd_des_reg_dbgset={0, CDESM_OPCHR|CDESM_RW,
			"REGDBG?","sets debug flag",cmd_do_reg_dbgset,{}};
cmd_des_t const cmd_des_reg_dbgpre={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGPRE","store reference course",cmd_do_reg_dbgpre,{}};
cmd_des_t const cmd_des_reg_dbghis={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGHIS","read history course",cmd_do_reg_dbghis,{}};
cmd_des_t const cmd_des_reg_dbggnr={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGGNR","controller response to HIST course",cmd_do_reg_dbggnr,{}};

cmd_des_t const *const cmd_pxmc_deb[]={
  &cmd_des_reg_dbgset,
  &cmd_des_reg_dbgpre,
  &cmd_des_reg_dbghis,
  &cmd_des_reg_dbggnr,
  NULL
};
