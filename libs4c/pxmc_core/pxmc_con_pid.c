/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_con_pid.c - generic multi axis motion controller
                   basic PID controller

  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"

/**
 * pxmc_pid_con - Position PID Controller
 * @mcs:	Motion controller state information
 */
int
pxmc_pid_con(pxmc_state_t *mcs)
{
  long dp, ds, pd, i;
  short ene;
  dp=mcs->pxms_rp-mcs->pxms_ap;
  ds=mcs->pxms_rs-mcs->pxms_as;
  if((dp>mcs->pxms_md)||(dp<-mcs->pxms_md)) {
    if(mcs->pxms_cfg&PXMS_CFG_MD2E_m) {
      pxmc_set_errno(mcs,PXMS_E_MAXPD);
      mcs->pxms_ene=0;
      return 0;
    }
    if(dp>0) dp=mcs->pxms_md;
      else dp=-mcs->pxms_md;
  }
  dp>>=PXMC_SUBDIV(mcs);
  ds>>=PXMC_SUBDIV(mcs);

  pd=(short)dp*(long)mcs->pxms_p+(short)ds*(long)mcs->pxms_d;
  if(pd>0) {
    if(pd>mcs->pxms_me) pd=mcs->pxms_me;
    i=((short)pd*(long)mcs->pxms_i + (1L<<11))>>12;
    i+=mcs->pxms_foi;
    if(i>mcs->pxms_me) i=mcs->pxms_me;
    mcs->pxms_foi=i;
    ene=pd+i;
  } else if(pd<0) {
    if(pd<-mcs->pxms_me) pd=-mcs->pxms_me;
    i=((short)pd*(long)mcs->pxms_i + (1L<<11))>>12;
    i+=mcs->pxms_foi;
    if(i<-mcs->pxms_me) i=-mcs->pxms_me;
    mcs->pxms_foi=i;
    ene=pd+i;
  } else {

    ene=mcs->pxms_foi;
  }

  if(ene>mcs->pxms_me) {
    ene=mcs->pxms_me;
    if(!mcs->pxms_as)
      if((mcs->pxms_erc+=0x80)<0) {
        pxmc_set_errno(mcs,PXMS_E_OVERL);
	ene=0;
      }
  } else if(ene<-mcs->pxms_me) {
    ene=-mcs->pxms_me;
    if(!mcs->pxms_as)
      if((mcs->pxms_erc+=0x80)<0) {
        pxmc_set_errno(mcs,PXMS_E_OVERL);
	ene=0;
      }
  } else { mcs->pxms_erc=0; }

  mcs->pxms_ene=ene;

  return 0;
}
