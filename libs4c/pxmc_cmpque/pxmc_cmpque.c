/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_cmpque.c - generic multi axis motion controller interface
                  support for queues of request for comparator

  (C) 2001-2007 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2007 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <string.h>
#include <malloc.h>

#include <cpu_def.h>

#include <pxmc.h>
#include <pxmc_cmpque.h>
#include <ul_list.h>
#include <ul_gavl.h>
#include <ul_gavlflesint.h>

typedef struct {
 ul_list_head_t queues_list;
} pxmc_cmpque_all_queues_t;

pxmc_cmpque_all_queues_t pxmc_cmpque_all_queues;
char pxmc_cmpque_bottom_needed;

typedef int (*pxmc_cmpque_pos_cmp_fnc_t)(const long *a, const long *b);

static inline int
pxmc_cmpque_pos_cmp_acyclic(const long *a, const long *b) __attribute__ ((pure));

static inline int
pxmc_cmpque_pos_cmp_cyclic(const long *a, const long *b) __attribute__ ((pure));

static inline int
pxmc_cmpque_pos_cmp_acyclic(const long *a, const long *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

static inline int
pxmc_cmpque_pos_cmp_cyclic(const long *a, const long *b)
{
  if (*a==*b) return 0;
  if (ul_cyclic_gt(*a,*b)) return 1;
  return -1;
}

#define pxmc_cmpque_pos_cmp4gavl(a, b) \
( \
  (root->mcs->pxms_cfg & PXMS_CFG_CYCL_m) ? \
    pxmc_cmpque_pos_cmp_cyclic((a), (b)) : \
    pxmc_cmpque_pos_cmp_acyclic((a), (b)) \
)

GAVL_FLES_INT_DEC(pxmc_cmpque_tree, pxmc_cmpque_queue_t, pxmc_cmpque_entry_t, long,
		tree_queue_root, tree_queue_node, cmp_pos, pxmc_cmpque_pos_cmp4gavl)

GAVL_FLES_INT_IMP(pxmc_cmpque_tree, pxmc_cmpque_queue_t, pxmc_cmpque_entry_t, long,
		tree_queue_root, tree_queue_node, cmp_pos, pxmc_cmpque_pos_cmp4gavl,
		GAVL_FAFTER, , ,)

UL_LIST_CUST_DEC(pxmc_cmpque_linear,  pxmc_cmpque_queue_t, pxmc_cmpque_entry_t,
		 linear_queue_carrier, linear_queue_node)

UL_LIST_CUST_DEC(pxmc_cmpque_linear4sentinel,  pxmc_cmpque_queue_t, pxmc_cmpque_entry_t,
		 linear_queue_sentinel, linear_queue_node)

UL_LIST_CUST_DEC(pxmc_cmpque_deactivated,  pxmc_cmpque_queue_t, pxmc_cmpque_entry_t,
		 deactivated_queue, linear_queue_node)

UL_LIST_CUST_DEC(pxmc_cmpque_done,  pxmc_cmpque_queue_t, pxmc_cmpque_entry_t,
		 done_queue, linear_queue_node)

UL_LIST_CUST_DEC(pxmc_cmpque_all_queues,  pxmc_cmpque_all_queues_t, pxmc_cmpque_queue_t,
		 queues_list, all_queues_node)

#if FOR_RTEMS

#define pxmc_cmpque_lock_local_declare(cookie) \
  rtems_interrupt_level cookie;

#define pxmc_cmpque_lock(cookie) \
  rtems_interrupt_disable(cookie)

#define pxmc_cmpque_unlock(cookie) \
  rtems_interrupt_enable(cookie)

#else /*FOR_RTEMS*/

#define pxmc_cmpque_lock_local_declare(cookie) \
  unsigned long cookie;

#define pxmc_cmpque_lock(cookie) \
  save_and_cli(cookie)

#define pxmc_cmpque_unlock(cookie) \
  restore_flags(cookie)

#endif /*FOR_RTEMS*/

pxmc_cmpque_entry_t *
pxmc_cmpque_entry_first(pxmc_cmpque_queue_t *queue)
{
  return pxmc_cmpque_tree_first(queue);
}

pxmc_cmpque_entry_t *
pxmc_cmpque_entry_last(pxmc_cmpque_queue_t *queue)
{
  return pxmc_cmpque_tree_last(queue);
}

pxmc_cmpque_entry_t *
pxmc_cmpque_entry_next(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry)
{
  return pxmc_cmpque_tree_next(queue, entry);
}

pxmc_cmpque_entry_t *
pxmc_cmpque_entry_prev(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry)
{
  return pxmc_cmpque_tree_prev(queue, entry);
}

static inline
int pxmc_cmpque_deactivate(pxmc_cmpque_queue_t *queue,
                      pxmc_cmpque_entry_t *entry, int code)
{
  pxmc_cmpque_lock_local_declare(lock_save)

  pxmc_cmpque_lock(lock_save);
  entry->active_fl = 0;
  pxmc_cmpque_linear_del_item(entry);
  pxmc_cmpque_deactivated_insert(queue, entry);
  pxmc_cmpque_unlock(lock_save);
  pxmc_cmpque_bottom_needed = 1;
  return 0;
}

static inline
int pxmc_cmpque_premature_done(pxmc_cmpque_queue_t *queue,
                      pxmc_cmpque_entry_t *entry, int done_code)
{
  int already_done;
  pxmc_cmpque_lock_local_declare(lock_save)

  pxmc_cmpque_lock(lock_save);
  entry->active_fl = 0;
  pxmc_cmpque_linear_del_item(entry);
  already_done = entry->done_code;
  if(!already_done)
    entry->done_code = done_code;
  pxmc_cmpque_unlock(lock_save);
  if(!already_done)
    pxmc_cmpque_tree_delete(queue, entry);
  pxmc_cmpque_done_insert(queue, entry);
  pxmc_cmpque_bottom_needed = 1;
  return already_done;
}

void pxmc_cmpque_deactivated2done(pxmc_cmpque_queue_t *queue)
{
  pxmc_cmpque_entry_t *entry;
  long act_pos;
  pxmc_cmpque_lock_local_declare(lock_save)
  const unsigned long ulong_max=~0L;

  do {
    pxmc_cmpque_lock(lock_save);
    entry=pxmc_cmpque_deactivated_cut_first(queue);
    if(!entry->done_code)
      entry->done_code = PXMC_CMPQUE_DONE_NORMAL;
    pxmc_cmpque_unlock(lock_save);
    if(!entry)
      break;
    pxmc_cmpque_tree_delete(queue, entry);
    pxmc_cmpque_done_insert(queue, entry);
    pxmc_cmpque_bottom_needed = 1;
  } while(1);

  if(!(queue->mcs->pxms_cfg & PXMS_CFG_CYCL_m))
    return;

  act_pos = queue->mcs->pxms_ap;

  do {
    int diff;
    entry = pxmc_cmpque_tree_first(queue);
    if(!entry)
      break;
    diff = entry->cmp_pos - act_pos;
    if((diff>=0) || (-diff < (ulong_max/4) + (ulong_max/8)))
      break;
    pxmc_cmpque_premature_done(queue, entry, PXMC_CMPQUE_DONE_BYLIMIT);
  } while(1);

  do {
    int diff;
    entry = pxmc_cmpque_tree_last(queue);
    if(!entry)
      break;
    diff = entry->cmp_pos - act_pos;
    if((diff<=0) || (diff < (ulong_max/4) + (ulong_max/8)))
      break;
    pxmc_cmpque_premature_done(queue, entry, PXMC_CMPQUE_DONE_BYLIMIT);
  } while(1);
}

int pxmc_cmpque_insert(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry)
{
  int res;
  int cyclic = queue->mcs->pxms_cfg & PXMS_CFG_CYCL_m;
  pxmc_cmpque_pos_cmp_fnc_t cmp_fnc;
  int succeed = 0;
  pxmc_cmpque_lock_local_declare(lock_save)

  entry->done_code = 0;
  entry->cmp_code = 0;
  entry->queue = queue;

  res = pxmc_cmpque_tree_insert(queue, entry);
  if(res<0) {
    entry->queue = NULL;
    return -1;
  }

  if(cyclic)
    cmp_fnc = pxmc_cmpque_pos_cmp_cyclic;
  else
    cmp_fnc = pxmc_cmpque_pos_cmp_acyclic;

  do {
    pxmc_cmpque_entry_t *next_entry;
    pxmc_cmpque_entry_t *prev_entry;
    ul_list_node_t *next_node;
    ul_list_node_t *prev_node;
    unsigned char active_check_dummy = 1;
    unsigned char *active_check_ptr = &active_check_dummy;
    long act_pos;

    __memory_barrier();

    next_entry = entry;
    do {
      next_entry = pxmc_cmpque_tree_next(queue, next_entry);
      if(!next_entry) {
        next_node = &queue->linear_queue_sentinel;
	break;
      }
      if(next_entry->active_fl) {
        active_check_ptr = &next_entry->active_fl;
        next_node = &next_entry->linear_queue_node;
	break;
      }
    } while(1);

    prev_entry = entry;
    do {
      prev_entry = pxmc_cmpque_tree_prev(queue, prev_entry);
      if(!prev_entry) {
        prev_node = &queue->linear_queue_sentinel;
	break;
      }
      if(prev_entry->active_fl) {
        active_check_ptr = &prev_entry->active_fl;
        prev_node = &prev_entry->linear_queue_node;
	break;
      }
    } while(1);

    if(next_node->prev == &queue->linear_queue_carrier) {
      act_pos = queue->mcs->pxms_ap;
      if(cmp_fnc(&act_pos, &entry->cmp_pos) > 0)
        next_node = &queue->linear_queue_carrier;
      else
        prev_node = &queue->linear_queue_carrier;
    }

    __memory_barrier();

    pxmc_cmpque_lock(lock_save);
    if((prev_node->next == next_node) &&
       (next_node->prev == prev_node) && *active_check_ptr) {
      __list_add(&entry->linear_queue_node, prev_node, next_node);
      entry->active_fl = 1;
      succeed = 1;
    }
    pxmc_cmpque_unlock(lock_save);

  } while(!succeed);

  if(entry->cmp_req & PXMC_CMPQUE_CMP_NOMISS) do {
    long act_pos;
    int cmp_code;
    int already_done = 0;
    pxmc_cmpque_lock_local_declare(lock_save)

    __memory_barrier();

    act_pos = queue->mcs->pxms_ap;

    if(cmp_fnc(&act_pos, &entry->cmp_pos) > 0) {
      if(!(entry->cmp_req & PXMC_CMPQUE_CMP_GT))
        break;
    } else {
      if(!(entry->cmp_req & PXMC_CMPQUE_CMP_LT))
        break;
    }

    pxmc_cmpque_lock(lock_save);
    cmp_code = entry->cmp_code;
    if(!cmp_code) {
      already_done = entry->done_code;
      if(!already_done) {
        entry->done_code = PXMC_CMPQUE_DONE_BYMISS;
        entry->active_fl = 0;
        pxmc_cmpque_linear_del_item(entry);
      }
    }
    pxmc_cmpque_unlock(lock_save);

    if(!cmp_code) {
      if(!already_done) {
        pxmc_cmpque_tree_delete(queue, entry);
        return -1;
      }
    }
  } while(0);

  return 0;
}

int pxmc_cmpque_delete(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry)
{
  int already_done;
  pxmc_cmpque_lock_local_declare(lock_save)

  pxmc_cmpque_lock(lock_save);
  entry->active_fl = 0;
  pxmc_cmpque_linear_del_item(entry);
  already_done = entry->done_code;
  if(!already_done)
    entry->done_code = PXMC_CMPQUE_DONE_BYUSER;
  pxmc_cmpque_unlock(lock_save);

  if(!already_done)
    pxmc_cmpque_tree_delete(queue, entry);

  return already_done;
}

pxmc_cmpque_queue_t *pxmc_cmpque4mcs(pxmc_state_t *mcs, int alloc_if_needed)
{
  pxmc_cmpque_queue_t *queue;
  pxmc_cmpque_lock_local_declare(lock_save)

  if(pxmc_cmpque_all_queues.queues_list.next) {
    ul_list_for_each(pxmc_cmpque_all_queues, &pxmc_cmpque_all_queues, queue) {
      if (queue->mcs == mcs)
        return queue;
    }
  }

  if (!alloc_if_needed)
    return NULL;

  queue = malloc(sizeof(*queue));
  if(!queue)
    return NULL;

  pxmc_cmpque_tree_init_root_field(queue);
  pxmc_cmpque_deactivated_init_head(queue);
  pxmc_cmpque_done_init_head(queue);

  pxmc_cmpque_linear_init_head(queue);
  list_add(&queue->linear_queue_sentinel, &queue->linear_queue_carrier);

  queue->mcs = mcs;

  if(!pxmc_cmpque_all_queues.queues_list.next)
    pxmc_cmpque_all_queues_init_head(&pxmc_cmpque_all_queues);

  pxmc_cmpque_lock(lock_save);
  pxmc_cmpque_all_queues_insert(&pxmc_cmpque_all_queues, queue);
  pxmc_cmpque_unlock(lock_save);

  return queue;
}

void do_pxmc_cmpque(void)
{
  pxmc_cmpque_queue_t *queue;

  if(!pxmc_cmpque_all_queues.queues_list.next)
    return;

  ul_list_for_each(pxmc_cmpque_all_queues, &pxmc_cmpque_all_queues, queue) {
    pxmc_state_t *mcs = queue->mcs;
    int cyclic = mcs->pxms_cfg & PXMS_CFG_CYCL_m;
    long act_pos = mcs->pxms_ap;
    pxmc_cmpque_entry_t *entry;
    int cmp_code = 0;
    int cmp_match;

    if(queue->linear_queue_carrier.next != &queue->linear_queue_sentinel) {
      entry = pxmc_cmpque_linear_node2item(queue, queue->linear_queue_carrier.next);

      if((cyclic? pxmc_cmpque_pos_cmp_cyclic(&act_pos, &entry->cmp_pos):
                  pxmc_cmpque_pos_cmp_acyclic(&act_pos, &entry->cmp_pos)) > 0) {
        cmp_code = PXMC_CMPQUE_CMP_GT;
      }
    }
    if(!cmp_code && (queue->linear_queue_carrier.prev != &queue->linear_queue_sentinel)) {
      entry = pxmc_cmpque_linear_node2item(queue, queue->linear_queue_carrier.prev);
      if((cyclic? pxmc_cmpque_pos_cmp_cyclic(&act_pos, &entry->cmp_pos):
                  pxmc_cmpque_pos_cmp_acyclic(&act_pos, &entry->cmp_pos)) < 0) {
        cmp_code = PXMC_CMPQUE_CMP_LT;
      }
    }
    if(!cmp_code)
      continue;

    cmp_match = entry->cmp_req & cmp_code;

    pxmc_cmpque_linear_del_item(entry);
    if((entry->cmp_req & PXMC_CMPQUE_CMP_SINGLE) && cmp_match) {
      entry->active_fl = 0;
      pxmc_cmpque_deactivated_insert(queue, entry);
      pxmc_cmpque_bottom_needed = 1;
    } else {
      if(cmp_code == PXMC_CMPQUE_CMP_GT)
        pxmc_cmpque_linear_ins_tail(queue, entry);
      else
        pxmc_cmpque_linear_ins_head(queue, entry);
    }
    if(cmp_match) {
      entry->cmp_code |= cmp_code;
      entry->fnc(entry, cmp_code);
    }
    break;
  }
}

int pxmc_cmpque_bottom(void)
{
  pxmc_cmpque_queue_t *queue;
  int done_code;

  pxmc_cmpque_bottom_needed = 0;
  __memory_barrier();

  if(!pxmc_cmpque_all_queues.queues_list.next)
    return 0;

  ul_list_for_each(pxmc_cmpque_all_queues, &pxmc_cmpque_all_queues, queue) {
    if(!pxmc_cmpque_deactivated_is_empty(queue))
      pxmc_cmpque_deactivated2done(queue);
  }

  ul_list_for_each(pxmc_cmpque_all_queues, &pxmc_cmpque_all_queues, queue) {
    pxmc_cmpque_entry_t *entry;
    entry = pxmc_cmpque_done_cut_first(queue);
    if(!entry)
      continue;

    done_code = entry->done_code;
    if(!done_code)
      done_code = PXMC_CMPQUE_DONE_UNKNOWN;
    entry->fnc(entry, done_code);
    pxmc_cmpque_bottom_needed = 1;
    return 1;
  }
  return 0;
}
