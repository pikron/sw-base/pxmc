/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_cmpque.h - generic multi axis motion controller interface
                  support for queues of request for comparator

  (C) 2001-2007 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2007 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_CMPQUE_H_
#define _PXMC_CMPQUE_H_

#include <pxmc.h>
#include <ul_list.h>
#include <ul_list.h>
#include <ul_gavl.h>

typedef struct pxmc_cmpque_entry  pxmc_cmpque_entry_t;
typedef struct pxmc_cmpque_queue  pxmc_cmpque_queue_t;

typedef int (*pxmc_cmpque_fnc_t)(pxmc_cmpque_entry_t *entry, int code);

#define PXMC_CMPQUE_CMP_GT        0x01
#define PXMC_CMPQUE_CMP_LT        0x02
#define PXMC_CMPQUE_CMP_NOMISS    0x04
#define PXMC_CMPQUE_CMP_SINGLE    0x08
#define PXMC_CMPQUE_CMP_m         0x0f

#define PXMC_CMPQUE_DONE_NORMAL   0x10
#define PXMC_CMPQUE_DONE_BYUSER   0x20
#define PXMC_CMPQUE_DONE_BYMISS   0x40
#define PXMC_CMPQUE_DONE_BYLIMIT  0x80
#define PXMC_CMPQUE_DONE_UNKNOWN 0x100
#define PXMC_CMPQUE_DONE_m       0x1f0

struct pxmc_cmpque_entry {
  ul_list_node_t      linear_queue_node;
  gavl_node_t         tree_queue_node;
  long                cmp_pos;
  pxmc_cmpque_fnc_t   fnc;
  pxmc_cmpque_queue_t *queue;
  unsigned char       done_code;
  unsigned char       cmp_code;
  unsigned char       cmp_req;
  unsigned char       active_fl;
};

struct pxmc_cmpque_queue {
  ul_list_head_t             linear_queue_carrier;
  ul_list_head_t             linear_queue_sentinel;
  ul_list_head_t             deactivated_queue;
  ul_list_head_t             done_queue;
  gavl_fles_int_root_field_t tree_queue_root;
  pxmc_state_t               *mcs;
  ul_list_node_t             all_queues_node;
};

static inline int pxmc_cmpque_entry_is_queued(pxmc_cmpque_entry_t *entry)
{
  return entry->queue &&
    (entry->linear_queue_node.next != entry->linear_queue_node.prev)? 1 : 0;
}

pxmc_cmpque_entry_t *pxmc_cmpque_entry_first(pxmc_cmpque_queue_t *queue);
pxmc_cmpque_entry_t *pxmc_cmpque_entry_last(pxmc_cmpque_queue_t *queue);
pxmc_cmpque_entry_t *
pxmc_cmpque_entry_next(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry);
pxmc_cmpque_entry_t *
pxmc_cmpque_entry_prev(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry);

int pxmc_cmpque_insert(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry);

int pxmc_cmpque_delete(pxmc_cmpque_queue_t *queue, pxmc_cmpque_entry_t *entry);

pxmc_cmpque_queue_t *pxmc_cmpque4mcs(pxmc_state_t *mcs, int alloc_if_needed);

int pxmc_cmpque_bottom(void);

extern char pxmc_cmpque_bottom_needed;

#endif /*_PXMC_CMPQUE_H_*/
