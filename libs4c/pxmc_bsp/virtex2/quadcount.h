#ifndef _QUADCOUNT_H
#define _QUADCOUNT_H

#include "iovolatile.h"

typedef struct QCNT {
  __I  uint16_t QCNTL;
  __I  uint16_t QCNTH;
} QCNT_t;


static inline uint32_t
hal_irc_get(struct QCNT *qcnt_base, int i)
{
  uint32_t result;

  result  = (qcnt_base+i)->QCNTL;
  result |= ((uint32_t)(qcnt_base+i)->QCNTH << 16);

  return result;
}

#endif
