/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  mo_psyshw.c - position controller subsystem core generic
                and hardware specific support
 
  Copyright (C) 2001-2005 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <ctm4.h>
#include <qadc.h>
#include <tpu.h>
#include <usd_7266.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_gen_info.h"
#include <string.h>

#ifdef WITH_DIO
  void do_dio_proc(void);
#endif

#ifdef WITH_CMPQUE
  void do_pxmc_cmpque(void);
#endif

#ifdef WITH_BRAKE_CTRL
  #include <periph/tpu_sup.h>
  #include <periph/tpu_func.h>
  extern unsigned short statchk_power_off;

  #define MO_BRAKE_B_TPU_CH  0
  #define MO_BRAKE_C_TPU_CH  1
#endif /*WITH_BRAKE_CTRL*/

/********************************************************************/
/* PWM and SF interrupt from CTM4 */

#define PXML_MAIN_CNT 8

uint8_t ctm4_pwm_pdir0s;	/* pwm direction shadow registers */
uint8_t ctm4_pwm_pdir1s;

unsigned ctm4_sfi_cnt=0;
unsigned ctm4_sfi_prolong=0;
unsigned ctm4_sfi_promax=0;
unsigned ctm4_sfi_spent=0;
unsigned ctm4_sfi_spmax=0;

int ctm4_sfi_msec_int;
int ctm4_sfi_msec_frac;
int ctm4_sfi_msec_sub;
int ctm4_sfi_msec_approx;

/* sampling frequency interrupt handler */
void ctm4_sfi_isr(int intno, void *dev_id, struct pt_regs *regs)
{
  *CTM4_MCSM2SIC&=~CTM4_MCSM_COF;

  ctm4_sfi_prolong=*CTM4_MCSM2CNT;
  if(ctm4_sfi_prolong>ctm4_sfi_promax)
    ctm4_sfi_promax=ctm4_sfi_prolong;
  
  ctm4_sfi_cnt++;
  do_pxm_control();
  do_pxmc_coordmv();

  if(mo_psys_mode&PSYS_MODE_BIGB){
    if(mo_psys_pwr_flg&PSYS_PWR_ON)	/* enable bridges PWM control */ 
      bigb_pwr_ctr_shad^=BIGB_PWR_CTR_WDG1;
    if(mo_psys_pwr_flg&PSYS_PWR_LIM)	/* enable power even for limit */
      bigb_pwr_ctr_shad^=BIGB_PWR_CTR_WDG2;
    if(mo_psys_pwr_flg&PSYS_PWR_BREAK)	/* breaking control*/
      bigb_pwr_ctr_shad|=BIGB_PWR_CTR_BREAK;
    else
      bigb_pwr_ctr_shad&=~BIGB_PWR_CTR_BREAK;
    if(mo_psys_pwr_mains>0)
      bigb_pwr_ctr_shad^=BIGB_PWR_CTR_MAINS;
    *BIGB_PWR_CTRL=bigb_pwr_ctr_shad;
  } 

 #ifdef WITH_DIO
  do_dio_proc();
 #endif

 #ifdef WITH_CMPQUE
  do_pxmc_cmpque();
 #endif

 #ifdef WITH_BRAKE_CTRL
  {
    int brake_rel;
    short int flg;
    flg=pxmc_main_list.pxml_arr[1]->pxms_flg;
    brake_rel=(flg&PXMS_ENR_m)&&!(flg&PXMS_ERR_m)&&!statchk_power_off?1:0;
    tpu_set_hsrr(MO_BRAKE_B_TPU_CH,(brake_rel?1:0)|0x2);
    flg=pxmc_main_list.pxml_arr[2]->pxms_flg;
    brake_rel=(flg&PXMS_ENR_m)&&!(flg&PXMS_ERR_m)&&!statchk_power_off?1:0;
    tpu_set_hsrr(MO_BRAKE_C_TPU_CH,(brake_rel?1:0)|0x2);
  }
 #endif /*WITH_BRAKE_CTRL*/

 #ifdef WITH_SFI_SEL
  {
    unsigned ms;
    ms=msec_time+ctm4_sfi_msec_int;
    if((ctm4_sfi_msec_approx+=ctm4_sfi_msec_frac)>0){
      ctm4_sfi_msec_approx-=ctm4_sfi_msec_sub;
      ms++;
    }
    msec_time=ms;
  }
 #else /* WITH_SFI_SEL */
  msec_time++;
 #endif /* WITH_SFI_SEL */

  ctm4_sfi_spent=*CTM4_MCSM2CNT-ctm4_sfi_prolong;
  if(ctm4_sfi_spent>ctm4_sfi_spmax)
    ctm4_sfi_spmax=ctm4_sfi_spent;
}


#ifdef WITH_SFI_SEL
int ctm4_sfi_sel(unsigned new_sfi_hz)
{
  unsigned ctm4_div;
  if(new_sfi_hz==0) new_sfi_hz=CTM4_SFI_HZ;
  if(new_sfi_hz<100) return -1;
  if(new_sfi_hz>5000) return -1;
  
  ctm4_div=(cpu_sys_hz+new_sfi_hz)/(new_sfi_hz*2);
  *CTM4_MCSM2ML=-ctm4_div;
  ctm4_div*=2;
  ctm4_sfi_hz=cpu_sys_hz/ctm4_div;

  ctm4_div*=1000;
  ctm4_sfi_msec_int=ctm4_div/cpu_sys_hz;
  ctm4_sfi_msec_frac=ctm4_div%cpu_sys_hz;
  ctm4_sfi_msec_sub=cpu_sys_hz;
  ctm4_sfi_msec_approx=0;

  return ctm4_sfi_hz;
}
#endif /* WITH_SFI_SEL */


irq_handler_t ctm4_sfi_handler={
  handler: ctm4_sfi_isr,
  flags:   0,
  dev_id:  0,
  devname: "ctm4_sfi",
  next:    0
};


int ctm4_pwm_and_sfi_init()
{
 #ifdef WITH_BRAKE_CTRL
  tpu_qom_init(MO_BRAKE_B_TPU_CH, 1, 0, 0, 0, NULL, -1, 0, 0);
  tpu_qom_init(MO_BRAKE_C_TPU_CH, 1, 0, 0, 0, NULL, -1, 0, 0);
 #endif /*WITH_BRAKE_CTRL*/

  /* disable PWM outputs */
  *QADC_PORTQ|=0x8000;
  /* power bridges direction control */
  *CTM4_PWM_PDIR0=ctm4_pwm_pdir0s=0;
  *CTM4_PWM_PDIR1=ctm4_pwm_pdir1s=0;

  /* BIUMCR - BIU Module Configuration Register */
  *CTM4_BIUMCR=(0*CTM4_STOP)|(CTM4_VECT&(ISR_CTM4_INTV<<5))|
               (CTM4_IARB&(ISR_CTM4_IARB<<8));
  
  /* CPCR - CPSM Control Register */
  *CTM4_CPCR=CTM4_PRUN|(0*CTM4_DIV23)|(CTM4_PSEL&0);
      
  /* PWM5 initialize */
  *CTM4_PWM5SIC=CTM4_PWM_POL|CTM4_PWM_EN|(CTM4_PWM_CLK&0);
  *CTM4_PWM5A=512;	/* period */
  *CTM4_PWM5B=0;	/* pulse width */

  /* PWM6 initialize */
  *CTM4_PWM6SIC=CTM4_PWM_POL|CTM4_PWM_EN|(CTM4_PWM_CLK&0);
  *CTM4_PWM6A=512;	/* period */
  *CTM4_PWM6B=0;	/* pulse width */

  /* PWM7 initialize */
  *CTM4_PWM7SIC=CTM4_PWM_POL|CTM4_PWM_EN|(CTM4_PWM_CLK&0);
  *CTM4_PWM7A=512;	/* period */
  *CTM4_PWM7B=0;	/* pulse width */

  /* PWM8 initialize */
  *CTM4_PWM8SIC=CTM4_PWM_POL|CTM4_PWM_EN|(CTM4_PWM_CLK&0);
  *CTM4_PWM8A=512;	/* period */
  *CTM4_PWM8B=0;	/* pulse width */

  /* FCSM12 initialize */
  *CTM4_FCSM12SIC=CTM4_FSCM_DRVB|(CTM4_FSCM_CLK&0);

  /* DASM3 initialize */
  *CTM4_DASM3SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_PWM9;
  *CTM4_DASM3A=0;	/* pulse start */
  *CTM4_DASM3B=0;	/* pulse end */

  /* DASM4 initialize */
  *CTM4_DASM4SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_PWM9;
  *CTM4_DASM4A=0;	/* pulse start */
  *CTM4_DASM4B=0;	/* pulse end */

  /* DASM9 initialize */
  *CTM4_DASM9SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_PWM9;
  *CTM4_DASM9A=0;	/* pulse start */
  *CTM4_DASM9B=0;	/* pulse end */

  /* DASM10 initialize */
  *CTM4_DASM10SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_PWM9;
  *CTM4_DASM10A=0;	/* pulse start */
  *CTM4_DASM10B=0;	/* pulse end */
  
  /* Enable power direction register output */
  *QADC_DDRQA|=0x8000;
  *QADC_PORTQ&=~0x8000;

  /* initialize sampling frequency interrupt */
 #ifdef WITH_SFI_SEL
  ctm4_sfi_sel(CTM4_SFI_HZ);
 #else /* WITH_SFI_SEL */
  *CTM4_MCSM2ML=-((cpu_sys_hz+CTM4_SFI_HZ)/(CTM4_SFI_HZ*2));
  ctm4_sfi_hz=CTM4_SFI_HZ;
 #endif /* WITH_SFI_SEL */
  *CTM4_MCSM2SIC=(CTM4_MCSM_IL&(ISR_CTM4_SFI_IL<<12))|
                 (ISR_CTM4_IARB&8?CTM4_MCSM_IARB3:0)|CTM4_MCSM_DRVA|
                 (CTM4_MCSM_CLK&0);
  add_irq_handler(ISR_CTM4_SFI_INTV,&ctm4_sfi_handler);

  return 0;
}


typedef struct ctm4_pwm_des {
  volatile uint16_t *const pwm_addr;
  volatile uint8_t  *const dir_addr;
  volatile uint8_t  *const dir_sr;
  uint8_t	    dir_maskclr;
  uint8_t	    dir_maskpos;
  uint8_t	    pwm_shift;
  uint8_t	    res;
} ctm4_pwm_des_t;


const ctm4_pwm_des_t ctm4_pwm_des[PXML_MAIN_CNT]={
  {CTM4_PWM5B,  CTM4_PWM_PDIR0,&ctm4_pwm_pdir0s,0x03,0x01,16-10},
  {CTM4_PWM6B,  CTM4_PWM_PDIR0,&ctm4_pwm_pdir0s,0x0c,0x04,16-10},
  {CTM4_PWM7B,  CTM4_PWM_PDIR0,&ctm4_pwm_pdir0s,0x30,0x10,16-10},
  {CTM4_PWM8B,  CTM4_PWM_PDIR0,&ctm4_pwm_pdir0s,0xc0,0x40,16-10},
  {CTM4_DASM3B, CTM4_PWM_PDIR1,&ctm4_pwm_pdir1s,0x03,0x01,16-10},
  {CTM4_DASM4B, CTM4_PWM_PDIR1,&ctm4_pwm_pdir1s,0x0c,0x04,16-10},
  {CTM4_DASM9B, CTM4_PWM_PDIR1,&ctm4_pwm_pdir1s,0x30,0x10,16-10},
  {CTM4_DASM10B,CTM4_PWM_PDIR1,&ctm4_pwm_pdir1s,0xc0,0x40,16-10}
};


void ctm4_pwm_set(short num, short val)
{
  if(val==0){
    *(ctm4_pwm_des[num].pwm_addr)=val;
    atomic_clear_mask_b(ctm4_pwm_des[num].dir_maskclr,ctm4_pwm_des[num].dir_sr);
    *(ctm4_pwm_des[num].dir_addr)=*(ctm4_pwm_des[num].dir_sr); /* ??!! atomic */
  }else{
    if(val>0){
      *(ctm4_pwm_des[num].pwm_addr)=val;
      atomic_clear_mask_b(ctm4_pwm_des[num].dir_maskpos<<1,ctm4_pwm_des[num].dir_sr);
      atomic_set_mask_b(ctm4_pwm_des[num].dir_maskpos,ctm4_pwm_des[num].dir_sr);
      *(ctm4_pwm_des[num].dir_addr)=*(ctm4_pwm_des[num].dir_sr); /* ??!! atomic */
    }else{
      *(ctm4_pwm_des[num].pwm_addr)=-val;
      atomic_clear_mask_b(ctm4_pwm_des[num].dir_maskpos,ctm4_pwm_des[num].dir_sr);
      atomic_set_mask_b(ctm4_pwm_des[num].dir_maskpos<<1,ctm4_pwm_des[num].dir_sr);
      *(ctm4_pwm_des[num].dir_addr)=*(ctm4_pwm_des[num].dir_sr); /* ??!! atomic */
    }
  }
}

int ctm4_pwm_out(pxmc_state_t *mcs)
{ 
  short val;
  val=mcs->pxms_ene>>6;
  if((val<-100)||(val>100)) val=0;
  ctm4_pwm_set(mcs->pxms_out_info,val);
  return 0;
}

/********************************************************************/
/* IRC from US Digital 7266 */

/* structure for mark and index filters  */
typedef struct usd_irc_mark_filt {
  volatile uint16_t index_hist;
  volatile uint16_t switch_hist;
} usd_irc_mark_filt_t;

/* structure configuring mark and index sources */
typedef struct usd_irc_mark_des {
  volatile uint8_t *const shadow_addr;
  volatile uint8_t *const gate_addr;
  volatile uint8_t *const mark_addr;
  uint8_t	   index_mask;
  uint8_t	   switch_mask;
  uint8_t	   gate_mask;
  uint8_t	   res1;
} usd_irc_mark_des_t;

/* irc counters mapping in memory */
const typeof(USD_IRC_ADDR) usd_irc_addr_tab[PXML_MAIN_CNT]={
  USD_IRC_ADDR+0x00,
  USD_IRC_ADDR+0x02,
  USD_IRC_ADDR+0x08,
  USD_IRC_ADDR+0x0A,
  USD_IRC_ADDR+0x10,
  USD_IRC_ADDR+0x12,
  USD_IRC_ADDR+0x18,
  USD_IRC_ADDR+0x1A,
};

/* mark and index filters */
usd_irc_mark_filt_t usd_irc_mark_filt[PXML_MAIN_CNT];

uint8_t usd_irc_gate_s;	/* shadow register of gate of mark signals */
			/* to 7266 xLCNTR/xLOL */

/* mark and index description */
usd_irc_mark_des_t usd_irc_mark_des[PXML_MAIN_CNT];

const usd_irc_mark_des_t usd_irc_mark_des_mars8[PXML_MAIN_CNT]={
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK0,0x80,0x40,0x80},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK0,0x20,0x10,0x40},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK0,0x08,0x04,0x20},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK0,0x02,0x01,0x10},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x80,0x40,0x08},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x20,0x10,0x04},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x08,0x04,0x02},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x02,0x01,0x01}
};

const usd_irc_mark_des_t usd_irc_mark_des_mars8b[PXML_MAIN_CNT]={
  {&usd_irc_gate_s,USD_IRC_GATE,BIGB_REF_SW,  0x00,0x01,0x80},
  {&usd_irc_gate_s,USD_IRC_GATE,BIGB_REF_SW,  0x00,0x02,0x40},
  {&usd_irc_gate_s,USD_IRC_GATE,BIGB_REF_SW,  0x00,0x04,0x20},
  {&usd_irc_gate_s,USD_IRC_GATE,BIGB_REF_SW,  0x00,0x08,0x10},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x80,0x40,0x08},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x20,0x10,0x04},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x08,0x04,0x02},
  {&usd_irc_gate_s,USD_IRC_GATE,USD_IRC_MARK1,0x02,0x01,0x01}
};

const uint8_t onesin10bits[1024]={
  0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,6,7,7,8,7,8,8,9,7,8,8,9,8,9,9,10
};

int usd_irc_init(int chan)
{
  volatile uint8_t *p;
  int i;
  /* Clear index gate */
  atomic_clear_mask_b(usd_irc_mark_des[chan].gate_mask,
  		      usd_irc_mark_des[chan].shadow_addr);
  *(usd_irc_mark_des[chan].gate_addr)=*(usd_irc_mark_des[chan].shadow_addr);
  /* Take address of 7266 */
  p=usd_irc_addr_tab[chan];
  /* Counter Mode Register */
  p[1]=USD_7266_CMR|
       USD_7266_CMR_BIN|USD_7266_CMR_NORM|USD_7266_CMR_Q4;
  /* Counter Mode Register */
  p[1]=USD_7266_IOR|
       USD_7266_IOR_ENAB|USD_7266_IOR_RCNTR|USD_7266_IOR_CMPBO;
  /* Index Control Register */
  p[1]=USD_7266_IDR|
       USD_7266_IDR_DIS;
  /* Reset and Load Signal Decoders */
  p[1]=USD_7266_RLD|
       USD_7266_RLD_BP|USD_7266_RLD_RFF;
  /* DATA = PSC */
  p[0]=1;
  /* Reset and Load Signal Decoders */
  p[1]=USD_7266_RLD|
       USD_7266_RLD_PR2PSC;
  for(i=100;i;i--)__memory_barrier();
  p[1]=USD_7266_RLD|
       USD_7266_RLD_RC;
  for(i=100;i;i--)__memory_barrier();
  p[1]=USD_7266_RLD|
       USD_7266_RLD_BP;

  return 0;
}

int usd_irc_init_all(void)
{
  int i;
  usd_irc_gate_s=0;
  if(!(mo_psys_mode&PSYS_MODE_BIGB_MARK)){
    memcpy(usd_irc_mark_des,usd_irc_mark_des_mars8,sizeof(usd_irc_mark_des));
  }else{
    /* MARS8 BigBot specific marks sources */
    memcpy(usd_irc_mark_des,usd_irc_mark_des_mars8b,sizeof(usd_irc_mark_des));
  }
  for(i=0;i<PXML_MAIN_CNT;i++)
  {
    if(usd_irc_init(i)<0)
      break;
  }
  return i;
}


int usd_irc_update(pxmc_state_t *mcs)
{
  int irc;
  int chan;
  volatile uint8_t *p;
  chan=mcs->pxms_inp_info;
  p=usd_irc_addr_tab[chan];

  /* Reset and Load Signal Decoders */
  p[1]=USD_7266_RLD|
       USD_7266_RLD_BP|USD_7266_RLD_C2OL;
  /* OL = IRC */
  irc=p[0]<<8;
  irc|=p[0]<<16;
  irc|=p[0]<<24;
  mcs->pxms_as=irc-mcs->pxms_ap;
  mcs->pxms_ap=irc;
  return 0;
}


/* configuration taken from 
   pxms_cfg  xxxxxxxT PLCRDSSS
   SSS	.. initial speed of hardhome pxms_ms>>SSS
   D	.. initial direction
   R	.. use revolution index from HP HEDS
   C	.. find mark center
   L	.. use limit switch
   P	.. switch polarity
*/

int usd_irc_find_mark(pxmc_state_t *mcs)
{
  int chan=mcs->pxms_inp_info;
  const usd_irc_mark_des_t *mdes=&usd_irc_mark_des[chan];

  atomic_clear_mask_b(mdes->gate_mask,mdes->shadow_addr);
  *(mdes->gate_addr)=*(mdes->shadow_addr);

  switch(mcs->pxms_gen_st){
    case 0:
      if(*(mdes->mark_addr)&mdes->switch_mask){
        mcs->pxms_rs=-mcs->pxms_gen_hsp;
        mcs->pxms_rp+=mcs->pxms_rs;
	break;
      }
      mcs->pxms_rs=0;
      mcs->pxms_gen_st++;
      break;
    case 1:
      if(!(*(mdes->mark_addr)&mdes->switch_mask)){
        mcs->pxms_rs=mcs->pxms_gen_hsp;
        mcs->pxms_rp+=mcs->pxms_rs;
	break;
      }
      mcs->pxms_rs=0;
      mcs->pxms_gen_st++;
      break;
    case 2:
      if(*(mdes->mark_addr)&mdes->switch_mask){
        mcs->pxms_rs=-(mcs->pxms_gen_hsp>>1);
        mcs->pxms_rp+=mcs->pxms_rs;
	break;
      }
      mcs->pxms_rs=0;
      mcs->pxms_gen_st++;
      break;
    case 3:
      
      /* reset position */
      *(usd_irc_addr_tab[chan]+1)=USD_7266_RLD|USD_7266_RLD_RC;
      mcs->pxms_rp=mcs->pxms_ap=0;
      
      
    default:
      mcs->pxms_rs=0;
      mcs->pxms_flg&=~(PXMS_BSY_m|PXMS_ENG_m);
      return 0;
  }
  /* stop on error */
  if(mcs->pxms_flg&PXMS_ERR_m)
  {
    mcs->pxms_rs=0;
    mcs->pxms_flg&=~(PXMS_BSY_m|PXMS_ENG_m);
    return 0;
  }
  return 0;
}

int usd_irc_get_mark(pxmc_state_t *mcs)
{
  int ret=0;
  int chan=mcs->pxms_inp_info;
  const usd_irc_mark_des_t *mdes=&usd_irc_mark_des[chan];
  if(*(mdes->mark_addr)&mdes->switch_mask) ret|=0x7f;
  if(*(mdes->mark_addr)&mdes->index_mask) ret|=0x7f<<8;
  return ret;
}
