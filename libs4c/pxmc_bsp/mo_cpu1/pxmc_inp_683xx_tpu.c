/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  pxmc_inp_683xx_tpu.c - generic multi axis motion controller
                         Motorolo MC683xx TPU position input

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <tpu.h>
#include "pxmc.h"
#include "pxmc_inp_common.h"

/********************************************************************/
/* IRC from TPU  */

#include <periph/tpu_sup.h>
#include <periph/tpu_func.h>

int tpu_irc_init(int chan)
{
  return tpu_fqd_init(chan, chan+1, 2);
}

int
tpu_irc_inp(struct pxmc_state *mcs)
{
  short irc;
  irc=tpu_fqd_read_count(mcs->pxms_inp_info);
  pxmc_irc_16bit_update(mcs,irc);

  /* Running of the motor commutator */
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  return 0;
}

int tpu_irc_ap2hw(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  tpu_set_cpr(chan,0);		/* disable chanel function */
  tpu_set_cpr(chan+1,0);	/* disable chanel function */
  *tpu_param_addr(chan,FQD_POSITION_COUNT)=mcs->pxms_ap;
  tpu_set_cpr(chan,2);		/* medium priority */
  tpu_set_cpr(chan+1,2);	/* medium priority */
  return 0;
}
