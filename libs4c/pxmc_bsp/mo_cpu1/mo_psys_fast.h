/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_psys_fast.h - position controller subsystem
                 the decalarations of optimised fast assembly
		 routines for MARS8 hardware and 683xx architecture

  Copyright (C) 2001-2005 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2005 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

/********************************************************************/
/* imported fast assembly routines for controllers */

/* process all sample time activities for list of mcs */
extern int pxmc_fast_proc_list(pxmc_state_t **list_ptr, int list_cnt);

/* call routine with pxmc_fast calling convention */
extern int pxmc_fast_call(pxmc_state_t *mcs, int fnc(pxmc_state_t *mcs));

/* fast PID controller, be careful, uses special calling convention */
extern int pxmc_fast_pid(pxmc_state_t *mcs);

/* fast IRC update, be careful, uses special calling convention */
extern int pxmc_fast_usd_irc(pxmc_state_t *mcs);

/* fast IRC set value of AP to HW, be careful, uses special calling convention */
extern int pxmc_fast_usd_irc_ap2hw(pxmc_state_t *mcs);

/* fast PWM output, be careful, uses special calling convention */
extern int pxmc_fast_ctm4_pwm(pxmc_state_t *mcs);

/* fast trapezoid profile generator,
   be careful, uses special calling convention */
extern int pxmc_fast_trp_gi(pxmc_state_t *mcs);

/* special function for trapezoid move retargetting,
   be careful, uses special calling convention */
extern int pxmc_fast_trp_retgt(pxmc_state_t *mcs);

/* continues with last pxms_rs,
   be careful, uses special calling convention */
extern int pxmc_fast_cont_gi(pxmc_state_t *mcs);

/* fast hard home finding,
   be careful, uses special calling convention */
extern int pxmc_fast_hh_gi(pxmc_state_t *mcs);

/* fast constant speed generator,
   be careful, uses special calling convention */
extern int pxmc_fast_spd_gi(pxmc_state_t *mcs);

/* transition to zero speed and next function,
   be careful, uses special calling convention */
extern int pxmc_fast_spdnext_gi(pxmc_state_t *mcs);

/* transition to zero speed and then stop
   be careful, uses special calling convention */
extern int pxmc_fast_spdstop_gi(pxmc_state_t *mcs);

#ifdef PXMC_WITH_FAST_TRPREL
/* fast relative trapezoid profile generator with infinite range,
   be careful, uses special calling convention */
extern int pxmc_fast_trprel_gi(pxmc_state_t *mcs);
#endif /*PXMC_WITH_FAST_TRPREL*/

