/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  usd_7266.h - USD7266 dual IRC interface/counter definitions
 
  Copyright (C) 2001-2003 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#define USD_7266_XY		0x80	/* write to both X and Y */

/* Counter Mode Register */
#define USD_7266_CMR		0x20

#define   USD_7266_CMR_BIN	0x00	/* binary */
#define   USD_7266_CMR_BCD	0x01	/* decimal */

#define   USD_7266_CMR_NORM	0x00	/* normal mode */
#define   USD_7266_CMR_RLIM	0x02	/* range limit */
#define   USD_7266_CMR_NREC	0x04	/* non-recycle */
#define   USD_7266_CMR_MODN	0x06	/* modulo-N */

#define   USD_7266_CMR_NQ	0x00	/* non qadrature */
#define   USD_7266_CMR_Q1	0x08	/* qadrature x1 */
#define   USD_7266_CMR_Q2	0x10	/* qadrature x2 */
#define   USD_7266_CMR_Q4	0x18	/* qadrature x4 */

/* Input/Output Control Register */
#define USD_7266_IOR		0x40

#define   USD_7266_IOR_DISAB	0x00	/* disable inputs A and B */
#define   USD_7266_IOR_ENAB	0x01	/* enable inputs A and B */

#define   USD_7266_IOR_LCNTR	0x00	/* LCNTR/LOL pin is load CNTR */
#define   USD_7266_IOR_LOL	0x02	/* LCNTR/LOL pin is load OL */

#define   USD_7266_IOR_RCNTR	0x00	/* RCNTR/ABG pin is reset CNTR */
#define   USD_7266_IOR_ABG	0x04	/* RCNTR/ABG pin is A and B gate */

#define   USD_7266_IOR_CYBO	0x00	/* FLG1=carry, FLG2=borrow */
#define   USD_7266_IOR_CMPBO	0x08	/* FLG1=compare, FLG2=Borrow */
#define   USD_7266_IOR_CBUD	0x10	/* FLG1=carry/borrow, FLG2=U/D */
#define   USD_7266_IOR_IDXE	0x18	/* FLG1=IDX flag, FLG2=E flag */

/* Index Control Register */
#define USD_7266_IDR		0x60

#define   USD_7266_IDR_DIS	0x00	/* disable index */
#define   USD_7266_IDR_EN	0x01	/* enable index */

#define   USD_7266_IDR_NEG	0x00	/* negative index */
#define   USD_7266_IDR_POS	0x02	/* positive index */

#define   USD_7266_IDR_LCLOL	0x00	/* index on LCNTR/LOL pin */
#define   USD_7266_IDR_RCABG	0x04	/* index on RCNTR/ABG pin */

/* Reset and Load Signal Decoders */
#define USD_7266_RLD		0x00

#define   USD_7266_RLD_BP	0x01	/* reset BP */

#define   USD_7266_RLD_RC	0x02	/* reset CNTR */
#define   USD_7266_RLD_RFF	0x04	/* reset BT, CT, CPT, S */
#define   USD_7266_RLD_RE	0x06	/* reset E */

#define   USD_7266_RLD_PR2C	0x08	/* transfer PR to CNTR */
#define   USD_7266_RLD_C2OL	0x10	/* transfer CNTR to OL */
#define   USD_7266_RLD_PR2PSC	0x18	/* transfer PR0 to PSC */

/* Status FLAGs Register */
#define   USD_7266_FLAG_BT	0x01	/* borrow toggle f-f */
#define   USD_7266_FLAG_CT	0x02	/* carry toggle f-f */
#define   USD_7266_FLAG_CPT	0x04	/* compare toggle f-f */
#define   USD_7266_FLAG_S	0x08	/* sign flag */
#define   USD_7266_FLAG_E	0x10	/* error flag */
#define   USD_7266_FLAG_UD	0x20	/* up/down flag */
#define   USD_7266_FLAG_IDX	0x40	/* index flag */
