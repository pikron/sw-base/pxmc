/*******************************************************************
  Copyright (C)2001-2011 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C)2002-2005 by PiKRON Ltd. http://www.pikron.com
            (C)2006-2007 by Konrad Skup ahblitz@yahoo.de

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <pxmc.h>
#include "pxmc_eurobot_config.h"

#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include <pxmc.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include "eurobot.h"

#define MOTOR_PWM2D_REPLACE_A

#ifdef MOTOR_HAL1000
#include "pt4000.h"
#endif

#ifdef MOTOR_EUROBOT_DEMO
#include "pt2000.h"
#endif

#ifdef MOTOR_EUROBOT2
#include "pt2048.h"
#endif

//=================================
/* 	Definitions of some basic but
 	usefull variables. */
//=================================
int pxmc_sfikhz = 2;	// Sampling freq. interrupt - how often we want to collect data
int pxmc_sfi2msec;
volatile unsigned pxmc_msec_counter; // miliseconds counter
#define PWM_HZ 30000
#define PWM_MAX (((CPU_SYS_HZ / PWM_HZ) <= PWBFR2A_DTxm) ? (CPU_SYS_HZ / PWM_HZ) : PWBFR2A_DTxm)
#define HAL_ERR_SENSITIVITY 20
#define HAL_ERR_MAX_COUNT 1000
static int motor_do_input(struct pxmc_state *mcs);
#ifndef DEBUG
static
#endif
int motor_do_output(struct pxmc_state *mcs);
static int tpu_irc_ap2hw(struct pxmc_state *mcs);

#ifdef MOTOR_EUROBOT2
const char pxmc_variant[] = "eurobot";
#define MAX_RPS 2               /* Wheel rotations per second */
#define GEAR_RATIO 29
#define PXMS_MD (2000l*28)<<8
#define PXMS_MS (((long)MAX_RPS*GEAR_RATIO)<<8)
#define PXMS_MA 30
#define PXMS_P  30
#define PXMS_I  0
#define PXMS_D  0

#define PXMS_PTMARK_L 375
#define PXMS_PTMARK_R 310
#endif

#ifdef MOTOR_EUROBOT_DEMO
const char pxmc_variant[] = "eurobot-demo";
#define MAX_RPS 2               /* Wheel rotations per second */
#define GEAR_RATIO 28
#define PXMS_MD (2000l*28)<<8
#define PXMS_MS (((long)MAX_RPS*GEAR_RATIO)<<8)
#define PXMS_MA 30
#define PXMS_P  100
#define PXMS_I  1
#define PXMS_D  0

#define PXMS_PTMARK_L 845
#define PXMS_PTMARK_R 1368
#endif

#ifdef MOTOR_HAL1000
const char pxmc_variant[] = "hal1000";
#define GEAR_RATIO 159
#define PXMS_MD 20000l<<8
#define PXMS_MS 125<<8
#define PXMS_MA 40
#define PXMS_P  100
#define PXMS_I  20
#define PXMS_D  100
#define PXMS_PTMARK_L 2773
#define PXMS_PTMARK_R 2773
#endif

const short  sintab[256]={0,3,6,9,13,16,19,22,25,28,31,34,37,40,43,46,49,52,55,58,60,63,66,68,71,74,76,79,81,84,86,88,91,93,95,97,99 , 101,103,105,106,108,110,111,113,114,116,117,\
118,119,121,122,122,123,124,125,126,126,127,127,127,128,128,128,128,128,128,128,127,127,127,126,\
126,125,124,123,122,122,121,119,118,117,116,114,113,111,110,108,106,105,103,101,99,97,95,93,\
91,88,86,84,81,79,76,74,71,68,66,63,60,58,55,52,49,46,43,40,37,34,31,28,\
25,22,19,16,13, 9, 6, 3, 0,-3,-6,-9,-13,-16,-19,-22,-25,-28,-31,-34,-37,-40,-43,-46,\
-49,-52,-55,-58,-60,-63,-66,-68,-71,-74,-76,-79,-81,-84,-86,-88,-91,-93,-95,-97,-99  -101,-103,-105,\
-106,-108,-110,-111,-113,-114,-116,-117,-118,-119,-121,-122,-122,-123,-124,-125,-126,-126,-127,-127,-127,-128,-128,-128,\
-128,-128,-128,-128,-127,-127,-127,-126,-126,-125,-124,-123,-122,-122,-121,-119,-118,-117,-116,-114,-113,-111,-110,-108,\
 -106,-105,-103,-101,-99,-97,-95,-93,-91,-88,-86,-84,-81,-79,-76,-74,-71,-68,-66,-63,-60,-58,-55,-52,\
-49,-46,-43,-40,-37,-34,-31,-28,-25,-22,-19,-16,-13,-9,-6,-3, 0};		// sinus table

pxmc_state_t mcs_right = {
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp: motor_do_input,	// set pointer to funct. for measuring IRC
  pxms_do_out: motor_do_output, //pxmc_do_out_brushless;	// set pointer to funct. control energi for outpu (PWM)
  pxms_do_con: pxmc_pid_con,	// set pointer to PID con. function  (implicit (PXMC.c) or user
  pxms_do_ap2hw: tpu_irc_ap2hw,
  pxms_md: PXMS_MD, pxms_ms: PXMS_MS, pxms_ma: PXMS_MA,
  pxms_inp_info: (long)TPU_TCNT1,//TPU_TCNT1			/*chanel TPU A,B*/
  pxms_out_info: 0,			/*chanel PWM A,B*/
  pxms_p: PXMS_P, pxms_i: PXMS_I, pxms_d: PXMS_D, pxms_s1: 0, pxms_s2: 0,
  pxms_ptirc: bldc_ptirc, // IRC count per phase table
  pxms_ptper: bldc_ptper,	// Number of periods per table
  pxms_ptmark: PXMS_PTMARK_R,

  //* @pxms_ptofs:	Offset between table and IRC counter
  pxms_ptshift: 0, // Shift of generated phase curves
  pxms_ptvang:  bldc_ptirc/4,	// Angle (in irc) between rotor and stator mag. fld.

  pxms_ptptr1: (short*)bldc_phase1,
  pxms_ptptr2: (short*)bldc_phase3,
  pxms_ptptr3: (short*)bldc_phase2,
  pxms_ptamp: 0x7fff,
  pxms_me:PWM_MAX << 5,
  pxms_hal: 0x40,
  pxms_cfg: PXMS_CFG_I2PT_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m |
            PXMS_CFG_HDIR_m| PXMS_CFG_SMTH_m| PXMS_CFG_MD2E_m| 0x1
};

pxmc_state_t mcs_left = {
  pxms_flg:PXMS_ENI_m,
  pxms_do_inp: motor_do_input,	// set pointer to funct. for measuring IRC
  pxms_do_out: motor_do_output, //pxmc_do_out_brushless;	// set pointer to funct. control energi for outpu (PWM)
  pxms_do_con: pxmc_pid_con,	// set pointer to PID con. function  (implicit (PXMC.c) or user
  pxms_do_ap2hw: tpu_irc_ap2hw,
  pxms_md: PXMS_MD, pxms_ms: PXMS_MS, pxms_ma: PXMS_MA,
  pxms_inp_info: (long)TPU_TCNT2,//TPU_TCNT1			/*chanel TPU A,B*/
  pxms_out_info: 1,			/*chanel PWM A,B*/
  pxms_p: PXMS_P, pxms_i: PXMS_I, pxms_d: PXMS_D, pxms_s1: 0, pxms_s2: 0,
  pxms_ptirc: bldc_ptirc, // IRC count per phase table
  pxms_ptper: bldc_ptper,	// Number of periods per table
  pxms_ptmark: PXMS_PTMARK_L,

  //* @pxms_ptofs:	Offset between table and IRC counter
  pxms_ptshift: 0, // Shift of generated phase curves
  pxms_ptvang:  bldc_ptirc/4,	// Angle (in irc) between rotor and stator mag. fld.

  pxms_ptptr1: (short*)bldc_phase1,
  pxms_ptptr2: (short*)bldc_phase3,
  pxms_ptptr3: (short*)bldc_phase2,
  pxms_ptamp: 0x7fff,
  pxms_me:PWM_MAX << 5,
  pxms_hal: 0x40,
  pxms_cfg: PXMS_CFG_I2PT_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m |
            PXMS_CFG_HDIR_m| PXMS_CFG_SMTH_m| PXMS_CFG_MD2E_m| 0x1
};

pxmc_state_t *pxmc_main_arr[] = {&mcs_left, &mcs_right};

pxmc_state_list_t pxmc_main_list = {
                                       pxml_arr: pxmc_main_arr,
                                       pxml_cnt: sizeof(pxmc_main_arr) / sizeof(pxmc_main_arr[0])
                                   };

const unsigned char eurobot_bdc_hal_pos_table[8] =
    {
        [0] = 0xff,
        [7] = 0xff,
        [6] = 0,/*0*/
        [2] = 1,/*1*/
        [3] = 2,/*2*/
        [1] = 3,/*3*/
        [5] = 4,/*4*/
        [4] = 5,/*5*/
    };

/**
 * Function used to call pxms_do_inp functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_input()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENI_m - check if input (IRC) update is enabled
    if (mcs->pxms_flg&PXMS_ENI_m) {
      pxmc_call(mcs, mcs->pxms_do_inp);
//      	error_led(~1);
    }
  }
}

/**
 * Function used to call pxms_do_con and pxms_do_out
 * functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_controller_and_output()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENR_m - check if controller is enabled
    if (mcs->pxms_flg&PXMS_ENR_m || mcs->pxms_flg&PXMS_ENO_m) {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg&PXMS_ENR_m) {
        //static int i=0;
        //i=(i+1)%200;
        //if (i==0) *DIO_PJDR ^= 2;

        pxmc_call(mcs, mcs->pxms_do_con);
        // PXMS_ERR_m - if axis in error state
        if (mcs->pxms_flg&PXMS_ERR_m) mcs->pxms_ene = 0;
      }

      // FIXME: For bushless motors, it is necessary to call do_out
      // even if the controller is not enabled.
      pxmc_call(mcs, mcs->pxms_do_out);
      // error_led(1);
    }
  }
}

/**
 * Function used to call pxms_do_gen functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_generator()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    // PXMS_ENG_m - check if requested value (position) generator is enabled
    if (mcs->pxms_flg&PXMS_ENG_m) {
      pxmc_call(mcs, mcs->pxms_do_gen);
      // error_led(~2);
    }
  }
}

/**
 * Function used to call pxms_do_deb functions for all motors.
 * It's called by pxmc_sfi_isr during interrupt event.
 */
static inline void interupt_dbg()
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs) {
    if (mcs->pxms_flg&PXMS_DBG_m) {
      pxmc_call(mcs, mcs->pxms_do_deb);
      // error_led(2);
    }
  }

  if (--pxmc_sfi2msec <= 0) {
    msec_time++;
    pxmc_sfi2msec = pxmc_sfikhz;
    /* Hook for external finite state machine */
  } else {
    /* Run ADC measurements */
  }
}

int x=0,y=0;  // initial position of the robot
long alfa=0.0; // angle in which robot is turned at the begin

/**
 * Sampling Frequency Interrupt
 * ----------------------------
 * This function is used to call functions for different services,
 * like pxms_do_inp,pxms_do_out...
 * It works due to interrupts, and is called with freq. = pxmc_sfikhz.
 * Is is setting up by: PXMC_TPU_SFI_INIT - macro.
 */
/* int intno, void *dev_id, struct pt_regs *regs */
void pxmc_sfi_isr(void) __attribute__ ((interrupt_handler));
void pxmc_sfi_isr(void)	// sampling frequency
{
    //DEB_LED_ON(3);
    //*TPU_TSR5&=~TSR5_TGFAm;
    /* Clear interrupt source */
    /* PXMS_ENI_m = enable input (IRC) update */
    interupt_input();

    /* PXMS_ENG_m - enable requested value (position) generator*/
    interupt_generator();

    /*PXMS_ENR_m - enable controller*/
    interupt_controller_and_output();

    /* PXMS_DBG_m - enable debugging */
    interupt_dbg();

    static int submsec = 0;
    submsec++;
    if (submsec==pxmc_sfikhz) {
        submsec = 0;
        pxmc_msec_counter++;
    }

    // Allow next interrupt...
    *TPU_TSR4 &= ~TSR4_TGFAm; //Acknowledge an interrupt
    //DEB_LED_OFF(3);
}

#define TEMP_TPU_PHASE_CNT_INIT(n) \
  do{ \
  /*TimerControlRegistr - TCNT clearing disabled; count rasing adge; internal clock fi/1 */\
  *TPU_TCR##n=0; \
  /*phase counting mode 1 - how will be TCNT incrementd */\
  *TPU_TMDR##n=TPMDR_MD_PHACN1; \
  /* Input capture at rising edge on TIOCAx pin (index mark)  */\
  *TPU_TIOR##n = TIOR##n##_IOA3m; \
   /* Clear status bit of index input capture */  \
  *TPU_TSR##n &= ~TSR##n##_TGFAm; \
  /*AD conversion disabled; interrupt TGIA disabled.; */\
  *TPU_TIER##n=TIER##n##_TGIEAm * 0; \
  excptvec_set(EXCPTVEC_TGI##n##A,index_mark_isr##n); /* not used for now */ \
  /*start TCNT*/\
  *TPU_TSTR|=(1<<n); \
}while (0)

// TRICK: See cpp info pages, node Argument Prescan,
// parahraph "Macros that call other macros that stringify or concatenate."
#define TPU_PHASE_CNT_INIT(n) TEMP_TPU_PHASE_CNT_INIT(n)

static void init_pwm(void)
{
    /* setup pwm */
    *SYS_MSTPCRD &= ~MSTPCRD_PWMm;

    *PWM_PWOCR2 =
        PWOCR2_OE2Am | PWOCR2_OE2Bm | PWOCR2_OE2Cm |
      #ifdef MOTOR_PWM2D_REPLACE_A 
	PWOCR2_OE2Dm |
      #endif
        PWOCR2_OE2Em | PWOCR2_OE2Fm | PWOCR2_OE2Gm;	// switch on needed channels

    *PWM_PWPR2 =
            PWPR2_OPS2Am | PWPR2_OPS2Bm | PWPR2_OPS2Cm |
          #ifdef MOTOR_PWM2D_REPLACE_A 
	    PWPR2_OPS2Dm |
          #endif
            PWPR2_OPS2Em | PWPR2_OPS2Fm | PWPR2_OPS2Gm;	// reverse polarity of PWM outputs

    *PWM_PWCYR2 = PWM_MAX;

    *PWM_PWCR2 = PWCR2_CSTm | PWCR2_CKS_F1; /* Start PWM timer, clock = phi/1 */
}

void index_mark_isr1(void) __attribute__((interrupt_handler));
void index_mark_isr2(void) __attribute__((interrupt_handler));

#define INDEX_MARK_BLINK(num)                                           \
    static int led = 0;                                                 \
    if (led ^= 1) DEB_LED_ON(LED_INDEX##num);                           \
    else  DEB_LED_OFF(LED_INDEX##num);                                  \


#define INDEX_MARK_ISR(num)                                             \
    mcs->pxms_ptofs = *TPU_TGR##num##A - mcs->pxms_ptmark;              \
    /* Lock commutation only if mark position is set */                 \
    if (mcs->pxms_ptmark) pxmc_set_flag(mcs, PXMS_PTI_b);               \
    pxmc_set_flag(mcs, PXMS_PHA_b);                                     \
    /* *TPU_TIER##num=0; */ /* Disable future index mark interrupts */ \
    *TPU_TSR##num &= ~TSR##num##_TGFAm; /* ACK interrupt */

typedef struct mark_source_des_t {
    volatile uint16_t * tgr;
    volatile uint8_t * tsr;
} mark_source_des_t;

const mark_source_des_t mark_source[2] = {
    {   /* mcs_right, out_info = 0, TPU1 */
        .tgr = TPU_TGR1A,
        .tsr = TPU_TSR1
    },
    {   /* mcs_left, out_info = 1, TPU2 */
        .tgr = TPU_TGR2A,
        .tsr = TPU_TSR2
    }
};


void index_mark_isr1(void)
{
    pxmc_state_t *mcs = &mcs_right;
    INDEX_MARK_ISR(1);
#ifdef LED_INDEX1
    INDEX_MARK_LED(1);
#endif
}
void index_mark_isr2(void)
{
    pxmc_state_t *mcs = &mcs_left;
    INDEX_MARK_ISR(2);
#ifdef LED_INDEX2
    INDEX_MARK_LED(2);
#endif
}

static void init_irc(void)
{
    /* setup tpu */
    *SYS_MSTPCRA &= ~MSTPCRA_TPUm; /* Switch on TPU module */
    TPU_PHASE_CNT_INIT(1);
    TPU_PHASE_CNT_INIT(2);

}

static void init_hal(void)
{
    *DIO_PBDDR = 0;	// hal and fault signals are inputs
}

static void init_sampling(void)
{
    *SYS_MSTPCRA &= ~MSTPCRA_TPUm; /* Switch on TPU module */

    *TPU_TSTR &= ~TSTR_CST4m;	/*counter n is stoped*/
    /*CNTN cleared by by TGRA, rising edge; source TGRA compare match/input capture  */
    *TPU_TCR4 = (TPCR_TPSC_F1 | TPCR_CKEG_RIS | TPCR_CCLR_TGRA);
    /*TGRA is output compare registr - . 0 output at compare match*/
    *TPU_TIOR4 |= TIOR4_IOA1m;
    /*MDn = 0x000 normal operation */
    *TPU_TMDR4 = TPMDR_MD_NORMAL;
    /*seting interupt vector - from TGRA at registr TSRn*/
    excptvec_set(EXCPTVEC_TGI4A, pxmc_sfi_isr);
    /*TimerStatusRegistr - clear match flag */
    *TPU_TSR4 &= ~TSR4_TGFAm;
    /*TimerInteruptEnableReg - enable IRQ generation from TGRA*/
    *TPU_TIER4 |= TIER4_TGIEAm;
    /*setup TGRA - sampling period*/
    *TPU_TGR4A = (CPU_SYS_HZ / 1000) / pxmc_sfikhz;
    /*Start TCNTn*/
    *TPU_TSTR |= TSTR_CST4m;
}


/**
 * This function set up pwm for out and hal sensors as i/o.
 * TODO:	change form hex to names
 *			generalization for more than one motor
 */
void pxmc_set_pwm_for_rotation()
{
    *DIO_PJDDR &= ~0xf8;	// hal(d,e,f and g,h) is in as i/o
    *DIO_PJDDR |= 0x07;	// led-s are out as i/o
    //*DIO_PHDDR|=0x15;	// 3 windings as out

    *PWM_PWOCR1 |= 0x15;	// enable output as pwm
    *PWM_PWOCR2 &= ~0x07;	// switch off: A,B,C for PWM - leds - now as i/o
    *PWM_PWOCR2 &= ~0xf8;	// switch off: D,E,F(and g,h) for PWM - hal - now as i/o

    //*PWM_PWPR1=0x00;	// normal polarity
    *PWM_PWCR1 &= ~0x08; // be sure clock is off
    //*PWM_PWCYR1=0x0f;	// minimal duty
    *PWM_PWCR1 |= 0xc8; // 8-start clock, ph/1
    *PWM_PWCR2 |= 0xc8;
    *SYS_MSTPCRD &= ~MSTPCRD_PWMm;
}
//=========================================
/*	Control functions for brushless motor...*/
//=========================================

int tpu_irc_ap2hw(struct pxmc_state *mcs)
{
    short old_pos;
    if (mcs->pxms_inp_info)
    {
#ifdef PXMC_WITH_PHASE_TABLE
        pxmc_clear_flag(mcs, PXMS_PHA_b);
        old_pos = *(short *)mcs->pxms_inp_info;
        mcs->pxms_ptofs += (mcs->pxms_ap>>PXMC_SUBDIV(mcs)) - old_pos;
#endif
        *(short *)mcs->pxms_inp_info = (mcs->pxms_ap>>PXMC_SUBDIV(mcs));
    }
    return 0;
}

static int motor_do_input(struct pxmc_state *mcs)
{
    short irc;
    irc = *(volatile short*)mcs->pxms_inp_info;
    pxmc_irc_16bit_update(mcs, irc);

    /* Running of the motor commutator */
    //irc=mcs->pxms_rp>>PXMC_SUBDIV(mcs);	// when enabled means without feedback
    if (mcs->pxms_flg&PXMS_PTI_m) {
        pxmc_irc_16bit_commindx(mcs, irc);
    }

    return 0;
}

#ifndef DEBUG
static
#endif

#if HW_VER_CODE == VER_CODE(1,0,0)
/* Eurobot 2007 */
inline unsigned char hal_read(struct pxmc_state *mcs)
{
    char hal = *DIO_PORTB;
    if (mcs->pxms_out_info==0)
        return (hal&0x14)>>2 | (hal&0x20)>>4;
    else
        return (hal&0x0a)>>1 | (hal&0x01)<<1;
}
#endif /* VER_CODE(1,0,0) */

#if HW_VER_CODE == VER_CODE(1,1,0)
/* Eurobot 2008 */
inline unsigned char hal_read(struct pxmc_state *mcs)
{
    char hal = *DIO_PORTB;
    /* A and B HAL connectors are switched on the board with respect
     * to the schematics! */
    if (mcs->pxms_out_info==0)
      return hal&0x07;
    else
      return (hal&0x38) >> 3;
}
#endif

static inline void process_hal_error(struct pxmc_state *mcs)
{
  if (mcs->pxms_halerc >= HAL_ERR_SENSITIVITY*HAL_ERR_MAX_COUNT) {
    pxmc_set_errno(mcs, PXMS_E_HAL);
    mcs->pxms_ene = 0; //FIXME is it correct to stop the motor this way?
    mcs->pxms_halerc--;
  } else
    mcs->pxms_halerc+=HAL_ERR_SENSITIVITY;
}

int motor_do_output(struct pxmc_state *mcs)
{
    unsigned short pwm1, pwm2, pwm3;
    short ene;
    int indx = 0;
    unsigned char hal_pos;
    const mark_source_des_t *mdes;

    hal_pos = eurobot_bdc_hal_pos_table[hal_read(mcs)];

    ene = mcs->pxms_ene;

    if (!(mcs->pxms_flg&PXMS_PTI_m) || (mcs->pxms_flg&PXMS_PRA_m))
    {
        short ptindx;
        short ptirc = mcs->pxms_ptirc;
        short divisor = mcs->pxms_ptper * 6;

        if (hal_pos == 0xff) {
          if (ene > 0) process_hal_error(mcs);
        } else {
            if (mcs->pxms_halerc > 0) mcs->pxms_halerc--; /* Slowly foget errors */

            if (!(mcs->pxms_flg&PXMS_PTI_m)) {

                ptindx = (hal_pos * ptirc + divisor / 2) / divisor;

                if ((hal_pos!=mcs->pxms_hal) && (mcs->pxms_hal!=0x40) && 1) {
                    /* align on HAL change - NOT WORKING AND UNUSED NOW */
                    short ptindx_prev = (mcs->pxms_hal * ptirc + divisor / 2) / divisor;
                    if((ptindx > ptindx_prev + ptirc / 2) ||
                       (ptindx_prev > ptindx + ptirc / 2)) {
                        ptindx = (ptindx_prev + ptindx - ptirc) / 2;
                        if(ptindx < 0)
                            ptindx += ptirc;
                    }else{
                            ptindx = (ptindx_prev+ptindx)/2;
                    }

                    mcs->pxms_ptindx=ptindx;

                    mcs->pxms_ptofs=(mcs->pxms_ap>>PXMC_SUBDIV(mcs))+mcs->pxms_ptshift-ptindx;

                    mdes = &mark_source[mcs->pxms_out_info];
	            *mdes->tsr &= ~TSR1_TGFAm;

                    pxmc_set_flag(mcs,PXMS_PTI_b);
                    pxmc_clear_flag(mcs,PXMS_PRA_b);
                } else {
                    mcs->pxms_ptindx=ptindx;
                }
                mcs->pxms_hal = hal_pos;
            }
        }
    } else {
        if (hal_pos != 0xff)
            mcs->pxms_hal = hal_pos;

        if(!(mcs->pxms_flg & PXMS_PHA_m) && (mcs->pxms_cfg & PXMS_CFG_I2PT_m)) {
            mdes = &mark_source[mcs->pxms_out_info];
            if(*mdes->tsr & TSR1_TGFAm) {
	        if(mcs->pxms_ptmark && 1) {
                    mcs->pxms_ptofs = *mdes->tgr - mcs->pxms_ptmark;
                    pxmc_set_flag(mcs, PXMS_PHA_b);
	        }
	        *mdes->tsr = ~TSR1_TGFAm;
	    }
        }
    }

    if (ene)
    {
        indx = mcs->pxms_ptindx;
        if (ene < 0) {
            // Generating direction of stator mag. field for backward torque
            ene = -ene;
            indx -= mcs->pxms_ptvang;
            if (indx < 0)
	        indx += mcs->pxms_ptirc;
        } else {
            // Generating direction of stator mag. field for forward torque
            indx += mcs->pxms_ptvang;
            if (indx >= mcs->pxms_ptirc)
	        indx -= mcs->pxms_ptirc;
        }
    }

    //indx %= mcs->pxms_ptirc;
    pwm1 = mcs->pxms_ptptr1[indx];
    pwm2 = mcs->pxms_ptptr2[indx];
    pwm3 = mcs->pxms_ptptr3[indx];

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    pwm1 = (((unsigned long)pwm1*(unsigned int)ene) >> (15+5));
    pwm2 = (((unsigned long)pwm2*(unsigned int)ene) >> (15+5));
    pwm3 = (((unsigned long)pwm3*(unsigned int)ene) >> (15+5));

    if (mcs->pxms_out_info==0) {
      /* Don't overwrite second motor PWM values. Wait for next pwm cycle */
      *PWM_PWCR2 &= ~PWCR2_CMFm; /* Clear compare match flag */
      while ((*PWM_PWCR2 & PWCR2_CMFm) == 0);

      *PWM_PWBFR2A = pwm1;
      *PWM_PWBFR2B = pwm2;
      *PWM_PWBFR2C = pwm3;
    #ifdef MOTOR_PWM2D_REPLACE_A 
      *PWM_PWBFR2D = pwm1;
    #endif

      *PWM_PWCR2 &= ~PWCR2_CMFm; /* Clear compare match flag */
    } else {
      /* Don't overwrite second motor PWM values. Wait for next pwm cycle */
      *PWM_PWCR2 &= ~PWCR2_CMFm; /* Clear compare match flag */
      while ((*PWM_PWCR2 & PWCR2_CMFm) == 0);

      *PWM_PWBFR2A = PWBFR2A_TDSm | pwm1;
      *PWM_PWBFR2B = PWBFR2B_TDSm | pwm2;
      *PWM_PWBFR2C = PWBFR2C_TDSm | pwm3;

      *PWM_PWCR2 &= ~PWCR2_CMFm; /* Clear compare match flag */
    }

    return 0;
}

int pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
	return -1;
}

/* No homing is provided by hardware */
pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return NULL;
}

int motor_pthalalign_callback(pxmc_state_t *mcs)
{
  short ptofs;
  short ptmark;
  int irc = *(volatile short*)mcs->pxms_inp_info;
  int idx_rel;
  const mark_source_des_t *mdes = &mark_source[mcs->pxms_out_info];

  if(!(*mdes->tsr & TSR1_TGFAm))
    return 0;

  idx_rel = *mdes->tgr - irc;
  idx_rel %= mcs->pxms_ptirc;
  if(idx_rel < 0)
    idx_rel += mcs->pxms_ptirc;

  ptofs = irc-mcs->pxms_ptofs;
  ptmark = ptofs+idx_rel;

  if((unsigned short)ptmark >= mcs->pxms_ptirc) {
    if(ptmark>0)
      ptmark -= mcs->pxms_ptirc;
    else
      ptmark += mcs->pxms_ptirc;
  }

  if((unsigned short)ptmark < mcs->pxms_ptirc) {
    mcs->pxms_ptmark = ptmark;
  }
  return 0;
}

int motor_pthalalign(pxmc_state_t *mcs, int periods)
{
  int res;
  long r2acq;
  long spd;
  pxmc_call_t *fin_fnc=motor_pthalalign_callback;
  const mark_source_des_t *mdes = &mark_source[mcs->pxms_out_info];

  mcs->pxms_cfg&=~PXMS_CFG_I2PT_m;

  *mdes->tsr = ~TSR1_TGFAm;

  r2acq=((long)mcs->pxms_ptirc<<PXMC_SUBDIV(mcs))*periods;
  spd=1<<PXMC_SUBDIV(mcs);

  res=pxmc_pthalalign(mcs, r2acq, spd, fin_fnc);

  return res;
}


/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:        Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  return (long)pxmc_sfikhz*1000;
}


int pxmc_initialize(void)
{
    init_pwm();
    init_hal();
    init_irc();
    init_sampling();
    return 0;
};



/* Local Variables: */
/* c-basic-offset: 2 */
/* End */
