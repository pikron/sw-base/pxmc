#ifndef _PXMC_EUROBOT_H
#define _PXMC_EUROBOT_H

#include <pxmc.h>

extern pxmc_state_t mcs_right, mcs_left;
extern volatile unsigned pxmc_msec_counter; // miliseconds counter

extern long alfa; // angle in which robot is turned at the begin
extern int x,y;  // initial position of the robot
//extern int xl,yl;  // initial position of wheels
//extern int xr,yr;   // it is strong correlated with alfa

/* #define LED_INDEX1 0 */
/* #define LED_INDEX2 1 */

extern int pxmc_sfikhz;	// Sampling freq. interrupt - how often we want to collect data
extern const char pxmc_variant[];

int motor_pthalalign(pxmc_state_t *mcs, int periods);

#define DEBUG
#ifdef DEBUG
int motor_do_output(struct pxmc_state *mcs);
inline unsigned char hal_read(struct pxmc_state *mcs);
#endif

#define pxmcbsp_is_home(mcs) 0 //(*DIO_PORTF&0x80)

#endif
