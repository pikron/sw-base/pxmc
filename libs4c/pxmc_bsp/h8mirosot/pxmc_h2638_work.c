/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_h2638.c - generic multi axis motion controller
                 h8s2638 hrdware specific functions
// 
  (C) 2001-2005 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2005 by PiKRON Ltd. http://www.pikron.com
  (C) 2005 Petr Kovacik <kovacp1@Guillaume>

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/*  [extern API] characteristic means that the function is declared
in the header file pxmc.h so it is part of the external API */

#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include "pxmc.h"
#include "pxmc_internal.h"
#include "pxmc_inp_common.h"
#include "pxmc_h2638.h"

#ifndef PXMC_SFI_TPU_CHN
#define PXMC_SFI_TPU_CHN 4 		/* TPU channel used for timing */
#endif

// #define PXMC_USE_VANG		/* Use pxms_ptvang for mag.fld. offs */
//
// #define PXMC_WITH_TPUCUR		/* Measurement of phase current by TPU */

/* Phase counting clock sources */
/*  TPCNT1, TPCNT5 .. TCLKA, TCLKB */
/*  TPCNT2, TPCNT4 .. TCLKC, TCLKD */

// #if HW_VER_CODE<VER_CODE(0,2,0)
//   #define PXMC_IRC_TPUN 2	/* TPU channel used for IRC */
// #else /* >=VER_CODE(0,2,0) */
//   #define PXMC_IRC_TPUN 5	/* TPU channel used for IRC */
// #endif /* >=VER_CODE(0,2,0) */

//pxmc_state_t mcs0;

int pxmc_sfikhz=2;
int pxmc_sfi2msec;
#define  pxmc_cyclekhz 20
#define pwm_per (((CPU_SYS_HZ/16)/1000)/pxmc_cyclekhz)


#define TEMP_PXMC_TPU_SFI_INIT(n)\
  do{ \
  *TPU_TSTR&=~TSTR_CST##n##m;	/*counter n is stoped*/\
  /*CNTN cleared by by TGRA, rising edge; source TGRA compare match/input capture  */\
  *TPU_TCR##n=(TPCR_TPSC_F1 | TPCR_CKEG_RIS | TPCR_CCLR_TGRA); \
  /*TGRA is output compare registr - . 0 output at compare match*/\
  *TPU_TIOR##n|=TIOR##n##_IOA1m; \
  /*MDn = 0x000 normal operation */\
  *TPU_TMDR##n=TPMDR_MD_NORMAL; \
  /*seting interupt vector - from TGRA at registr TSRn*/\
  excptvec_set(EXCPTVEC_TGI##n##A,pxmc_sfi_isr); \
  /*TimerInteruptEnableReg - enable IRQ generation from TGRA*/\
  *TPU_TIER##n |=TIER##n##_TGIEAm; \
  /*setup TGRA - sampling period*/\
  *TPU_TGR##n##A=(CPU_SYS_HZ/1000)/pxmc_sfikhz; \
  /*TimerStatusRegistr - clear match flag */\
  *TPU_TSR##n &=~TSR##n##_TGFAm; \
  /*Start TCNTn*/\
  *TPU_TSTR|=(1<<n);\
  }while (0)

// TRICK: See cpp info pages, node Argument Prescan, 
// parahraph "Macros that call other macros that stringify or concatenate."
#define PXMC_TPU_SFI_INIT(n) TEMP_PXMC_TPU_SFI_INIT(n)

/*---------------------------------------------------------------------*/

#define TEMP_PXMC_TPU_PHASE_CNT_INIT(n) \
  do{ \
  /*TimerControlRegistr - TCNT clearing disabled; count rasing adge; internal clock fi/1 */\
  *TPU_TCR##n=0; \
  /*phase counting mode 1 - how will be TCNT incrementd */\
  *TPU_TMDR##n=TPMDR_MD_PHACN1; \
  /*AD conversion disabled; interrupts from TCFU(TCFV,TGIA,TGIB) all interupt disabled.; */\
  *TPU_TIER##n=0; \
  /*start TCNT*/\
  *TPU_TSTR|=(1<<n); \
  }while (0)

// TRICK: See cpp info pages, node Argument Prescan, 
// parahraph "Macros that call other macros that stringify or concatenate."
#define PXMC_TPU_PHASE_CNT_INIT(n) TEMP_PXMC_TPU_PHASE_CNT_INIT(n)


extern void deb_led_out(char val);

#if 0 //FOR DEBUGGING

#define error_led(led) \
    if (mcs->pxms_flg & PXMS_ERR_m) { \
        deb_led_out(led); \
        while(1);\
    }\

#else
#define error_led(led)
#endif
//-----------------------------
// pxmc_axis_rdmode() - they setting up mode=4 but it's impos. to read it
// _MOTOR_MODE_ - maybe it should be motor type (?)
// what is here exactly mode?
// what is 1 - near #if 1
// pxmc_set_pwm_tpu_new - wow that was stayed is einfach great :o)
// PXMC_IRC_TPUN - what is it?
// HW_VER_CODE<VER_CODE(0,2,0) - what (for) is this 
//-----------------------------
enum _BOOL_ {false=0,true=1};
typedef enum _BOOL_ bool;

/**
 *		0 .. previous mode, 
 *		1 .. stepper motor mode,
 *		2 .. stepper motor with IRC feedback and PWM ,
 *		3 .. stepper motor with PWM control
 *      4 .. DC motor with IRC feedback and PWM
*/ 
enum _MOTOR_MODE_ {m_stepper=1,m_stepper_irc_pwm=2,m_stepper_pwn=3,m_dc_irc_pwm=4};
typedef enum _MOTOR_MODE_ motor_mode_e;

inline bool pxmc_set_default_services(pxmc_state_t *mcs,motor_mode_e mode)
{
	switch(mode)
	{
		case m_stepper:
			/*mcs->pxms_do_inp=pxmc_nofb_inp;
			mcs->pxms_do_con=pxmc_nofb_con;
			mcs->pxms_do_out=pxmc_nofb_out;*/
		break;
		case m_stepper_irc_pwm:
			//mcs->pxms_do_inp=pxmc_tpuirc_inp;			/*set up pointer on function measure IRC*/
			//mcs->pxms_do_out=pxmc_pwm1f_out;	//pointer on funct. control energi for outpu (PWM)
			//pxmc_tpupwm2f_tpuwr
			//pxmc_tpuirc_nofb_inp
			//pxmc_tpupwm2f_out
			//pxmc_cpme_con
		break;
		case m_stepper_pwn:
		break;
		case m_dc_irc_pwm:
			/*mcs->pxms_do_inp=pxmc_nofb_inp;
			mcs->pxms_do_con=pxmc_nofb_con;
			mcs->pxms_do_out=pxmc_nofb_out;
			*/  
			mcs->pxms_do_inp=pxmc_tpuirc_inp;	/*set up pointer on function measure IRC*/
			mcs->pxms_do_out=pxmc_pwm1f_out;	//pointer on funct. control energi for outpu (PWM)
			mcs->pxms_do_con=pxmc_pid_con;		/*set up pointer on PID con. function  (implicit (PXMC.c) or user*/
		break;
		default:
			return false;
	}
   
   return true;
}

inline short pxmc_set_max_energy(pxmc_state_t *mcs)
{
   int HelVar=pwm_per;
   return (mcs->pxms_me = HelVar << 7);         //max energy enter in a motor
}

inline bool pxmc_set_default_debug(pxmc_state_t *mcs,motor_mode_e mode)
{
	switch(mode)
	{
		case m_stepper:
		break;
		case m_stepper_irc_pwm:
		break;
		case m_stepper_pwn:
		break;
		case m_dc_irc_pwm:
			   if(!pxmc_dbg_hist) pxmc_dbg_hist=0; /* FIXME: Are here correct braces??? */
			   pxmc_dbg_hist=pxmc_dbg_histalloc(4096);
			   pxmc_dbgset(mcs, pxmc_dbg_pwm12, 1);		
			   pxmc_set_flag(mcs,PXMS_DBG_b);
		break;
		default:
			return false;
	}
   
   return true;
}


void pxmc_set_pwm_tpu_new(void)
{
	int num_motors = pxmc_main_list.pxml_cnt;

	*SYS_MSTPCRD &= ~MSTPCRD_PWMm; /*set up Gate as PWM gate (not I/O)*/
	*SYS_MSTPCRA &= ~MSTPCRA_TPUm; /*set up Gate as TPU (not I/O) */

	*PWM_PWCR1 =0xC4;//interrupt disabled, counter stopped, frequency phi/16

	unsigned char pwm_pwocr1[]={PWOCR1_OE1Am,PWOCR1_OE1Bm,PWOCR1_OE1Cm,PWOCR1_OE1Dm,\
  		PWOCR1_OE1Em,PWOCR1_OE1Fm,PWOCR1_OE1Gm,PWOCR1_OE1Hm};

	unsigned char pwm_pwpr1[]={PWPR1_OPS1Am,PWPR1_OPS1Bm,PWPR1_OPS1Cm,PWPR1_OPS1Dm,\
		PWPR1_OPS1Em,PWPR1_OPS1Fm,PWPR1_OPS1Gm,PWPR1_OPS1Hm};
	
  int r=0;
  for(r=0;r<num_motors;r++)
  {	// motors - output PWM 1A and 1B
	  *PWM_PWOCR1 |= ( pwm_pwocr1[2*r] | pwm_pwocr1[2*r+1] ); 
	  *PWM_PWPR1 &= ~((pwm_pwpr1[2*r]) | (pwm_pwpr1[2*r+1]));
  }
  
  if(num_motors>0)  /*motor 1 - output PWM 1A and 1B*/
  {
    /*TPU seting*/
#if 1
    *DIO_P1DDR &= ~(P1DDR_P12DDRm | P1DDR_P13DDRm);  	/*TCLKA and TCLKB are inputs*/
    PXMC_TPU_SFI_INIT(PXMC_SFI_TPU_CHN); 
    PXMC_TPU_PHASE_CNT_INIT(1);				/*macro for inicialized TPU - mod 1*/
#endif
  };
  if(num_motors>1)  /*motor 2 - output PWM 1C and 1D*/
  {
    /*TPU seting*/
    *DIO_P1DDR &= ~(P1DDR_P15DDRm | P1DDR_P17DDRm);	/*TCLKC and TCLKD are inputs*/
//    PXMC_TPU1(4); 					/*macro for inicialized TPU*/
    PXMC_TPU_PHASE_CNT_INIT(2); 			/*macro for inicialized TPU - mod 1*/ 
  };
  //????????????????????
 // WLACZENIE PWM jesli jest choc jeden silnik ???????????
//????????????????????
  if(num_motors>0)  /*motor 1 - output PWM 1A and 1B*/
  {
    *PWM_PWCYR1 = pwm_per;    //constant set up period of PWM counter
    *PWM_PWCR1 |=PWCR1_CSTm; /*start PWM counter*/
  };
}


//-----------------------------
__attribute__ ((deprecated)) inline void pxmc_add_pservice_and_mode(short mod)
{
	int var;
    for (var = 0; var < pxmc_main_list.pxml_cnt; var++) {
      /* Set default motor behaviour */
      /*pxmc_main_list.pxml_arr[var]->pxms_do_inp=pxmc_nofb_inp;
      pxmc_main_list.pxml_arr[var]->pxms_do_con=pxmc_nofb_con;
      pxmc_main_list.pxml_arr[var]->pxms_do_out=pxmc_nofb_out;
      */
	  pxmc_set_default_services(pxmc_main_list.pxml_arr[var],mod);
      pxmc_axis_mode(pxmc_main_list.pxml_arr[var], mod);
    }
}


/* direct, one mcs processing */
inline void pxmc_tpu_interupt_eni()
{
  int var=0;
  for (var = 0; var < pxmc_main_list.pxml_cnt; var++) {
    /* direct, one mcs processing */
    if(pxmc_main_list.pxml_arr[var]->pxms_flg&PXMS_ENI_m)
  	{
      /* pxmc_nofb_inp(mcs); */
      pxmc_call(pxmc_main_list.pxml_arr[var],pxmc_main_list.pxml_arr[var]->pxms_do_inp);
      error_led(~1);
    }
  } 
}


inline void pxmc_tpu_interupt_eng()
{
  int var=0;
  for (var = 0; var < pxmc_main_list.pxml_cnt; var++) {
    if(pxmc_main_list.pxml_arr[var]->pxms_flg&PXMS_ENG_m)
    {
      /* pxmc_spdfg_gnr(pxmc_main_list.pxml_arr[var]); */
      pxmc_call(pxmc_main_list.pxml_arr[var],pxmc_main_list.pxml_arr[var]->pxms_do_gen);
      error_led(~2);
    }
  }
}
   
inline void pxmc_tpu_interupt_enr()
{
  int var=0;
  for (var = 0; var < pxmc_main_list.pxml_cnt; var++) {
    if(pxmc_main_list.pxml_arr[var]->pxms_flg&PXMS_ENR_m)
    {
      /* pxmc_nofb_con(mcs); */
      pxmc_call(pxmc_main_list.pxml_arr[var],pxmc_main_list.pxml_arr[var]->pxms_do_con);
      error_led(~4);
      if(pxmc_main_list.pxml_arr[var]->pxms_flg&PXMS_ERR_m)
        pxmc_main_list.pxml_arr[var]->pxms_ene=0;
      /* pxmc_nofb_out(mcs); */
      pxmc_call(pxmc_main_list.pxml_arr[var],pxmc_main_list.pxml_arr[var]->pxms_do_out);
      error_led(1);
    }
  }
}
   
 
inline void pxmc_tpu_interupt_dbg()
{
  int var=0;
  for (var = 0; var < pxmc_main_list.pxml_cnt; var++) {
    if(pxmc_main_list.pxml_arr[var]->pxms_flg&PXMS_DBG_m) {
      pxmc_call(pxmc_main_list.pxml_arr[var],pxmc_main_list.pxml_arr[var]->pxms_do_deb);
      error_led(2);
    }
    if(--pxmc_sfi2msec<=0) {
      msec_time++;
      pxmc_sfi2msec=pxmc_sfikhz;
      /* Hook for external finite state machine */
    }else {
      /* Run ADC measurements */
    }
  }
}

/********************************************************************/
/* Stepper controller hardware dependant part */

#define USE_BUF_PWM

/*------------------------------------------------------------------*/
/* Stepper motor with IRC feedback */

short pxmc_tpupwm2f_mult;
//int pxmc_sfikhz;

/**
 * pxmc_tpuirc_inp - IRC encoder connected to H8S 2638 TPU
 * @mcs:	Motion controller state information
 *
 * This version of input routine updates @pxms_ap from TPU counter.
 * Acquired value is then used for phase commutation..
 */
int pxmc_tpuirc_inp(struct pxmc_state *mcs)
{
  short irc;
  irc=*(volatile short*)mcs->pxms_inp_info;
  pxmc_irc_16bit_update(mcs,irc);

  /* Running of the motor commutator */
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  return 0;
}

/**
 * pxmc_tpuirc_nofb_inp - TPU IRC input without feedback commutation control
 * @mcs:	Motion controller state information
 *
 * This version of input routine updates @pxms_ap from TPU counter,
 * but commutation is controlled by @pxms_rp.
 */
inline int pxmc_tpuirc_nofb_inp(struct pxmc_state *mcs)
{
  short irc;
  irc=*(volatile short*)mcs->pxms_inp_info;
  pxmc_irc_16bit_update(mcs,irc);

  /* Running of the motor commutator */
  irc=mcs->pxms_rp>>PXMC_SUBDIV(mcs);
  if(mcs->pxms_flg&PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs,irc);

  return 0;
}

#if HW_VER_CODE<VER_CODE(0,2,0)

static inline void pxmc_tpupwm2f_tpuwr(struct pxmc_state *mcs, short pwm1, short pwm2)
{
  char mask=0;

  if(pwm1>0) mask|=1;
  else if(pwm1<0) {
    mask|=2;
    pwm1=-pwm1;
  }
  if(pwm2>0) mask|=4;
  else if(pwm2<0) {
    mask|=8;
    pwm2=-pwm2;
  }
  *STPM_OUT=mask;

  pwm1=((long)pwm1*pxmc_tpupwm2f_mult)>>16;
  pwm2=((long)pwm2*pxmc_tpupwm2f_mult)>>16;
  /* Output PWM requests into TPU registers */
  /* TPU logic becomes mad if width and period is same */
  /* and it cannot output zero width */
  /* full fill is possible by width longer than period */
#ifndef USE_BUF_PWM
  *TPU_TPGR0A=pwm1;
  *TPU_TPGR0B=pwm2;
#else /* USE_BUF_PWM */
  *TPU_TPGR0C=pwm1;
  *TPU_TPGR0D=pwm2;

#endif /* USE_BUF_PWM */
}

#else /* >=VER_CODE(0,2,0) */

static inline void pxmc_tpupwm2f_tpuwr(struct pxmc_state *mcs, short pwm1, short pwm2)
{
  if(pwm1>0){
    pwm1+=mcs->pxms_pwm1cor;
//     atomic_set_mask_b1(PFDR_PF0DRm,DIO_PFDR);
  }else if(pwm1<0) {
    pwm1=-pwm1+mcs->pxms_pwm1cor;
//     atomic_clear_mask_b1(PFDR_PF0DRm,DIO_PFDR);
  }
  if(pwm2>0){
    pwm2+=mcs->pxms_pwm2cor;
//     atomic_clear_mask_b1(PFDR_PF1DRm,DIO_PFDR);
  }else if(pwm2<0) {
    pwm2=-pwm2+mcs->pxms_pwm2cor;
//     atomic_set_mask_b1(PFDR_PF1DRm,DIO_PFDR);
  }

  pwm1=((long)pwm1*pxmc_tpupwm2f_mult)>>16;
  pwm2=((long)pwm2*pxmc_tpupwm2f_mult)>>16;
  /* Output PWM requests into TPU registers */
  /* TPU logic becomes mad if width and period is same */
  /* and it cannot output zero width */
  /* full fill is possible by width longer than period */


#ifndef USE_BUF_PWM
  *TPU_TGR0A=pwm1;
  *TPU_TGR0B=pwm2;
#else /* USE_BUF_PWM */
  *TPU_TGR0C=pwm1;
  *TPU_TGR0D=pwm2;
#endif /* USE_BUF_PWM */
}

#endif /* >=VER_CODE(0,2,0) */

/**
 * pxmc_tpupwm2f_out - Two phase stepper motor phase PWM output
 * @mcs:	Motion controller state information
 *
 * calls pxmc_tpupwm2f_tpuwr which, ifndef USE_BUF_PWM,
 * uses (TPU_TGR0A, TPU_TGR0B) timers for PWM registers
 * else (TPU_TGR0C, TPU_TGR0D)
 */
int pxmc_tpupwm2f_out(struct pxmc_state *mcs)
{
  short pwm1;
  short pwm2;
  short indx;
  short ene;

  indx=mcs->pxms_ptindx;
  ene=mcs->pxms_ene;
  if(ene==0){
    pwm1=pwm2=0;
  }else{
#ifdef PXMC_USE_VANG
    if(ene<0){
      /* Generating direction of stator mag. field for backward torque */
      ene=-ene;
      if((indx-=mcs->pxms_ptvang)<0)
        indx+=mcs->pxms_ptirc;
    }else{
      /* Generating direction of stator mag. field for forward torque */
      if((indx+=mcs->pxms_ptvang)>=mcs->pxms_ptirc)
        indx-=mcs->pxms_ptirc;
    }
#endif /* PXMC_USE_VANG */
    pwm1=mcs->pxms_ptptr1[indx];
    pwm2=mcs->pxms_ptptr2[indx];

    pwm1=((long)pwm1*ene)>>15;
    pwm2=((long)pwm2*ene)>>15;

  }

  pxmc_tpupwm2f_tpuwr(mcs,pwm1,pwm2);
  return 0;
}

int pxmc_dbg_pwm12(pxmc_state_t *mcs)
{
  long *ptr;
  if(!(mcs->pxms_flg&PXMS_ENG_m))
    return 0;
  if(pxmc_dbg_hist){
    ptr=pxmc_dbg_hist->ptr;
    if(ptr&&(ptr<pxmc_dbg_hist->end-1)){
      *(ptr++)=*TPU_TGR0A;
      *(ptr++)=*TPU_TGR0B;
      pxmc_dbg_hist->ptr=ptr;
    }
  }
  return 0;
}

inline int pxmc_cpme_con(pxmc_state_t *mcs)
{
  mcs->pxms_ene=mcs->pxms_me;
  return 0;
}

/*------------------------------------------------------------------*/
/* Stepper motor without feedback */

/**
 * pxmc_nofb_out - Phase output for open loop direct stepper motor control
 * @mcs:	Motion controller state information
 */
int pxmc_nofb_out(pxmc_state_t *mcs)
{
  short pwm1;
  short pwm2;
  short indx;
  short ene;

  ene=mcs->pxms_ene;
  if(ene){
    pxmc_irc_16bit_commindx(mcs, mcs->pxms_ap>>PXMC_SUBDIV(mcs));
    indx=mcs->pxms_ptindx;
    pwm1=mcs->pxms_ptptr1[indx];
    pwm2=mcs->pxms_ptptr2[indx];

    pwm1=((long)pwm1*ene)>>15;
    pwm2=((long)pwm2*ene)>>15;

    pxmc_tpupwm2f_tpuwr(mcs,pwm1,pwm2);
    return 0;
  }else{
    pxmc_tpupwm2f_tpuwr(mcs,0,0);
    return 0;
  }
}



/**
 * pxmc_nofb_inp - Dummy input for direct stepper motor control
 * @mcs:	Motion controller state information
 */
int pxmc_nofb_inp(pxmc_state_t *mcs)
{
  return 0;
}

/**
 * pxmc_nofb_con - Empty controller for direct stepper motor control
 * @mcs:	Motion controller state information
 */
int
pxmc_nofb_con(pxmc_state_t *mcs)
{
  mcs->pxms_ap=mcs->pxms_rp;
  mcs->pxms_as=mcs->pxms_rs;
  mcs->pxms_ene=mcs->pxms_me;
  return 0;
}


/*------------------------------------------------------------------*/

/* sampling frequency interrupt handler */
/* int intno, void *dev_id, struct pt_regs *regs */
void pxmc_sfi_isr(void) __attribute__ ((interrupt_handler));

/**
 * pxmc_sfi_isr - Stepper motor interrupt service routine
 *
 *(I do not understand the following comment, maybe these are features that will be written later??)
 *
 * This is basic routine calling motion controller functions.
 */


void pxmc_sfi_isr(void)
{ 
  short local_var=0xff;
  
  local_var=*TPU_TSR4;
  if(local_var & TSR4_TGFAm)
  {
    *TPU_TSR4&=~TSR4_TGFAm;  //FIXME:zakazat nejak rozumne preruseni pro vsechny motory a ne jenom preruseni 4 tj.Motor4
  };
  local_var=*TPU_TSR5;
  if(local_var & TSR5_TGFAm)
  {
    *TPU_TSR5&=~TSR5_TGFAm;  //FIXME:zakazat nejak rozumne preruseni pro vsechny motory a ne jenom preruseni 4 tj.Motor4
  };
  
  /* Clear interrupt source */
  
  /*PXMS_ENG=enable input (IRC) update */
  pxmc_tpu_interupt_eni();	/*macro for service Interupt for all defined motors*/
 
  /*PXMS_ENG - enable requested value (position) generator*/
  pxmc_tpu_interupt_eng();	/*macro for service Interupt for all defined motors*/
  
  /*enable controller*/
  pxmc_tpu_interupt_enr(); 	/*macro for service Interupt for all defined motors*/
  
  /* enable debugging */
  pxmc_tpu_interupt_dbg();	/*macro for service Interupt for all defined motors*/
}

/********************************************************************/
/* Stepper controller tests */

#include <math.h>
#include <cmd_proc.h>

/********************************************************************/
/* DC motor controller hardware dependant part */


/*------------------------------------------------------------------*/
/* DC motor with IRC feedback */

/**
 * pxmc_pwm1f_out - two phase PWM output for DC motor
 * @mcs:	Motion controller state information
 *
 * This function sets directly duty cycle register in
 * PWM_PWBFR1n n=(A,C,E,G) buffer register.
 */
int  pxmc_pwm1f_out(struct pxmc_state *mcs)
{
  short ene=0;
  ene=mcs->pxms_ene;
  if(ene>=0)
  {
/*   #ifdef BOARD_MIROSOT
 *(volatile __u16 *)(mcs->pxms_out_info) = ((ene) >> 7)|0x1000;
 #else    */
    *(volatile __u16 *)(mcs->pxms_out_info) = ((ene) >> 7)&0xefff;
//   #endif
   }

  else
  {
/* #ifdef BOARD_MIROSOT
    *(volatile __u16 *)(mcs->pxms_out_info) = (((-ene) >> 7))&0xefff;
  #else    */
    *(volatile __u16 *)(mcs->pxms_out_info) = (((-ene) >> 7))|0x1000;
//  #endif

  }
  return 0;
}

/**
 * pxmc_axis_rdmode - Reads actual axis mode.[extern API]
 * @mcs:	Motion controller state information
 */
int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
  if(mcs->pxms_do_con==pxmc_nofb_con)
    return 1;
  if(mcs->pxms_do_con==pxmc_pid_con)
    return 2;
  if(mcs->pxms_do_con==pxmc_cpme_con)
    return 3;
  return 0;
}

/**************************area change ****************/
/*pxmc.c - line 1586 and px,c%h2638.c line 494*/

 //fce pxmc_axis_mode - is not pxmc.c and here
/**************************area change ****************/


int tpu_irc_ap2hw(struct pxmc_state *mcs) {
  short old_pos;
  if(mcs->pxms_inp_info) {
#ifdef PXMC_WITH_PHASE_TABLE
    pxmc_clear_flag(mcs,PXMS_PHA_b);
    old_pos = *(short *)mcs->pxms_inp_info;
    mcs->pxms_ptofs+=mcs->pxms_ap-old_pos;
#endif
    *(short *)mcs->pxms_inp_info=mcs->pxms_ap;
  }
  return 0;
}


/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:	Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  return (long)pxmc_sfikhz*1000;
}

#ifdef WITH_SFI_SEL
/**
 * pxmc_sfi_sel - Setting sampling frequency of PXMC subsystem (TPU_TGR4A)
 * @sfi_hz:	Requested sampling frequency in Hz
 *
 * Function returns newly selected sampling frequency
 * after rounding or -1 if there is problem to set
 * requested frequency.
 */
long pxmc_sfi_sel(long sfi_hz)
{
  int sfikhz=(sfi_hz+500)/1000;
  if((sfikhz<2)||(sfikhz>12))
    return -1;
  *TPU_TSTR&=~(1<<4);		/* Stop channel 4 */
  pxmc_sfikhz=sfikhz;
  *TPU_TCNT4=0; //FIXME: Use macro PCMC_SFI_SEL
  *TPU_TGR4A=((CPU_SYS_HZ+500)/1000)/pxmc_sfikhz;
  *TPU_TSTR|=(1<<4);		/* Start channel 4 */
  return (long)pxmc_sfikhz*1000l;
}
#endif /* WITH_SFI_SEL */

/**
 * pxmc_axis_mode - Sets axis mode.[extern API]
 * @mcs:	Motion controller state information
 * @mode:	0 .. previous mode, 
 			1 .. stepper motor mode,
 *			2 .. stepper motor with IRC feedback and PWM ,
 *			3 .. stepper motor with PWM control
 *          4 .. DC motor with IRC feedback and PWM 
 *
 */
__attribute__ ((deprecated)) int pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
//  int ret;
  pxmc_set_const_out(mcs,0);
  pxmc_clear_flag(mcs,PXMS_ENI_b);
  pxmc_clear_flag(mcs,PXMS_PHA_b);
 
  if(!mode){
    mode=pxmc_axis_rdmode(mcs);
    if(!mode) mode=1;
  }
    
  switch(mode){
//     case 1 : /* Stepper motor mode */
//      #if HW_VER_CODE>=VER_CODE(0,2,0)
//       if((ret=pxmc_init_ptable(mcs,0))<0)
//         return ret;
//       /* necessary to initialize TPU for phase current control */
//       pxmc_stm_init_fbmode(mcs);
//      #endif /*HW_VER_CODE>=VER_CODE(0,2,0)*/
//       mcs->pxms_do_inp=pxmc_nofb_inp;
//       mcs->pxms_do_out=pxmc_nofb_out;
//       mcs->pxms_do_con=pxmc_nofb_con;
//       break;
//   
//     case 2 : /* Stepper motor with IRC feedback and PWM */
//       if((ret=pxmc_init_ptable(mcs,0))<0)
//         return ret;
//       pxmc_stm_init_fbmode(mcs);
//       mcs->pxms_do_con=pxmc_pid_con;
//     
//       break;
// 
//     case 3 : /* Stepper motor with PWM control */
//       if((ret=pxmc_init_ptable(mcs,0))<0)
//         return ret;
//       pxmc_stm_init_fbmode(mcs);
//       mcs->pxms_ptshift=0;
//       mcs->pxms_ptvang=0;
//       mcs->pxms_do_inp=pxmc_tpuirc_nofb_inp;
//       mcs->pxms_do_con=pxmc_cpme_con;
//       break;
      
    case 4 : /* DC motor with IRC feedback and PWM */
      pxmc_dcm_init_fbmode(mcs);
      break;
    default : 
		return -1;
  }
  pxmc_set_flag(mcs,PXMS_ENI_b);
  return 0;
}



/**
 * pxmc_dcm_init_fbmode - Initializes feedback mode control
 * @mcs:	Motion controller state information
 *
 * Function initializes TPU for IRC input and one phase
 * PWM output, generated by PWM unit, not TPU.
 * 
 * -> IRC inputs are TCLKC and TCLKD,
 * Timer 2 is in phase counting mode 1.
 * Timer 4 is used to count sampling period (set in TPU_TGR4A)
 * and activate pxmc_sfi_isr interrupt handler.
 * -> PWM outputs are A and B, the frequency of the signal is set in PWM_PWCYR1.
 */

void pxmc_set_pwm_tpu(void)
{
  int num_motors = pxmc_main_list.pxml_cnt;

  *SYS_MSTPCRD &= ~MSTPCRD_PWMm; /*set up Gate as PWM gate (not I/O)*/
  *SYS_MSTPCRA &= ~MSTPCRA_TPUm; /*set up Gate as TPU (not I/O) */

  *PWM_PWCR1 =0xC4;//interrupt disabled, counter stopped, frequency phi/16

  if(num_motors>0)  /*motor 1 - output PWM 1A and 1B*/
  {
    /*PWM seting*/
    *PWM_PWOCR1 |= (PWOCR1_OE1Am | PWOCR1_OE1Bm );
    *PWM_PWPR1 &= ~((PWPR1_OPS1Am)|(PWPR1_OPS1Bm));
    /*TPU seting*/
#if 1
    *DIO_P1DDR &= ~(P1DDR_P12DDRm | P1DDR_P13DDRm);  	/*TCLKA and TCLKB are inputs*/
    PXMC_TPU_SFI_INIT(PXMC_SFI_TPU_CHN); 
    PXMC_TPU_PHASE_CNT_INIT(1);				/*macro for inicialized TPU - mod 1*/
    
#endif
  };
  if(num_motors>1)  /*motor 2 - output PWM 1C and 1D*/
  {
    *PWM_PWOCR1 |= (PWOCR1_OE1Cm | PWOCR1_OE1Dm );
    *PWM_PWPR1 &= ~((PWPR1_OPS1Cm)|(PWPR1_OPS1Dm));
    /*TPU seting*/
    *DIO_P1DDR &= ~(P1DDR_P15DDRm | P1DDR_P17DDRm);	/*TCLKC and TCLKD are inputs*/
//    PXMC_TPU1(4); 					/*macro for inicialized TPU*/
    PXMC_TPU_PHASE_CNT_INIT(2); 			/*macro for inicialized TPU - mod 1*/ 
  };
  if(num_motors>2)   /*motor 3 - output PWM 1E and 1F*/
  {
  /*PWM seting*/
    *PWM_PWOCR1 |= (PWOCR1_OE1Em | PWOCR1_OE1Fm );
    *PWM_PWPR1 &= ~((PWPR1_OPS1Em)|(PWPR1_OPS1Fm));
  };
  if(num_motors>3)   /*motor 4 - output PWM 1G and 1H*/
  {
    *PWM_PWOCR1 = (PWOCR1_OE1Gm | PWOCR1_OE1Hm );
    *PWM_PWPR1 &= ~((PWPR1_OPS1Gm)|(PWPR1_OPS1Hm)); 
  };
  if(num_motors>0)  /*motor 1 - output PWM 1A and 1B*/
  {
    *PWM_PWCYR1 = pwm_per;    //constant set up period of PWM counter
    *PWM_PWCR1 |=PWCR1_CSTm; /*start PWM counter*/
  };
}

__attribute__ ((deprecated)) void pxmc_dcm_init_fbmode(pxmc_state_t *mcs)
{
//   mcs->pxms_do_inp=pxmc_tpuirc_inp;			/*set up pointer on function measure IRC*/
//   mcs->pxms_do_out=pxmc_pwm1f_out;	//pointer on funct. control energi for outpu (PWM)
//   mcs->pxms_do_con=pxmc_pid_con;		/*set up pointer on PID con. function  (implicit (PXMC.c) or user*/
   pxmc_set_default_services(mcs,m_dc_irc_pwm);		// !!! For compability only
   
   //int HelVar=pwm_per;
   //mcs->pxms_me = HelVar << 7;         //max energy enter in a motor
   pxmc_set_max_energy(mcs);	// !!! For compability only
 
  /* debug PWM generation */
/*  if(!pxmc_dbg_hist)
     pxmc_dbg_hist=0;*/           /* FIXME: Are here correct braces??? */
/*   pxmc_dbg_hist=pxmc_dbg_histalloc(4096);
   pxmc_dbgset(mcs, pxmc_dbg_pwm12, 1);		
   pxmc_set_flag(mcs,PXMS_DBG_b);*/
   pxmc_set_default_debug(mcs,m_dc_irc_pwm);	// !!! For compability only
}


#if 0
/**
 * pxmc_stm_init - Initializes stepper or brush-less motor subsystem
 *
 * Configuration of IO registers, TPU for phase counting mode.
 * Returns 0 if the axis mode is set correctly.
 *
 * Timers 2 or 5 are in phase counting mode 1.
 * Choose which one defining PXMC_IRC_TPUN.
 * Timer 4 is used to count sampling period (set in TPU_TGR4A)
 * and activate pxmc_sfi_isr interrupt handler.
 *
 */
int pxmc_stm_init(void)
{
 #if HW_VER_CODE<VER_CODE(0,2,0)
  ID_STM_ENABLE(0);
  /*SHADDOW_REG_SET(DIO_P1DDR,0x0f);*/
 #else /* >=VER_CODE(0,2,0) */
  /* P10, P11 .. PWM */
  /* P15 .. synchronization for PWM */
  /* P12, P13 .. IRC */
  /* P16 .. THUMB inp, P17 .. THUMB output */
  SHADDOW_REG_CLR(DIO_P1DDR,0x5c);
  SHADDOW_REG_SET(DIO_P1DDR,0xa3);
  /* PF0, PF1 .. phase direction registers */
  SHADDOW_REG_SET(DIO_PFDDR,0x03);
 #endif /* >=VER_CODE(0,2,0) */
  pxmc_main_list.pxml_arr[0]->pxms_ap+=0x100; /*!!!*/
  if(pxmc_main_list.pxml_arr[0]->pxms_ene<0x1800)
    pxmc_main_list.pxml_arr[0]->pxms_ene+=0x0400;

  pxmc_sfikhz=1; /* 5 kHz fast sampling interval for stepper motors; 1kHz for normal CD brash motors*/

  pxmc_clear_flag(pxmc_main_list.pxml_arr[0],PXMS_ENG_b);
  pxmc_clear_flag(pxmc_main_list.pxml_arr[0],PXMS_ENR_b);

 #if HW_VER_CODE<VER_CODE(0,2,0)
  *SYS_MSTPCRC &= ~MSTPCRC_DA23m;
  *DA_DACR23|= (DACR23_DAEm*0) | DACR23_DAOE0m;

  pxmc_nofb_out(&mcs0);

  ID_STM_ENABLE(1);
 #endif /* <VER_CODE(0,2,0) */

  /* TPU initialization */
  *SYS_MSTPCRA&=~MSTPCRA_TPUm;
  *TPU_TSTR&=~(1<<4);		/* Stop channel 4 */
  /* system clock/1 ,rising edge, clearing source TGRA */
  *TPU_TCR4=(TPCR_TPSC_F1 | TPCR_CKEG_RIS | TPCR_CCLR_TGRA);
  /* normal mode */
  *TPU_TMDR4=TPMDR_MD_NORMAL;
  /* TGRA initiates interrupt */
  excptvec_set(EXCPTVEC_TGI4A,pxmc_sfi_isr);
  *TPU_TIER4=TIER4_TGIEAm;

  *TPU_TGR4A=((CPU_SYS_HZ+500)/1000)/pxmc_sfikhz;

  *TPU_TSTR|=(1<<4);		/* Start channel 4 */

 #if PXMC_IRC_TPUN==2
  /* IRC input initialization for channel 2 */
  *TPU_TPSTR&=~(1<<2);/* Stop channel 2 */
  /* system clock/1 ,rising edge */
  *TPU_TPCR2=0;
  /* phase counting mode 1 */
  *TPU_TPMDR2=TPMDR_MD_PHACN1;
  /* no irq */
  *TPU_TPIER2=0;
  *TPU_TPSTR|=(1<<2); /* Start channel 2 */
 #elif PXMC_IRC_TPUN==5
  /* IRC input initialization for channel 5 */
  *TPU_TSTR&=~(1<<5);/* Stop channel 5 */
  /* system clock/1 ,rising edge */
  *TPU_TCR5=0;
  /* phase counting mode 1 */
  *TPU_TMDR5=TPMDR_MD_PHACN1;
  /* no irq */
  *TPU_TIER5=0;
  *TPU_TSTR|=(1<<5); /* Start channel 5 */
 #endif /* PXMC_IRC_TPUN */

  /* set working mode for axis */
  return pxmc_axis_mode(pxmc_main_list.pxml_arr[0], 1);
}
#endif

