/* increment subdivission is fixed, else selectable by pxms_subdiv */
#define PXMC_WITH_FIXED_SUBDIV
/* multiphase motor control */
#define PXMC_WITH_PHASE_TABLE
/* controller with finegrained speed generation */
#define PXMC_WITH_FINE_GRAINED
/* special calling convention for pxms_??? state functions */
//#define PXMC_WITH_FAST_CALL
/* possibility to select generator variant per axis */
//#define PXMC_WITH_GEN_SELECTION
/* whether to compile in debugger support */
#define PXMC_WITH_DBG_HIST
/* controller with current feedback */
//#define PXMC_WITH_CURRENTFB
/* The pxmc_set/clear_flags not supported, use one by one pxmc_set/clear_flag */
#define PXMC_WITH_FLAGS_BYBITS_ONLY

#ifndef PXMC_SUBDIV
#ifdef PXMC_WITH_FIXED_SUBDIV
/* number of fraction bits in integer part of possition */
#define PXMC_SUBDIV(mcs) 8
#else /*PXMC_WITH_FIXED_SUBDIV*/
#define PXMC_SUBDIV(mcs) ((mcs)->pxms_subdiv)
#endif /*PXMC_WITH_FIXED_SUBDIV*/
#endif /*PXMC_SUBDIV*/

#ifndef PXMC_WITH_STDINT_INCLUDED
#define PXMC_WITH_STDINT_INCLUDED
#include <stdint.h>
#include <limits.h>
#endif /*PXMC_WITH_STDINT_INCLUDED*/
