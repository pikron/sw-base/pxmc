//
// C++ Interface: commthread
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef COMMTHREAD_H
#define COMMTHREAD_H

#include <QThread>
#include "../pxmc_sim_socket.h"

/**
	@author Michal Sojka <sojkam1@fel.cvut.cz>
*/
class CommThread : public QThread
{
Q_OBJECT
public:
    CommThread(int socket, QObject *parent = 0);

    ~CommThread();
private:
    int socket;
    virtual void run();
signals:
    void new_data(struct pxmc_to_gui data);
};

#endif
