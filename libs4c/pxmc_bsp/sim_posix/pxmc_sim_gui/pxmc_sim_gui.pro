FORMS += motor_widget.ui \
pxmc_sim_form.ui
SOURCES += main.cpp \
pxmcsimform.cpp \
pxmcmotorwidget.cpp \
commthread.cpp
HEADERS += pxmcsimform.h \
pxmcmotorwidget.h \
commthread.h
TEMPLATE = app

TARGET = pxmc_sim_gui

LIBS += -lpxmc_sim_socket

