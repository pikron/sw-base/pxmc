//
// C++ Implementation: 
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "pxmcmotorwidget.h"
#include "../pxmc_sim_socket.h"
#include <sys/socket.h> 
#include "commthread.h"
 

PXMCMotorWidget::PXMCMotorWidget(int motor_num, QWidget* parent, Qt::WFlags fl)
: QWidget( parent, fl ), Ui::Form()
{
	setupUi(this);
	socket = pxmc_create_socket(PXMC_SIM_GUI_SOCKET_PATH, motor_num);
	CommThread *commthr = new CommThread(socket, parent);
	qRegisterMetaType<struct pxmc_to_gui>("pxmc_to_gui");
	connect(commthr, SIGNAL(new_data(pxmc_to_gui)), 
		this, SLOT(display_data(pxmc_to_gui)),
		Qt::QueuedConnection);
	commthr->start();
}

PXMCMotorWidget::~PXMCMotorWidget()
{
}

/*$SPECIALIZATION$*/


/*!
    \fn PXMCMotorWidget::update()
 */
void PXMCMotorWidget::display_data(struct pxmc_to_gui data)
{
	int val;
	labelPosition->setNum((int)data.ap);
	val = data.ap%dial->maximum();
	if (val < 0) val = dial->maximum() + val;
	dial->setValue(val);
	
	labelActualSpeed->setNum((int)data.as);
	progressBarActualSpeed->setValue(abs(data.as));
	
	labelPower->setNum((int)data.ene);
	progressBarPower->setValue(abs(data.ene));
}
