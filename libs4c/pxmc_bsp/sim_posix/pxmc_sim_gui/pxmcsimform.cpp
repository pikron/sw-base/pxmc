//
// C++ Implementation: 
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "pxmcsimform.h"
#include "pxmcmotorwidget.h"

PXMCSimForm::PXMCSimForm(QWidget* parent, Qt::WFlags fl)
: QDialog( parent, fl ), Ui::MotorForm(), motor_widgets()
{
	setupUi(this);
	add_motor();
	add_motor();
}

PXMCSimForm::~PXMCSimForm()
{
}

/*$SPECIALIZATION$*/




/*!
    \fn PXMCSimForm::add_motor()
 */
void PXMCSimForm::add_motor()
{
	int num;
	
	num = motor_widgets.size();
	PXMCMotorWidget *mw = new PXMCMotorWidget(num);
	vboxLayout->insertWidget(num, mw);
	motor_widgets.append(mw);
}
