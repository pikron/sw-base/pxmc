#include <qapplication.h>
#include "pxmcsimform.h"

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    PXMCSimForm w;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
