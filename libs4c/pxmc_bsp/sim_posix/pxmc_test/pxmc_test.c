/* program to test PXMC (for DC motor control) on Hitashi processor H8S2638*/

/* procesor H8S/2638 ver 1.1  */
#include <stdio.h>
#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <cmd_proc.h>
#include <stdlib.h>
#include <pxmc.h>
#include <pxmcbsp.h>
#include <cmdio_std.h>

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help={0, 0,"HELP","prints help for commands",
                        cmd_do_help,{(char*)&cmd_list}};

extern cmd_des_t *cmd_stm_default[1];

cmd_des_t const *cmd_list_default[]={
  
  &cmd_des_help,
  CMD_DES_INCLUDE_SUBLIST(cmd_stm_default),
  NULL
};

cmd_des_t const **cmd_list=cmd_list_default;

int main()
{
	pxmc_initialize();
	printf("ready\n");
	do { 
		cmd_processor_run(&cmd_io_std_line, cmd_list_default);
	} while(1);
};

