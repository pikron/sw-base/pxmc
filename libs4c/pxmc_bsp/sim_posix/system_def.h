/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def_h8canusb.h - definition of hardware adresses and registers
 
  Copyright (C) 2002 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_HW01_H_
#define _SYSTEM_DEF_HW01_H_

#define DEB_LED_INIT()
#define DEB_LED_OFF(num)
#define DEB_LED_ON(num)

#endif /* _SYSTEM_DEF_HW01_H_ */
