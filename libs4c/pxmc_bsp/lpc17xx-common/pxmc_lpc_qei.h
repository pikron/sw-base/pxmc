/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmc_lpc_qei.h - generic multi axis motion controller
                       LPC17xx QEI input and events processing

  Copyright (C) 2001-2011 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2011 by PiKRON Ltd. - originator
                          http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMC_LPC_QEI_H_
#define _PXMC_LPC_QEI_H_

#include "pxmc.h"

#define LPC_QEI_INX_Int_b	0
#define LPC_QEI_TIM_Int_b	1
#define LPC_QEI_VELC_Int_b	2
#define LPC_QEI_DIR_Int_b	3
#define LPC_QEI_ERR_Int_b	4
#define LPC_QEI_ENCLK_Int_b	5
#define LPC_QEI_POS0_Int_b	6
#define LPC_QEI_POS1_Int_b	7
#define LPC_QEI_POS2_Int_b	8
#define LPC_QEI_REV_Int_b	9
#define LPC_QEI_POS0REV_Int_b	10 
#define LPC_QEI_POS1REV_Int_b	11 
#define LPC_QEI_POS2REV_Int_b	12 

#define LPC_QEI_Int_CNT		13 

#define LPC_QEI_INX_Int_m	(1 << LPC_QEI_INX_Int_b)
#define LPC_QEI_TIM_Int_m	(1 << LPC_QEI_TIM_Int_b)
#define LPC_QEI_VELC_Int_m	(1 << LPC_QEI_VELC_Int_b)
#define LPC_QEI_DIR_Int_m	(1 << LPC_QEI_DIR_Int_b)
#define LPC_QEI_ERR_Int_m	(1 << LPC_QEI_ERR_Int_b)
#define LPC_QEI_ENCLK_Int_m	(1 << LPC_QEI_ENCLK_Int_b)
#define LPC_QEI_POS0_Int_m	(1 << LPC_QEI_POS0_Int_b)
#define LPC_QEI_POS1_Int_m	(1 << LPC_QEI_POS1_Int_b)
#define LPC_QEI_POS2_Int_m	(1 << LPC_QEI_POS2_Int_b)
#define LPC_QEI_REV_Int_m	(1 << LPC_QEI_REV_Int_b)
#define LPC_QEI_POS0REV_Int_m	(1 << LPC_QEI_POS0REV_Int_b) 
#define LPC_QEI_POS1REV_Int_m	(1 << LPC_QEI_POS1REV_Int_b) 
#define LPC_QEI_POS2REV_Int_m	(1 << LPC_QEI_POS2REV_Int_b) 

struct lpc_qei_state_t;

typedef void lpc_qei_event_callback_t(struct lpc_qei_state_t *qst, void *context);

typedef struct lpc_qei_state_t {
  unsigned int flg;
  unsigned int index_occ;
  long index_pos;
  lpc_qei_event_callback_t **callback;
  void **context;
} lpc_qei_state_t;

#define LPC_QEI_INITIALIZED_m	0x0001

#define LPC_QEI_CMPOS_CHAN_CNT	3

extern lpc_qei_state_t lpc_qei_state;

int lpc_qei_setup_irq(lpc_qei_state_t *qst);
int lpc_qei_setup_index_catch(lpc_qei_state_t *qst);
int lpc_qei_set_cmpos_irq_disable(lpc_qei_state_t *qst, int chan, long pos);
int lpc_qei_set_cmpos(lpc_qei_state_t *qst, int chan, long pos);
int lpc_qei_cmpos_irq_enable(lpc_qei_state_t *qst, int chan);
int lpc_qei_cmpos_irq_disable(lpc_qei_state_t *qst, int chan);
int lpc_qei_cmpos_without_ie(lpc_qei_state_t *qst, int chan, long pos);
long lpc_qei_get_cmpos(lpc_qei_state_t *qst, int chan);
long lpc_qei_get_pos(lpc_qei_state_t *qst);


int pxmc_inp_lpc_qei_inp(struct pxmc_state *mcs);
int pxmc_inp_lpc_qei_ap2hw(struct pxmc_state *mcs);
int pxmc_inp_lpc_qei_init(int chan);

#endif /*_PXMC_LPC_QEI_H_*/