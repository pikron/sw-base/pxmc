#ifndef _PXMC_LPC_QEI_COMPAT_H_
#define _PXMC_LPC_QEI_COMPAT_H_

#include <LPC17xx.h>

/* Compatibility between different NXP headers versions */

#ifndef SC
#define SC LPC_SC
#endif

#ifndef QEI

#define QEI LPC_QEI

#define QEICON		CON
#define QEISTAT		STAT
#define QEICONF		CONF
#define QEIPOS		POS
#define QEIMAXPOS	MAXPOS
#define QEICMPOS0	CMPOS0
#define QEICMPOS1	CMPOS1
#define QEICMPOS2	CMPOS2
#define QEIINXCNT	INXCNT
#define QEIINXCMP0	INXCMP0
#define QEILOAD		LOAD
#define QEITIME		TIME
#define QEIVEL		VEL
#define QEICAP		CAP
#define QEIVELCOMP	VELCOMP
#define QEIFILTERPHA	FILTERPHA
#define QEIFILTERPHB	FILTERPHB
#define QEIFILTERINX	FILTERINX
#define QEIWINDOW	WINDOW
#define QEIINXCMP1	INXCMP1
#define QEIINXCMP2	INXCMP2
#define QEIIEC		IEC
#define QEIIES		IES
#define QEIINTSTAT	INTSTAT
#define QEIIE		IE
#define QEICLR		CLR
#define QEISET		SET
#endif

#endif /*_PXMC_LPC_QEI_COMPAT_H_*/
