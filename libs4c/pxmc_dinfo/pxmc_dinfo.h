#ifndef _PXMC_DINFO_H
#define _PXMC_DINFO_H

#include <pxmc.h>
#include <suiut/sui_dinfo.h>

/* Intercept and return position in subdivision of IRC */
#define PXMC_DI_SUBDIVPOS 0x100000l
#define PXMC_DI_SIZE_SHIFT 16
#define PXMC_DI_OFFSET_m   ((1l<<PXMC_DI_SIZE_SHIFT)-1)


#define PXMC_DI_GENERIC_INFO(_field) ( \
  UL_OFFSETOF(pxmc_state_t,pxms_##_field) | \
  ((long)sizeof(((pxmc_state_t*)0)->pxms_##_field) << PXMC_DI_SIZE_SHIFT) \
)

#define PXMC_DI_BITFLD_SHIFT  16
#define PXMC_DI_BITFLD_MASK_m ((1l<<PXMC_DI_BITFLD_SHIFT)-1)

#define PXMC_DI_CFG_INFO(_bitfld) ( \
  ( (long)PXMS_CFG_##_bitfld##_b << PXMC_DI_BITFLD_SHIFT) | \
  ( PXMS_CFG_##_bitfld##_m >> PXMS_CFG_##_bitfld##_b) \
)

#define PXMC_DI_GENERIC_PAR(n,field,min,max,rd,wr,iflg, is) \
  SUI_CANBE_CONST \
  sui_dinfo_static_with_idxsize(pxmc_##n##_di, SUI_TYPE_LONG, NULL, rd, wr, \
                   PXMC_DI_GENERIC_INFO(field) | (iflg), (min), (max), 0, is);

extern SUI_CANBE_CONST sui_dinfo_t pxmc_cfg_i2pt_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_is_busy_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_flg_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ap_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_rp_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_go_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_spd_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_spdfg_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_md_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ms_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ma_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_p_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_i_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_d_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_s1_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_s2_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_me_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ene_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_cfg_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_mode_di;
#ifdef PXMC_WITH_PHASE_TABLE
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ptirc_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ptper_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ptofs_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ptshift_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_ptmark_di;
#endif /*PXMC_WITH_PHASE_TABLE*/
extern SUI_CANBE_CONST sui_dinfo_t pxmc_pwm1cor_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_pwm2cor_di;

extern SUI_CANBE_CONST sui_dinfo_t pxmc_ap_sd_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_rp_sd_di;
extern SUI_CANBE_CONST sui_dinfo_t pxmc_go_sd_di;

int pxmc_di_update_idxsize(int idxsize);
int pxmc_di_list_enum(int idx, SUI_CANBE_CONST sui_dinfo_t **p_dinfo,
                      const char ** p_name);

#endif /*_PXMC_DINFO_H*/