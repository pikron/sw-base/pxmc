#include "pxmc.h"
#include "pxmc_dinfo.h"

#ifndef PXML_DI_AXES_CNT
#ifdef PXML_MAIN_CNT
#define PXML_DI_AXES_CNT PXML_MAIN_CNT
#else
#define PXML_DI_AXES_CNT_DYNAMIC 1
#define PXML_DI_AXES_CNT 1
#endif
#endif

pxmc_state_t *
pxmc_di_indx2mcs(sui_dinfo_t *datai, long idx)
{
  pxmc_state_t *mcs;

  if (datai) {
    if (datai->ptr)
      return (pxmc_state_t *)datai->ptr;
  }

  if ((idx < 0) || (idx >= pxmc_main_list.pxml_cnt))
    return NULL;

  mcs = pxmc_main_list.pxml_arr[idx];
  if (!mcs)
    return NULL;

  return mcs;
}

int
pxmc_di_generic_rdval(sui_dinfo_t *datai, long idx, void *buf)
{
  long val;
  int fld_size = datai->info >> PXMC_DI_SIZE_SHIFT;
  int fld_offs = datai->info & PXMC_DI_OFFSET_m;
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  if (fld_size == sizeof(long)) {
    long *p = (long *)((char *)mcs+fld_offs);
    val = *p;
  } else if (fld_size == sizeof(int)) {
    int *p = (int *)((char *)mcs+fld_offs);
    val = *p;
  } else if (fld_size == sizeof(short)) {
    short *p = (short *)((char *)mcs+fld_offs);
    val = *p;
  } else {
    return SUI_RET_ERR;
  }

  *(long *)buf = val;

  return SUI_RET_OK;
}

int
pxmc_di_generic_wrval(sui_dinfo_t *datai, long idx, const void *buf)
{
  long val;
  int fld_size = datai->info >> PXMC_DI_SIZE_SHIFT;
  int fld_offs = datai->info & PXMC_DI_OFFSET_m;
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  val = *(long *)buf;

  if (fld_size == sizeof(long)) {
    long *p = (long *)((char *)mcs+fld_offs);
    *p = val;
  } else if (fld_size == sizeof(int)) {
    int *p = (int *)((char *)mcs+fld_offs);
    *p = val;
  } else if (fld_size == sizeof(short)) {
    short *p = (short *)((char *)mcs+fld_offs);
    *p = val;
  } else {
    return SUI_RET_ERR;
  }

  sui_dinfo_changed(datai);
  return SUI_RET_OK;
}

int
pxmc_di_cfg_fld_rdval(sui_dinfo_t *datai, long idx, void *buf)
{
  unsigned long val;
  pxmc_state_t *mcs;
  int fld_shift = datai->info >> PXMC_DI_BITFLD_SHIFT;
  int fld_mask = datai->info & PXMC_DI_BITFLD_MASK_m;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  val = mcs->pxms_cfg;

  val = (val >> fld_shift) & fld_mask;

  *(unsigned long *)buf = val;

  return SUI_RET_OK;
}

int
pxmc_di_cfg_fld_wrval(sui_dinfo_t *datai, long idx, const void *buf)
{
  unsigned long val;
  pxmc_state_t *mcs;
  int fld_shift = datai->info >> PXMC_DI_BITFLD_SHIFT;
  int fld_mask = (datai->info & PXMC_DI_BITFLD_MASK_m) << fld_shift;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  val = *(unsigned long *)buf;

  val = (val << fld_shift) & fld_mask;

  mcs->pxms_cfg = (mcs->pxms_cfg & ~fld_mask) | val;

  pxmc_axis_mode(mcs, PXMC_AXIS_MODE_NOCHANGE);

  sui_dinfo_changed(datai);

  return SUI_RET_OK;
}

int
pxmc_di_rdpos(sui_dinfo_t *dinfo, long indx, void *buf)
{
  char *ptr;
  pxmc_state_t *mcs;

  ptr = (char *)(mcs = pxmc_di_indx2mcs(dinfo, indx));
  if (!ptr)
    return SUI_RET_ERR;
  ptr += dinfo->info & PXMC_DI_OFFSET_m;
  if (dinfo->info & PXMC_DI_SUBDIVPOS)
    *(long *)buf = *(long *)ptr;
  else
    *(long *)buf = (*(long *)ptr) >> PXMC_SUBDIV(mcs);
  return SUI_RET_OK;
}

int
pxmc_di_wrpos(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  char *ptr;
  pxmc_state_t *mcs;

  ptr = (char *)(mcs = pxmc_di_indx2mcs(dinfo, indx));
  if (!ptr)
    return SUI_RET_ERR;
  ptr += dinfo->info & PXMC_DI_OFFSET_m;
  if (dinfo->info & PXMC_DI_SUBDIVPOS)
    *(long *)ptr = *(long *)buf;
  else
    *(long *)ptr = (*(long *)buf) << PXMC_SUBDIV(mcs);
  return SUI_RET_OK;
}

int
pxmc_di_wrgo(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  pxmc_state_t *mcs;
  long val = *(long *)buf;

  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  if (!(dinfo->info & PXMC_DI_SUBDIVPOS))
    val = val << PXMC_SUBDIV(mcs);
  if (pxmc_go(mcs, val, 0, 0) < 0)
    return SUI_RET_ERR;
  return SUI_RET_OK;
}

int
pxmc_di_wrene(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  pxmc_state_t *mcs;
  long val = *(long *)buf;

  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  if (!(dinfo->info & PXMC_DI_SUBDIVPOS))
    val = val << PXMC_SUBDIV(mcs);
  pxmc_set_const_out(mcs, val);
  return SUI_RET_OK;
}

int
pxmc_di_wrspd(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  if (pxmc_spd(mcs, *(long *)buf, 0) < 0)
    return SUI_RET_ERR;
  return SUI_RET_OK;
}

int
pxmc_di_rdspdfg(sui_dinfo_t *dinfo, long indx, void *buf)
{
  pxmc_state_t *mcs;

//  long val;
  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  /*
   * val=mcs->pxms_rs<<mcs->subdivfg;
   * val|=((long)mcs->pxms_rsfg)&((1l<<mcs->subdivfg)-1);
   * *(long*)buf=val;
   * return SUI_RET_OK;
   */
  return SUI_RET_ERR;
}

int
pxmc_di_wrspdfg(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  if (pxmc_spdfg(mcs, *(long *)buf, 0) < 0)
    return SUI_RET_ERR;
  return SUI_RET_OK;
}

int
pxmc_di_rdmode(sui_dinfo_t *datai, long idx, void *buf)
{
  unsigned long val;
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  val = pxmc_axis_rdmode(mcs);

  *(unsigned long *)buf = val;

  return SUI_RET_OK;
}

int
pxmc_di_wrmode(sui_dinfo_t *datai, long idx, const void *buf)
{
  long val;
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  val = *(long *)buf;

  if (pxmc_axis_mode(mcs, val) < 0)
    return SUI_RET_ERR;

  sui_dinfo_changed(datai);
  return SUI_RET_OK;
}

#ifdef PXMC_WITH_PHASE_TABLE
int
pxmc_di_wrptval(sui_dinfo_t *datai, long idx, const void *buf)
{
  pxmc_state_t *mcs;
  int ret;

  mcs = pxmc_di_indx2mcs(datai, idx);
  if (mcs == NULL)
    return SUI_RET_ERR;

  ret = pxmc_di_generic_wrval(datai, idx, buf);
  if (ret < 0)
    return SUI_RET_ERR;

  ret  = pxmc_axis_mode(mcs, 0);
  if (ret < 0)
    return SUI_RET_ERR;

  sui_dinfo_changed(datai);
  return SUI_RET_OK;
}
#endif /*PXMC_WITH_PHASE_TABLE*/

int
pxmc_di_rd_is_busy(sui_dinfo_t *dinfo, long indx, void *buf)
{
  pxmc_state_t *mcs;

  mcs = pxmc_di_indx2mcs(dinfo, indx);
  if (!mcs)
    return SUI_RET_ERR;
  *(long *)buf = mcs->pxms_flg & PXMS_BSY_m;
  return SUI_RET_OK;
}

SUI_CANBE_CONST
sui_dinfo_static_with_idxsize(pxmc_cfg_i2pt_di, SUI_TYPE_ULONG, NULL,
                 pxmc_di_cfg_fld_rdval, pxmc_di_cfg_fld_wrval,
                 PXMC_DI_CFG_INFO(I2PT), 0, 1, 0, PXML_DI_AXES_CNT);

PXMC_DI_GENERIC_PAR(is_busy, flg, 0, 0, pxmc_di_rd_is_busy, NULL, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(flg, flg, 0, 0, pxmc_di_generic_rdval, NULL, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ap, ap, 0, 0, pxmc_di_rdpos, NULL, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(rp, rp, 0, 0, pxmc_di_rdpos, pxmc_di_wrgo, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(go, ap, 0, 0, pxmc_di_rdpos, pxmc_di_wrgo, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(spd, rs, -0x7fff, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrspd, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(spdfg, rs, -0x7fffffff, 0x7fffffff, pxmc_di_rdspdfg, pxmc_di_wrspdfg, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(md, md, 0, 0x7fff, pxmc_di_rdpos, pxmc_di_wrpos, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ms, ms, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ma, ma, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(p, p, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(i, i, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(d, d, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(s1, s1, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(s2, s2, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(me, me, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ene, ene, -0x7fff, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrene, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(cfg, cfg, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(mode, do_con, 0, 0, pxmc_di_rdmode, pxmc_di_wrmode, 0, PXML_DI_AXES_CNT);
#ifdef PXMC_WITH_PHASE_TABLE
PXMC_DI_GENERIC_PAR(ptirc, ptirc, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrptval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ptper, ptper, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrptval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ptofs, ptofs, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrptval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ptshift, ptshift, -500, +500, pxmc_di_generic_rdval, pxmc_di_wrptval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ptmark, ptmark, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_wrptval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(ptvang, ptvang, -500, +500, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
#endif /*PXMC_WITH_PHASE_TABLE*/
PXMC_DI_GENERIC_PAR(pwm1cor, pwm1cor, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(pwm2cor, pwm2cor, 0, 0x7fff, pxmc_di_generic_rdval, pxmc_di_generic_wrval, 0, PXML_DI_AXES_CNT);

PXMC_DI_GENERIC_PAR(ap_sd, ap, 0, 0, pxmc_di_rdpos, NULL, PXMC_DI_SUBDIVPOS, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(rp_sd, rp, 0, 0, pxmc_di_rdpos, pxmc_di_wrgo, PXMC_DI_SUBDIVPOS, PXML_DI_AXES_CNT);
PXMC_DI_GENERIC_PAR(go_sd, ap, 0, 0, pxmc_di_rdpos, pxmc_di_wrgo, PXMC_DI_SUBDIVPOS, PXML_DI_AXES_CNT);

typedef struct {
  SUI_CANBE_CONST sui_dinfo_t* dinfo;
  const char *name;
} pxmc_di_list_entry_t;

const pxmc_di_list_entry_t pxmc_di_list[] = {
  {&pxmc_is_busy_di, "pxmc_is_busy"},
  {&pxmc_flg_di, "pxmc_flg"},
  {&pxmc_ap_di, "pxmc_ap"},
  {&pxmc_rp_di, "pxmc_rp"},
  {&pxmc_go_di, "pxmc_go"},
  {&pxmc_spd_di, "pxmc_spd"},
  {&pxmc_spdfg_di, "pxmc_spdfg"},
  {&pxmc_md_di, "pxmc_md"},
  {&pxmc_ms_di, "pxmc_ms"},
  {&pxmc_ma_di, "pxmc_ma"},
  {&pxmc_p_di, "pxmc_p"},
  {&pxmc_i_di, "pxmc_i"},
  {&pxmc_d_di, "pxmc_d"},
  {&pxmc_s1_di, "pxmc_s1"},
  {&pxmc_s2_di, "pxmc_s2"},
  {&pxmc_me_di, "pxmc_me"},
  {&pxmc_ene_di, "pxmc_ene"},
  {&pxmc_cfg_di, "pxmc_cfg"},
  {&pxmc_mode_di, "pxmc_mode"},
#ifdef PXMC_WITH_PHASE_TABLE
  {&pxmc_ptirc_di, "pxmc_ptirc"},
  {&pxmc_ptper_di, "pxmc_ptper"},
  {&pxmc_ptofs_di, "pxmc_ptofs"},
  {&pxmc_ptshift_di, "pxmc_ptshift"},
  {&pxmc_ptmark_di, "pxmc_ptmark"},
#endif /*PXMC_WITH_PHASE_TABLE*/
  {&pxmc_pwm1cor_di, "pxmc_pwm1cor"},
  {&pxmc_pwm2cor_di, "pxmc_pwm2cor"},

  {&pxmc_ap_sd_di, "pxmc_ap_sd"},
  {&pxmc_rp_sd_di, "pxmc_rp_sd"},
  {&pxmc_go_sd_di, "pxmc_go_sd"},
};

int pxmc_di_list_size = sizeof(pxmc_di_list) / sizeof(*pxmc_di_list);

int pxmc_di_list_enum(int idx, SUI_CANBE_CONST sui_dinfo_t **p_dinfo,
                      const char ** p_name)
{
  if (idx >= pxmc_di_list_size)
    return -1;

  *p_dinfo = pxmc_di_list[idx].dinfo;
  *p_name = pxmc_di_list[idx].name;
  return 0;
}

#ifdef PXML_DI_AXES_CNT_DYNAMIC
int pxmc_di_update_idxsize(int idxsize)
{
  int i;
  for (i = 0; i < pxmc_di_list_size; i++)
    pxmc_di_list[i].dinfo->idxsize = idxsize;
  return 0;
}
#else /*PXML_DI_AXES_CNT_DYNAMIC*/
int pxmc_di_update_idxsize(int idxsize)
{
  return -1;
}
#endif  /*PXML_DI_AXES_CNT_DYNAMIC*/
